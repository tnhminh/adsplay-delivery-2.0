<?xml version="1.0" encoding="UTF-8"?>
<VAST version="2.0">
   {{#each instreamVideoAds}}
   <Ad id="{{adId}}">
      <Wrapper>
         <AdSystem>AdsPlay-2.0</AdSystem>
         <AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
         <VASTAdTagURI><![CDATA[{{vastAdTagURI}}]]></VASTAdTagURI>
           <Error>
						<![CDATA[ {{logUrl}}?event=request&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]>
					 </Error>
         <Impression><![CDATA[{{logUrl}}?event=impression&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Impression>
         <Creatives>
            <Creative sequence="1" id="{{adId}}">
               <Linear>
                  <TrackingEvents>
                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?event=creativeView&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
                     <Tracking event="start"><![CDATA[{{logUrl}}?event=start&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?event=view50&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
                     <Tracking event="complete"><![CDATA[{{logUrl}}?event=view100&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
                     <Tracking event="close"><![CDATA[{{logUrl}}?event=close&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></Tracking>
                  </TrackingEvents>
                  <VideoClicks>{{videoClicksTags}}<ClickTracking><![CDATA[{{logUrl}}?event=click&adid={{adId}}&placementId={{placementId}}&flightId={{flighId}}&t={{time}}]]></ClickTracking></VideoClicks>
               </Linear>
            </Creative>
         </Creatives>
      </Wrapper>
   </Ad>
   {{/each}}
</VAST>