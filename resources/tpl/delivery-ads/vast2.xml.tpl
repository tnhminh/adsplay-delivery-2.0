<?xml version="1.0" encoding="UTF-8"?>
<VAST version="2.0.1">

	{{#each instreamVideoAds}}
		<Ad id="{{adId}}">
			<InLine>
				<AdSystem version="1.0">AdsPLAY</AdSystem>
				<AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
				  <Error>
						<![CDATA[ {{logUrl}}?event=request&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]>
					 </Error>
			         <Impression><![CDATA[{{logUrl}}?event=impression&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Impression>
				{{#each impression3rdUrls}}
					<Impression><![CDATA[{{this}}]]></Impression>
				{{/each}}

				<Creatives>
					<Creative sequence="1" id="{{adId}}">						
						<Linear skipoffset="{{skipoffset}}" starttime="{{startTime}}" >
							<Duration>{{duration}}</Duration>
							<TrackingEvents>
								{{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?event=creativeView&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?event=start&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?event=view25&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?event=view50&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?event=view75&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?event=view100&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?event=close&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
							</TrackingEvents>
							<VideoClicks>{{videoClicksTags}}
								<ClickTracking><![CDATA[{{logUrl}}?event=click&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></ClickTracking>
							</VideoClicks>
							<MediaFiles>
							{{#each mediaFiles as |mediaFile|}}							
								<MediaFile height="{{mediaFile.height}}" width="{{mediaFile.width}}" bitrate="{{mediaFile.bitrate}}" type="{{mediaFile.type}}" delivery="{{mediaFile.delivery}}"><![CDATA[{{mediaFile.mediaSource}}]]></MediaFile>
							{{/each}}
							
							{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
							</MediaFiles>
						</Linear>
					</Creative>					
				</Creatives>
			</InLine>
		</Ad>
	{{/each}}		
	
</VAST>
