<?xml version="1.0" encoding="UTF-8"?>
<vmap:VMAP xmlns:vmap="http://www.iab.net/videosuite/vmap" version="1.0">
	{{#each adBreaks }}
	<vmap:AdBreak timeOffset="{{timeOffset}}" breakType="{{breakType}}" breakId="{{breakId}}">
	
		{{#each instreamVideoAds}}
		<vmap:AdSource id="{{adId}}" allowMultipleAds="false" followRedirects="true">
		<vmap:VASTAdData>
			<VAST version="2.0">
			   <Ad id="{{adId}}">
			   
			   {{#if adTagURI}}
			      <Wrapper>
			   {{/if}}
			   {{#unless adTagURI}}
			      <InLine>
			   {{/unless}}
			         <AdSystem version="1.0">AdsPLAY</AdSystem>
			         <AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
			         {{#if adTagURI}}
			         	<VASTAdTagURI><![CDATA[{{adTagURI}}]]></VASTAdTagURI>
			         {{/if}}
			         <Error>
						<![CDATA[ {{logUrl}}?event=request&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]>
					 </Error>
			         
			         <Impression><![CDATA[{{logUrl}}?event=impression&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Impression>
			         {{#each impression3rdUrls}}
			         <Impression><![CDATA[{{this}}]]></Impression>
			         {{/each}}
			         <Creatives>
			            <Creative sequence="1" id="{{adId}}">
			            
			            {{#if adTagURI}}
			               <Linear>
			            {{/if}}
			            {{#unless adTagURI}}
			               <Linear skipoffset="{{skipoffset}}">
			                  <Duration>{{duration}}</Duration>
			            {{/unless}}      
			                  <TrackingEvents>
			                     {{trackingEvent3rdTags}}
			                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?event=creativeView&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="start"><![CDATA[{{logUrl}}?event=start&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="firstQuartile"><![CDATA[{{logUrl}}?event=view25&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?event=view50&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?event=view75&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="complete"><![CDATA[{{logUrl}}?event=view100&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                     <Tracking event="close"><![CDATA[{{logUrl}}?event=close&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></Tracking>
			                  </TrackingEvents>
			                  
				              {{#if adTagURI}}
				                  <VideoClicks>
				                     <ClickTracking><![CDATA[{{logUrl}}?event=click&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></ClickTracking>
				                  </VideoClicks>
			                  {{/if}}
			                  
				              {{#unless adTagURI}}
				                  <VideoClicks>
				                     {{videoClicksTags}}
				                     <ClickTracking><![CDATA[{{logUrl}}?event=click&adId={{adId}}&placementId={{placementId}}&campaignId={{campaignId}}&flightId={{flighId}}&contentId={{contentId}}&buyType={{buyType}}&t={{time}}]]></ClickTracking>
				                  </VideoClicks>
				                  
				                  <MediaFiles>
				                  	{{#each mediaFiles as |mediaFile|}}
				                     <MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				                  	{{/each}}
				                  	
				                  	{{#unless mediaFiles}}
				              	<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
				            {{/unless}}
				                  </MediaFiles>

			                  {{/unless}}

			               </Linear>
			            </Creative>
			         </Creatives>
			         
			   {{#if adTagURI}}
			      </Wrapper>
			   {{/if}}
			   {{#unless adTagURI}}
			      </InLine>
			   {{/unless}}
			   </Ad>
			</VAST>
		</vmap:VASTAdData>
		</vmap:AdSource>
		{{/each}}
	
	</vmap:AdBreak>
	{{/each}}
 </vmap:VMAP>