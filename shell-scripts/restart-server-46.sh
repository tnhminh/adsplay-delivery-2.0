#!/bin/sh

JVM_PARAMS1="-Xms1024m -Xmx2048m -XX:+TieredCompilation -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseNUMA -server  -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=4081 -Dcom.sun.management.jmxremote.rmi.port=4081 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"

JVM_PARAMS2="-Xms1024m -Xmx2048m -XX:+TieredCompilation -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseNUMA -server  -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=4082 -Dcom.sun.management.jmxremote.rmi.port=4082 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"

JVM_PARAMS3="-Xms1024m -Xmx2048m -XX:+TieredCompilation -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseNUMA -server  -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=4083 -Dcom.sun.management.jmxremote.rmi.port=4083 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"JAR_NAME="adsplay-server-1.0.jar"
JAR_NAME="adsplay-server-1.0.jar"

IP="118.69.190.46"

cd /build/adsplay-server/
rm -rf delivery*.log 

kill -9 $(pgrep -f $JAR_NAME)
sleep 2

/usr/bin/java -jar $JVM_PARAMS1 $JAR_NAME $IP 8081 >> ./delivery1.log 2>&1 &
sleep 1
/usr/bin/java -jar $JVM_PARAMS2 $JAR_NAME $IP 8082 >> ./delivery2.log 2>&1 &
sleep 1
