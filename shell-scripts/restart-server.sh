#!/bin/sh

JVM_PARAMS="-Xms1024m -Xmx2048m -XX:+TieredCompilation -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseNUMA -server"
JAR_NAME="adsplay-server-1.0.jar"

cd /build/adsplay-server/

kill -9 $(pgrep -f $JAR_NAME)
sleep 2

/usr/bin/java8 -jar $JVM_PARAMS $JAR_NAME 118.69.184.50 8081 >> /dev/null 2>&1 &
sleep 1
/usr/bin/java8 -jar $JVM_PARAMS $JAR_NAME 118.69.184.50 8082 >> /dev/null 2>&1 &
sleep 1
/usr/bin/java8 -jar $JVM_PARAMS $JAR_NAME 118.69.184.50 8083 >> /dev/null 2>&1 &
sleep 1
/usr/bin/java8 -jar $JVM_PARAMS $JAR_NAME 118.69.184.50 8084 >> /dev/null 2>&1 &
