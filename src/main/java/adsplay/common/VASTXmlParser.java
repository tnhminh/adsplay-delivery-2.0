package adsplay.common;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.common.template.TemplateUtil;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.HttpClientUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringUtil;

public class VASTXmlParser {

    public static final String VAST_EVENT_COMPLETE = "complete";
    public static final String VAST_EVENT_IMPRESSION = "impression";
    private static final String HTTPS = "https:";
    private static final String HTTP = "http:";

    public static class VastXmlData {
        StringBuilder eventTrackingTags = new StringBuilder();
        StringBuilder clickTags = new StringBuilder();

        List<String> impression3rdUrls = new ArrayList<>();
        List<AdTrackingUrl> streamAdTrackingUrls = new ArrayList<>();

        List<MediaFile> mediaFiles = new ArrayList<MediaFile>();

        public List<MediaFile> getMediaFiles() {
            return mediaFiles;
        }

        public void setMediaFile(List<MediaFile> mediaFiles) {
            this.mediaFiles = mediaFiles;
        }

        public void addMediaFile(MediaFile mediaFile) {
            this.mediaFiles.add(mediaFile);
        }

        public void addImpression3rdUrl(String tag) {
            impression3rdUrls.add(tag);
        }

        public void addEventTrackingTag(String tag) {
            eventTrackingTags.append(tag);
        }

        public void addClickTag(String tag) {
            clickTags.append(tag);
        }

        public List<String> getImpression3rdUrls() {
            return impression3rdUrls;
        }

        public String getEventTrackingTags() {
            return eventTrackingTags.toString();
        }

        public String getClickTags() {
            return clickTags.toString();
        }

        public void addStreamAdTrackingUrl(String event, String url, int delay) {
            this.streamAdTrackingUrls.add(new AdTrackingUrl(event, url, delay));
        }

        public List<AdTrackingUrl> getStreamAdTrackingUrls() {
            return streamAdTrackingUrls;
        }
    }

    public static class MediaFile {
        int width;
        int height;
        String delivery;
        String type;
        String bitrate;
        String scalable;
        String maintainAspectRatio;
        String mediaSource;

        public MediaFile(int width, int height, String delivery, String type, String bitrate, String scalable, String maintainAspectRatio,
                String mediaSource) {
            super();
            this.width = width;
            this.height = height;
            this.delivery = delivery;
            this.type = type;
            this.bitrate = bitrate;
            this.scalable = scalable;
            this.maintainAspectRatio = maintainAspectRatio;
            this.mediaSource = mediaSource;
        }

        public String getMediaSource() {
            return mediaSource;
        }

        public void setMediaSource(String mediaSource) {
            this.mediaSource = mediaSource;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public String getDelivery() {
            return delivery;
        }

        public void setDelivery(String delivery) {
            this.delivery = delivery;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getBitrate() {
            return bitrate;
        }

        public void setBitrate(String bitrate) {
            this.bitrate = bitrate;
        }

        public String getScalable() {
            return scalable;
        }

        public void setScalable(String scalable) {
            this.scalable = scalable;
        }

        public String getMaintainAspectRatio() {
            return maintainAspectRatio;
        }

        public void setMaintainAspectRatio(String maintainAspectRatio) {
            this.maintainAspectRatio = maintainAspectRatio;
        }

    }

    public static String beginImgTag = "<Impression>";
    public static String endImgTag = "</Impression>";
    public static String beginTrackingEventTag = "<TrackingEvents>";
    public static String endTrackingEventTag = "</TrackingEvents>";
    public static String beginVideoClicksTag = "<VideoClicks>";
    public static String endVideoClicksTag = "</VideoClicks>";

    public static final String NO_AD_XML = TemplateUtil.render("delivery-ads/no-ads-vast2.xml");
    public static final String NO_AD_XML_VMAP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><vmap:VMAP xmlns:vmap=\"http://www.iab.net/videosuite/vmap\" version=\"1.0\"/>";

    static void process(Document doc, VastXmlData vastXmlData) {
        int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

        //// vastXml = xml.replace("http://visitanalytics",
        //// "https://visitanalytics");
        Elements impNodes = doc.select("Impression");
        for (Element node : impNodes) {
            String url = node.text().replace(HTTP, HTTPS).replace("[timestamp]", time + "");

            // System.out.println(url);
            vastXmlData.addImpression3rdUrl(url);
            vastXmlData.addStreamAdTrackingUrl(VAST_EVENT_IMPRESSION, url, 1);
        }

        System.out.println("------------------");
        Elements eventNodes = doc.select("Tracking[event]");

        for (Element node : eventNodes) {
            String trackingUrl = node.attr("event");
            String event = trackingUrl.toLowerCase();

            String eventValue = node.text();
            if (StringUtil.isNotEmpty(eventValue) && StringUtil.isNotEmpty(event)) {
                if (event.equalsIgnoreCase("start")) {
                    event = "creativeview";
                }
                String url = node.text().replace(HTTP, HTTPS).replace("[timestamp]", time + "");
                String tag = "<Tracking event=\"" + trackingUrl + "\"><![CDATA[" + url + "]]></Tracking>\n";
                // System.out.println(tag);
                vastXmlData.addEventTrackingTag(tag);

                if (event.equals("creativeview")) {
                    vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 3);
                } else if (event.equals("firstquartile")) {
                    vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 10);
                } else if (event.equals("midpoint")) {
                    vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 20);
                } else if (event.equals("thirdquartile")) {
                    vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 30);
                } else if (event.equals(VAST_EVENT_COMPLETE)) {
                    vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 40);
                }
            }
        }

        System.out.println("------------------");
        Elements clickTrackingNodes = doc.select("ClickTracking");
        for (Element node : clickTrackingNodes) {
            String url = node.text().replace(HTTP, HTTPS) + "&_t=" + time;
            String tag = "<ClickTracking><![CDATA[" + url + "]]></ClickTracking>";
            System.out.println(tag);
            vastXmlData.addClickTag(tag);
        }

        System.out.println("------------------");
        Elements clickThroughNodes = doc.select("ClickThrough");
        for (Element node : clickThroughNodes) {
            String url = node.text();
            if (url.contains("track.adnetwork.vn")) {
                String tag = "<ClickTracking><![CDATA[" + url.replace(HTTP, HTTPS) + "]]></ClickTracking>";
                vastXmlData.addClickTag(tag);
            } else {
                String tag = "<ClickThrough><![CDATA[" + url + "]]></ClickThrough>";
                vastXmlData.addClickTag(tag);
            }
        }

        Elements medieFiles = doc.select("MediaFile");
        for (Element node : medieFiles) {
            String delivery = node.attr("delivery");
            int width = StringUtil.safeParseInt(node.attr("width"), 700);
            int height = StringUtil.safeParseInt(node.attr("height"), 550);
            String type = node.attr("type");
            String bitrate = node.attr("bitrate");
            String scalable = node.attr("scalable");
            String maintainAspectRatio = node.attr("maintainAspectRatio");
            String mediaSource = node.text();
            MediaFile mediaFile = new MediaFile(width, height, delivery, type, bitrate, scalable, maintainAspectRatio, mediaSource);
            vastXmlData.addMediaFile(mediaFile);
        }
    }

    public static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder().maximumSize(100000)
            .expireAfterWrite(2800, TimeUnit.MILLISECONDS).build(new CacheLoader<String, String>() {
                public String load(String url) {
                    String xml = HttpClientUtil.executeGet(url);
                    return xml;
                }
            });

    public static VastXmlData parse(String xml) {
        VastXmlData vastXmlData = new VastXmlData();
        Document doc = Jsoup.parse(xml);

        System.out.println("-----BEGIN:VASTXmlParser------");
        System.out.println(xml);
        System.out.println("-----END:VASTXmlParser------");

        Elements vastTagURINodes = doc.select("VASTAdTagURI");
        String vASTAdTagURI = null;
        for (Element node : vastTagURINodes) {
            vASTAdTagURI = node.text();
        }

        process(doc, vastXmlData);

        if (vASTAdTagURI != null) {
            int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);
            vASTAdTagURI = vASTAdTagURI.replace("[timestamp]", time + "");
            String xml2;
            try {
                xml2 = xml3rdDataCache.get(vASTAdTagURI);
                Document doc2 = Jsoup.parse(xml2);
                process(doc2, vastXmlData);
            } catch (Exception e) {
            }
        }

        return vastXmlData;
    }

    public static void main(String[] args) {
        String url = "https://adserver.adtech.de/?advideo/3.0/1686.1/6577089/0/0/cc=2;vidAS=pre_roll;vidRT=VAST;vidRTV=2.0.1;cors=yes";
        int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);
        url = url.replace("[timestamp]", time + "");
        String xml = HttpClientUtil.executeGet(url);
        VastXmlData data = parse(xml);

        System.out.println("VastXmlData.getImpressionTags " + data.getImpression3rdUrls());
        System.out.println("VastXmlData.getEventTrackingTags " + data.getEventTrackingTags());
        System.out.println("VastXmlData.getClickTags " + data.getClickTags());
        System.out.println("VastXmlData.getAdsStream " + data.getStreamAdTrackingUrls());

    }
}
