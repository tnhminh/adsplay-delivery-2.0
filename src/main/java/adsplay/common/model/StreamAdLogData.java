package adsplay.common.model;

import java.util.ArrayList;
import java.util.List;

import adsplay.server.delivery.video.model.AdTrackingUrl;

public class StreamAdLogData {
	int adType = 10;
	List<AdTrackingUrl> tracking3rdUrls = new ArrayList<>();
			
	public StreamAdLogData( List<AdTrackingUrl> tracking3rdUrls) {
		super();			
		this.tracking3rdUrls = tracking3rdUrls;
	}
	public int getAdType() {
		return adType;
	}
	public void setAdType(int adType) {
		this.adType = adType;
	}
	public List<AdTrackingUrl> getTracking3rdUrls() {
		return tracking3rdUrls;
	}
	public void setTracking3rdUrls(List<AdTrackingUrl> tracking3rdUrls) {
		this.tracking3rdUrls = tracking3rdUrls;
	}		
}