package adsplay.common.template;

import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.server.delivery.video.model.VmapDataModel;

/**
 * The base object handle VAST and VMAP ad unit.
 * 
 * @author thaint
 *
 */
public class AdModel {

  /**
   * VAST ad unit
   */
  private VideoAdDataModel videoAdModel;
  
  /**
   * VMAP ad unit
   */
  private VmapDataModel vmapAdModel;

  public AdModel() {

  }

  public VideoAdDataModel getVideoAdModel() {
    return videoAdModel;
  }

  public void setVideoAdModel(VideoAdDataModel videoAdModel) {
    this.videoAdModel = videoAdModel;
  }

  public VmapDataModel getVmapAdModel() {
    return vmapAdModel;
  }

  public void setVmapAdModel(VmapDataModel vmapAdModel) {
    this.vmapAdModel = vmapAdModel;
  }

  public void freeResource() {
    if (videoAdModel != null) {
      videoAdModel.freeResource();
    }
    if (vmapAdModel != null) {
      vmapAdModel.freeResource();
    }
  }
}
