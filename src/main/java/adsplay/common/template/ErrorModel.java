package adsplay.common.template;

import java.util.List;

import io.netty.handler.codec.http.HttpHeaders;

public class ErrorModel implements DataModel {

    private String url = "";

    public ErrorModel(String url) {
        super();
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void freeResource() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getClasspath() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isOutputable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<HttpHeaders> getHttpHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

}
