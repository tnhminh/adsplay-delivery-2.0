package adsplay.common.template;

import org.apache.commons.lang3.StringEscapeUtils;

import adsplay.server.delivery.video.model.DefaultModel;


public class TemplateUtil {

  public static String render(String tplPath, DataModel model) {
    String s = HandlebarsTemplateUtil.execute(tplPath, model);
    return StringEscapeUtils.unescapeHtml4(s);
  }

  public static String render(String tplPath) {
    String s = HandlebarsTemplateUtil.execute(tplPath, new DefaultModel());
    return StringEscapeUtils.unescapeHtml4(s);
  }
}
