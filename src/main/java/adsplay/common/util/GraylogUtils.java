package adsplay.common.util;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import com.adsplay.graylog.enumeration.GrayLevel;
import com.adsplay.graylog.handler.GrayLogHandler;

import adsplay.server.delivery.query.InstreamAdDataUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class GraylogUtils {

    public static void addingGrayLog(HttpServerRequest req, long duration, String deliveryPrefix) {
        String uri = req.uri();
        Map<String, Object> additionalInfos = new HashMap<String, Object>();
        String timeField = "";
        try {
            if ("/favicon.ico".startsWith(uri)) {
                return;
            }

            LogUtil.i("Graylog appending process", "URI API: " + uri, true);

            // Adding general
            additionalInfos.put("RQ.method.name", String.valueOf(Thread.currentThread().getStackTrace()[1].getMethodName()));
            additionalInfos.put("RQ.total.time", duration);
            additionalInfos.put("RQ.api", uri);
            String rqType = uri.substring(0, uri.indexOf("?"));
            additionalInfos.put("RQ.type", rqType.equals("/delivery/zone/") ? rqType.substring(0, rqType.lastIndexOf("/")) : rqType);
            additionalInfos.put("RQ.domain", req.host());

            MultiMap params = req.params();

            // ====== Additional========
            int placementId = StringUtil.safeParseInt(params.get("placement"), 0);
            String ctId = StringUtil.safeString(params.get("ctid"), "");
            String catId = StringUtil.safeString(params.get("catid"), "");
            String uuid = StringUtil.safeString(params.get("uuid"), "");
            String ut = StringUtil.safeString(params.get("ut"), "");
            String cid = StringUtil.safeString(params.get("cid"), "");
            String versionApp = StringUtil.safeString(params.get("verison_app"), "");

            String url = StringUtil.safeString(params.get("url"), "");
            String location = StringUtil.safeString(params.get("loc"), "");
            String adType = StringUtil.safeString(params.get("at"), "");
            String referer = StringUtil.safeString(params.get("referrer"), "");
            String contextTitle = StringUtil.safeString(params.get("cxt"), "");
            String contextKeywords = StringUtil.safeString(params.get("cxkw"), "");
            // =================

            MultiMap reqHeaders = req.headers();

            String useragent = StringUtil.safeString(reqHeaders.get("User-Agent"));
            additionalInfos.put("RQ.dl.userAgent", useragent);

            if (uri.startsWith("/delivery/zone")) {
                long clientTime = StringUtil.safeParseLong(params.get("t"), 0L);
                timeField = "RQ.dl.clientTime";
                additionalInfos.put(timeField, clientTime);
            } else if (uri.startsWith(deliveryPrefix) && !uri.startsWith("/delivery/zone")) {
                long clientTime = getClientTime(params, placementId);
                timeField = "RQ.dl.clientTime";
                additionalInfos.put(timeField, clientTime);

            }

            additionalInfos.put("RQ.dl.placementId", placementId);
            additionalInfos.put("RQ.dl.ctId", ctId);
            additionalInfos.put("RQ.dl.catId", catId);
            additionalInfos.put("RQ.dl.uuid", uuid);
            additionalInfos.put("RQ.dl.ut", ut);
            additionalInfos.put("RQ.dl.cid", cid);

            additionalInfos.put("RQ.dl.url", url);
            additionalInfos.put("RQ.dl.location", location);
            additionalInfos.put("RQ.dl.adType", adType);
            additionalInfos.put("RQ.dl.referer", referer);
            additionalInfos.put("RQ.dl.contextTitle", contextTitle);
            additionalInfos.put("RQ.dl.contextKeywords", contextKeywords);
            additionalInfos.put("RQ.dl.versionApp", versionApp);

            additionalInfos.put("RQ.serverAddr", InetAddress.getLocalHost().getHostName());
        } catch (Exception e) {
            LogUtil.error(e);
            LogUtil.error(uri);
            e.printStackTrace();
        }
        GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "[Request]-[Delivery/Log]-[ALL]", "Request was sent for all type",
                Thread.currentThread().getStackTrace()[1].getLineNumber(),
                String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos, timeField);
    }
    
    private static long getClientTime(MultiMap params, int placementId) {
        long deliveryTime = 0L;
        if (placementId == InstreamAdDataUtil.FPT_PLAY_WEB_VOD) {
            deliveryTime = StringUtil.safeParseLong(params.get("t"), 0L);
        } else {
            deliveryTime = StringUtil.safeParseLong(params.get("t"), 0L) * 1000;
        }
        return deliveryTime;
    }
}
