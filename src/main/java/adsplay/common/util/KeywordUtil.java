package adsplay.common.util;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class KeywordUtil {
	public static String convertUtf8ToAscii(String in) {
		String nfdNormalizedString = Normalizer.normalize(in, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("").replaceAll("đ", "d").replaceAll("Đ", "D")
				.replaceAll(" ", "_").toLowerCase();
	}
}
