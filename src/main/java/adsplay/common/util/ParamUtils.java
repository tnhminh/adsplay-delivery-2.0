package adsplay.common.util;

public class ParamUtils {

    public static final String MOVIE_ID = "movieId";

    public static final String IS_PAYTV = "isPayTV";

    public static final String CON_ID = "conID";

    public static final String COPYRIGHT_ID = "copyrightId";

    public static final String CATEGORY = "catID";
    
    public static final String PARENT_CATEGORY = "parentCat";

    public static final String PAGE_URL = "url";

    public static final String LOCATION = "loc";

    public static final String CONTENT_ID = "ctid";

    public static final String CID = "cid";
}
