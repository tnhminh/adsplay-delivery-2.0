package adsplay.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import rfx.core.util.LogUtil;

public class ParserUtils {

    private static final String COLON = ":";
    private static final String ANY_SPACE = "\\s+";
    private static final String COMMA = ",";

    // {a=x, b=y}
    public static void parseAndUpdateStringToMap(String mapString, Map<String, String> maps, String keyName) {
        if (maps == null || mapString == null) {
            return;
        }
        String value = "";
        value = mapString.substring(1, mapString.length() - 1); // remove curly
        // brackets
        String[] keyValuePairs = value.split(","); // split the string to creat
                                                   // key-value pairs
        Map<String, String> childMap = new HashMap<String, String>();
        for (String pair : keyValuePairs) // iterate over the pairs
        {
            String replacedPair = pair.replaceAll(ANY_SPACE, "").replace("\"", "");
            String[] entry = replacedPair.split("="); // split the pairs to get
                                                      // key and
            // value
            if (entry.length == 1) {
                entry = replacedPair.startsWith("value") ? replacedPair.split(COLON, 2) : replacedPair.split(COLON);
            }
            childMap.put(entry[0].trim(), entry[1].trim()); // add them to the
            // hashmap and trim
            // whitespaces
        }
        if (!childMap.isEmpty()) {
            maps.put(keyName, converMapToJSON(childMap));
        }
    }

    // "[a, b]"
    public static void parseAndAddStringToArray(String stringArr, boolean isNestedObject, Map<String, String> maps, String keyName) {
        if (maps == null || stringArr == null) {
            return;
        }
        StringBuilder strBuilder = new StringBuilder();
        String[] arr = convertStringToArray(stringArr);
        strBuilder.append("[");
        int i = 0;
        String separator = "";
        while (i < arr.length) {
            strBuilder.append(separator);
            String val = (arr[i].indexOf("\"") != -1 ? arr[i].replace("\"", "") : arr[i]).replace("'", "").trim();
            strBuilder.append("\"").append(val).append("\"");
            separator = ",";
            i++;
        }
        strBuilder.append("]");
        maps.put(keyName, strBuilder.toString());
    }

    public static String[] convertStringToArray(String stringArr) {
        String[] arr = null;
        if (stringArr != null) {
            arr = stringArr.replace("[", "").replaceAll("]", "").replaceAll("\\s+", "").replace("'", "").split(",");
        }
        return arr;
    }

    public static String standalizeRedisMapToJSON(Map<String, String> maps) {
        maps.forEach((key, value) -> {
            try {
                String strVal = value.trim();
                if (strVal.startsWith("{") || strVal.endsWith("}")) {
                    parseAndUpdateStringToMap(strVal, maps, key);
                } else if (strVal.startsWith("[{") || strVal.endsWith("]}")) {
                    strVal = strVal.replace("'", "\"");
                    maps.put(key, strVal);
                    // Nested object in array
                } else if (strVal.startsWith("[") || strVal.endsWith("]")) {
                    parseAndAddStringToArray(strVal, true, maps, key);
                }
            } catch (Exception e) {
                LogUtil.error(e);
            }
        });
        return converMapToJSON(maps);
    }

    public static String converMapToJSON(Map<String, ? extends Object> maps) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(maps).replace("\"{", "{").replace("}\"", "}").replace("\"[", "[").replace("]\"", "]").replace("\\", "");
    }
    
    public static String convertListToStringArray(List<String> fwIds) {
        StringBuilder wVals = new StringBuilder();
        wVals.append("[");
        String[] separatorVal = new String[1];
        separatorVal[0] = "";
        fwIds.forEach(flightId -> {
            wVals.append(separatorVal[0]);
            wVals.append(flightId);
            separatorVal[0] = COMMA;
        });
        wVals.append("]");
        return wVals.toString();
    }

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        // parsingStringToArray("[a, b, c, d]", true, new HashMap<String,
        // Object>(), "keyArr");
        parseAndUpdateStringToMap("{a=x, b=y}", map, "newKey");
        System.out.println(converMapToJSON(map));
    }
}
