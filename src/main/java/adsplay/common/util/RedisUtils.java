package adsplay.common.util;

public class RedisUtils {
    
    public static final String EMPTY_VAL = "0";
    
    public static final String TOTAL = "total";
    
    public static final String DAILY = "daily";
    
    public static final String BOOKING = "booking";
    
    public static final String UNDERLINE = "_";
    
    public static final String BK = "bk";
}
