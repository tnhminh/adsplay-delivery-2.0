package adsplay.common.util;

import static io.vertx.core.http.HttpHeaders.COOKIE;
import static io.vertx.core.http.HttpHeaders.USER_AGENT;

import java.net.URLDecoder;
import java.util.Set;

import adsplay.server.delivery.RoutingHandler;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.util.HttpRequestUtil;
import rfx.core.util.SecurityUtil;

public class UserUtil {

	public static final String APLUUID = "apluuid";

	public static String getUUID(HttpServerRequest req) {
		String uuid = null;
		String cookieString = req.headers().get(COOKIE);
		// System.out.println(cookieString);
		if (cookieString != null) {
			try {
				cookieString = URLDecoder.decode(cookieString, "UTF-8");
				Set<Cookie> cookies = ServerCookieDecoder.LAX.decode(cookieString);
				// System.out.println("cookies "+cookies);
				for (Cookie cookie : cookies) {
					String name = cookie.name();
					if (name.equals(APLUUID)) {
						uuid = cookie.value();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return uuid;
	}

	public static String setCookieUUID(HttpServerRequest req, HttpServerResponse resp) {
		String uuid = getUUID(req);
		if (uuid == null) {
			MultiMap headers = req.headers();
			String userAgent = headers.get(USER_AGENT);
			String ip = HttpRequestUtil.getRemoteIP(req);
			uuid = SecurityUtil.sha1(userAgent + ip + System.currentTimeMillis()).substring(0, 24);

			Cookie uuidCookie = new DefaultCookie(APLUUID, uuid);
			uuidCookie.setHttpOnly(true);
			uuidCookie.setMaxAge(RoutingHandler.COOKIE_AGE_10_YEAR);
			uuidCookie.setPath("/");
			uuidCookie.setDomain(RoutingHandler.ADSPLAY_NET);
			resp.headers().add("Set-Cookie", ServerCookieEncoder.LAX.encode(uuidCookie));
		}
		System.out.println("UUID: " + uuid);
		return uuid;
	}
}
