package adsplay.delivery.redis.cache.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adsplay.common.util.DateTimeUtil;
import adsplay.common.util.ParserUtils;
import adsplay.common.util.RedisUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.function.Targeting;
import adsplay.delivery.redis.cache.model.DateRange;
import rfx.core.util.StringUtil;

public abstract class FlightFilter implements Targeting {

    public boolean isAvailableBooking(String flightId, String timeZone) {

        // Current date Using UTC time - the reason is redis daily booking
        // using UTC.
        String dailyByDate = DateTimeUtil.getCurrentDateString("UTC", DateTimeUtil.YYYYMMDD, true, 0, 0, 0);

        // Get from cache db
        Map<String, String> bookingVals = RedisCache.hget("bookings_" + flightId, RedisPool.BOOKING);
        String bkType = StringUtil.safeString(bookingVals.get("type"), "impression");
        int dailyVal = StringUtil.safeParseInt(bookingVals.get("daily"), 0);
        int totalVal = StringUtil.safeParseInt(bookingVals.get("total"), 0);

        //
        String totalBookingKey = "";
        String dailyBookingKey = "";
        if (!bkType.isEmpty()) {
            totalBookingKey = RedisUtils.BK + RedisUtils.UNDERLINE + flightId + RedisUtils.UNDERLINE + RedisUtils.TOTAL
                    + RedisUtils.UNDERLINE + bkType;
            dailyBookingKey = RedisUtils.BK + RedisUtils.UNDERLINE + flightId + RedisUtils.UNDERLINE + RedisUtils.DAILY
                    + RedisUtils.UNDERLINE + dailyByDate + RedisUtils.UNDERLINE + bkType;
        }

        // Get from tracking realtime
        int totalBookingVal = StringUtil.safeParseInt(RedisCache.get(totalBookingKey, RedisPool.BOOKING), 0);
        int dailyBookingVal = StringUtil.safeParseInt(RedisCache.get(dailyBookingKey, RedisPool.BOOKING), 0);

        if (totalVal > totalBookingVal && dailyVal > dailyBookingVal) {
            return true;
        }
        return false;
    }

    public boolean isIncludeHour(String hours, String timeZone) {
        if (checkingEmptyVal(hours)) {
            return true;
        }
        String[] arr = ParserUtils.convertStringToArray(hours);

        List<String> fParams = Arrays.asList(arr);
        String currentHour = DateTimeUtil.getCurrentHour(timeZone, true);
        return fParams.contains(currentHour);
    }

    private boolean checkingEmptyVal(String string) {
        return string.isEmpty() || "null".equals(string) || "None".equals(string);
    }

    public boolean isIncludeDateOfWeek(String dateOfWeeks, String timeZone) {
        if (checkingEmptyVal(dateOfWeeks)){
            return true;
        }
        String[] arr = ParserUtils.convertStringToArray(dateOfWeeks);
        List<String> fParams = Arrays.asList(arr);
        String currentDateOfWeek = DateTimeUtil.getCurrentDateOfWeek(timeZone, true);
        return fParams.contains(currentDateOfWeek);
    }

    public boolean isIncludeDateRange(String dateRanges, String timeZone) {
        if (checkingEmptyVal(dateRanges)){
            return true;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        List<DateRange> listDateRanges = gson.fromJson(dateRanges, new TypeToken<List<DateRange>>() {
        }.getType());
        Date date = DateTimeUtil.getCurrentDateTimeZone(timeZone, true).getTime();
        boolean isIncluded[] = new boolean[1];
        isIncluded[0] = false;

        Iterator<DateRange> iterator = listDateRanges.iterator();
        while (iterator.hasNext()) {
            if (isIncluded[0] == true)
                break;
            DateRange dateRange = iterator.next();
            if (date.before(new Date(dateRange.getTo())) && date.after(new Date(dateRange.getFrom()))) {
                isIncluded[0] = true;
            }
        }
        return isIncluded[0];
    }

    public boolean isIncludeDateRangeWithinComparing(String dateRanges, String timeZone) {
        if (checkingEmptyVal(dateRanges)){
            return true;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.YYYY_MM_DD);

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String currentDate = DateTimeUtil.convertDateToStringFormat(new Date(), timeZone, DateTimeUtil.YYYY_MM_DD);

        Date dateAfterParsing = null;
        boolean isIncluded[] = new boolean[1];
        try {
            dateAfterParsing = dateFormat.parse(currentDate);

            List<DateRange> listDateRanges = gson.fromJson(dateRanges, new TypeToken<List<DateRange>>() {
            }.getType());

            isIncluded[0] = false;
            Iterator<DateRange> iterator = listDateRanges.iterator();
            while (iterator.hasNext()) {
                if (isIncluded[0] == true)
                    break;
                DateRange dateRange = iterator.next();
                if (dateAfterParsing.compareTo(dateFormat.parse(dateRange.getTo())) <= 0
                        && dateAfterParsing.compareTo(dateFormat.parse(dateRange.getFrom())) >= 0) {
                    isIncluded[0] = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isIncluded[0];
    }

    public static void main(String[] args) throws ParseException {

        // SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        //
        // TimeZone tz = TimeZone.getTimeZone("Asia/Singapore");
        // sdf.setTimeZone(tz);
        //
        // Calendar currentDate = Calendar.getInstance();
        // currentDate.setTimeInMillis(1516377600000L);
        //
        // System.out.println(currentDate.get(Calendar.DAY_OF_MONTH) + "/" +
        // (currentDate.get(Calendar.MONTH) + 1) + "/"
        // + currentDate.get(Calendar.YEAR));
        // System.out.println(currentDate);
        //
        // String dateFormat = sdf.format(currentDate.getTime());
        // System.out.println(dateFormat);
        //
        // sdf.setTimeZone(TimeZone.getDefault());
        // Date dateAfterParsing = sdf.parse(dateFormat);
        // System.out.println(dateAfterParsing);
        //
        // System.out.println(dateAfterParsing.compareTo(new
        // Date("2018/01/20")));

        // isIncludeDateRangeWithinComparing("[{'to': '2018/02/20', 'from':
        // '2018/01/20'}, {'to': '2018/02/28', 'from': '2018/02/23'}, {'to':
        // '2018/03/31', 'from': '2018/03/10'}]", "Asia/Ho_Chi_Minh");
        // isAvailableBooking("1", "Asia/Ho_Chi_Minh");
    }
}
