package adsplay.delivery.redis.cache.filter;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import adsplay.common.util.ParserUtils;
import adsplay.common.util.RedisUtils;

public class FptFlightFilter extends FlightFilter {

    @SuppressWarnings("unchecked")
    @Override
    public boolean isTargetingOn(String vals, Object rqParam) {

        // Checking param val
        if (rqParam == null) {
            return true;
        } else if (rqParam instanceof String) {
            if ("0".equals((String) rqParam)) {
                return true;
            }
            ;
        } else if (rqParam instanceof List<?>) {
            if (((List<String>) rqParam).isEmpty()) {
                return true;
            }
        }

        // Checking vals in redis
        String[] arrFVals = ParserUtils.convertStringToArray(vals);
        if (arrFVals.length == 0) {
            return true;
        }

        List<String> fParams = Arrays.asList(arrFVals);

        if (rqParam instanceof String) {
            return isContainStringField(rqParam, fParams);
        } else if (rqParam instanceof List<?>) {
            return isContainArrayField(rqParam, fParams);
        }
        return false;
    }

    /**
     * 
     * @param rqParam
     * @param fParams
     * @return
     */
    protected boolean isContainStringField(Object rqParam, List<String> fParams) {
        String stringParam = (String) rqParam;
        if (RedisUtils.EMPTY_VAL.equals(stringParam)) {
            return true;
        }
        return fParams.contains(stringParam);
    }

    /**
     * 
     * @param rqParam
     * @param fParams
     * @return
     */
    @SuppressWarnings("unchecked")
    protected boolean isContainArrayField(Object rqParam, List<String> fParams) {
        List<String> listParam = (List<String>) rqParam;
        if (listParam.contains(RedisUtils.EMPTY_VAL)) {
            return true;
        }
        return CollectionUtils.containsAny((Collection<?>) rqParam, (Collection<?>) fParams);
    }

}
