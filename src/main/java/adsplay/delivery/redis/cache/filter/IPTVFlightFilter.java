package adsplay.delivery.redis.cache.filter;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import adsplay.common.util.ParserUtils;
import adsplay.common.util.RedisUtils;

public class IPTVFlightFilter extends FlightFilter {

    @SuppressWarnings("unchecked")
    @Override
    public boolean isTargetingOn(String vals, Object rqParam) {

        // Checking param val
        if (rqParam == null) {
            return true;
        }

        String[] arrFlightParam = ParserUtils.convertStringToArray(vals);

        boolean isEmptyInFlight = false;
        if (arrFlightParam.length == 0) {
            isEmptyInFlight = true;
        }

        List<String> fParams = Arrays.asList(arrFlightParam);
        if (rqParam instanceof String) {
            return isContainStringField(rqParam, isEmptyInFlight, fParams);
        } else if (rqParam instanceof List<?>) {
            return isContainArrayField(rqParam, isEmptyInFlight, fParams);
        }
        return false;
    }

    protected boolean isContainStringField(Object rqParam, boolean isEmptyInFlight, List<String> fParams) {
        String val = (String) rqParam;
        if (isEmptyInFlight) {
            return RedisUtils.EMPTY_VAL.equals(val);
        }
        // if (EMPTY_VAL.equals(val)) {
        // return isEmptyInFlight;
        // }
        return fParams.contains(val);
    }

    @SuppressWarnings("unchecked")
    protected boolean isContainArrayField(Object rqParam, boolean isEmptyInFlight, List<String> fParams) {
        List<String> vals = (List<String>) rqParam;
        if (isEmptyInFlight) {
            return vals.contains(RedisUtils.EMPTY_VAL);
        }
        // if (vals.contains(EMPTY_VAL)) {
        // return isEmptyInFlight;
        // }
        return CollectionUtils.containsAny((Collection<?>) rqParam, (Collection<?>) fParams);
    }
}
