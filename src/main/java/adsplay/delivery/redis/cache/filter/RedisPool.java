package adsplay.delivery.redis.cache.filter;

public enum RedisPool {
    BOOKING, CREATIVE, FLIGHT, PLACEMENT, FLIGHT_WEIGHT
}
