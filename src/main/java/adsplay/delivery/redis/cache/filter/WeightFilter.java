package adsplay.delivery.redis.cache.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.model.Flight;

public class WeightFilter {

    public static Flight weightChecking(List<String> newFIds, Map<String, Flight> flightIdObjPair, StringBuilder wKey) {

        String flightWeightArr = RedisCache.getFlightWeight(wKey.toString());
        
        String[] fwRedisArr = ParserUtils.convertStringToArray(flightWeightArr);

        List<String> fwIdsInCache;
        Flight finalFlight = null;
        boolean isNeedToDelete = false;
        boolean isNeedToSet = true;
        // Key doesn't exist
        List<String> finalFlightIds = new ArrayList<String>();
        if (fwRedisArr == null || fwRedisArr.length == 0) {
            // Calculate flight's weight
            Collections.shuffle(newFIds);
            finalFlight = flightIdObjPair.get(newFIds.remove(0));
            finalFlightIds.addAll(newFIds);
            if (newFIds.isEmpty()) {
                isNeedToSet = false;
            }
        } else {
            fwIdsInCache = new ArrayList<String>(Arrays.asList(fwRedisArr));
            Collections.shuffle(fwIdsInCache);
            finalFlight = flightIdObjPair.get(fwIdsInCache.remove(0));
            finalFlightIds.addAll(fwIdsInCache);
            if (fwIdsInCache.isEmpty()) {
                isNeedToDelete = true;
            }
        }
        // Update in redis cache
        updateInRedis(finalFlightIds, wKey, isNeedToDelete, isNeedToSet);

        return finalFlight;
    }

    private static void updateInRedis(List<String> fwIds, StringBuilder wKey, boolean isNeedToDelete, boolean isNeedToSet) {
        if (isNeedToDelete) {
            RedisCache.delFlightWeight(wKey.toString());
        } else if (isNeedToSet) {
            String wVals = ParserUtils.convertListToStringArray(fwIds);
            // Then setting weight flight to redis
            RedisCache.setFlightWeight(wKey.toString(), wVals);
        }
    }
}
