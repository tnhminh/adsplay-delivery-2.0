package adsplay.delivery.redis.cache.function;

public interface BookingFilter {

    boolean isAvailableBooking();

}
