package adsplay.delivery.redis.cache.function;

import java.util.List;

public interface CategoryFilter {

    boolean isContainCategory(List<String> categories);

}
