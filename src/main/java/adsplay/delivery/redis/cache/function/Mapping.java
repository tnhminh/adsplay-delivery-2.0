package adsplay.delivery.redis.cache.function;

import java.util.List;
import java.util.Map;

import io.vertx.core.MultiMap;

public interface Mapping {

    void fillParamsMapping(MultiMap params, List<String> pmIds, Map<String, Object> paramsAsMap);
}
