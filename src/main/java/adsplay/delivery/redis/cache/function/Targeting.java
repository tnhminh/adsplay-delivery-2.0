package adsplay.delivery.redis.cache.function;

public interface Targeting {
    boolean isTargetingOn(String flightParam, Object rqParam);
}
