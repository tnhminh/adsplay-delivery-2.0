package adsplay.delivery.redis.cache.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adsplay.delivery.redis.cache.function.Mapping;
import adsplay.delivery.redis.cache.injector.IPTVModelInjector;
import adsplay.delivery.redis.cache.injector.InjectorExecutor;
import adsplay.delivery.redis.cache.injector.RedisModelInjector;
import adsplay.delivery.redis.cache.mapping.IPTVParamMapping;
import io.vertx.core.MultiMap;

public class AdsplayIPTVHandler {

    private static RedisModelInjector iptvInjector = new IPTVModelInjector();

    static Mapping mapping = new IPTVParamMapping();

    public static String handle(MultiMap params, List<String> pmIds) {
        // ========GET ALL PARAMS FPT-PLAY===================
        Map<String, Object> paramsAsMap = new HashMap<String, Object>();

        mapping.fillParamsMapping(params, pmIds, paramsAsMap);

        return InjectorExecutor.start(pmIds, paramsAsMap, iptvInjector);
    }
}
