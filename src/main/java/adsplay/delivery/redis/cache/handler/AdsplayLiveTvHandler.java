package adsplay.delivery.redis.cache.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.injector.FptplayModelInjector;
import adsplay.delivery.redis.cache.injector.RedisModelInjector;
import adsplay.delivery.redis.cache.model.Creative;
import adsplay.delivery.redis.cache.model.Flight;
import adsplay.delivery.redis.cache.thirdpt.FptPlayDataUtils;
import io.vertx.core.MultiMap;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class AdsplayLiveTvHandler {

    private static RedisModelInjector fptPlayinjector = new FptplayModelInjector();

    public static String handle(MultiMap params) {
        try {
            Gson gson = new Gson();

            int flightId = StringUtil.safeParseInt(params.get("flightId"), 0);

            Map<String, String> redisMapping = RedisCache.hget("flight" + "_" + flightId, RedisPool.FLIGHT);
            List<Creative> crts = new ArrayList<Creative>();

            Flight selectedFlight = gson.fromJson(ParserUtils.standalizeRedisMapToJSON(redisMapping), Flight.class);
            selectedFlight.setFlightId(flightId);
            if (selectedFlight != null) {
                selectedFlight.getCreativeIds().forEach(crtId -> {
                    Creative creative = (Creative) fptPlayinjector.injectToPersistent(Creative.KEY_NAME, String.valueOf(crtId),
                            new HashMap<>(), RedisPool.CREATIVE);
                    if (creative == null) {
                        return;
                    }
                    System.out.println("Valid creative with flightId: " + flightId + ", creativeId: " + creative.getCreativeId());
                    crts.add(creative);
                });

                // crt have preroll, midroll, postroll with ordering
                return FptPlayDataUtils.fillEventTracking(new HashMap<String, Object>(), crts, selectedFlight, 0, true);
            }
        } catch (Exception e) {
            LogUtil.error(AdsplayLiveTvHandler.class, "handle()","Failure when process on liveTV !!!");
            e.printStackTrace();
        }
        return "";
    }
}
