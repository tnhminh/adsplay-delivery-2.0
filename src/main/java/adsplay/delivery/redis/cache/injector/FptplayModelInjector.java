package adsplay.delivery.redis.cache.injector;

import java.util.Map;

import com.google.gson.Gson;

import adsplay.common.util.ParamUtils;
import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.filter.FlightFilter;
import adsplay.delivery.redis.cache.filter.FptFlightFilter;
import adsplay.delivery.redis.cache.model.Flight;
import rfx.core.util.StringUtil;

public class FptplayModelInjector extends RedisModelInjector {

    FlightFilter filter = new FptFlightFilter();

    @Override
    public Object checkingAndInjectFlight(String id, Map<String, Object> params, Gson gson, Map<String, String> redisMapping) {
        Flight flightObj = null;

        // Checking all params before injecting to object
        String catIds = StringUtil.safeString(redisMapping.get(CATEGORIES), "");
        Object catRQ = params.get(ParamUtils.CATEGORY);

        String locids = StringUtil.safeString(redisMapping.get(LOCATION), "");
        Object locRQ = params.get(ParamUtils.LOCATION);

        String hours = StringUtil.safeString(redisMapping.get(HOURS), "");

        String dateOfWeeks = StringUtil.safeString(redisMapping.get(DAY_WEEKS), "");

        String tzone = StringUtil.safeString(redisMapping.get(TIMEZONE), "");

        String dateRanges = StringUtil.safeString(redisMapping.get(DATE_RANGE), "");

        if (isValidFlight(id, catIds, catRQ, locids, locRQ, hours, dateOfWeeks, tzone, dateRanges)) {
            flightObj = gson.fromJson(ParserUtils.standalizeRedisMapToJSON(redisMapping), Flight.class);
            flightObj.setFlightId(Integer.valueOf(id).intValue());
        }
        return flightObj;
    }

    private boolean isValidFlight(String id, String catIds, Object catRQ, String locids, Object locRQ, String hours, String dateOfWeeks,
            String tzone, String dateRanges) {

        boolean availableBooking = filter.isAvailableBooking(id, tzone);
        boolean targetingOnCat = filter.isTargetingOn(catIds, catRQ);
        boolean includeHour = filter.isIncludeHour(hours, tzone);
        boolean includeDateOfWeek = filter.isIncludeDateOfWeek(dateOfWeeks, tzone);
        boolean includeDateRangeWithinComparing = filter.isIncludeDateRangeWithinComparing(dateRanges, tzone);
        boolean targetingOnLoc = filter.isTargetingOn(locids, locRQ);

        System.out.println("FlightId: " + id + ",\n availableBooking: " + availableBooking + "\n, targetingOnCat: " + targetingOnCat
                + "\n, includeHour: " + includeHour + "\n, includeDateOfWeek: " + includeDateOfWeek
                + "\n, includeDateRangeWithinComparing: " + includeDateRangeWithinComparing + "\n, targetingOnLoc: " + targetingOnLoc);

        return availableBooking && targetingOnCat && includeHour && includeDateOfWeek && includeDateRangeWithinComparing && targetingOnLoc;
    }
}
