package adsplay.delivery.redis.cache.injector;

import java.util.Map;

import com.google.gson.Gson;

import adsplay.common.util.ParamUtils;
import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.filter.FlightFilter;
import adsplay.delivery.redis.cache.filter.IPTVFlightFilter;
import adsplay.delivery.redis.cache.model.Flight;
import rfx.core.util.StringUtil;

public class IPTVModelInjector extends RedisModelInjector {

    FlightFilter filter = new IPTVFlightFilter();

    @Override
    public Object checkingAndInjectFlight(String id, Map<String, Object> params, Gson gson, Map<String, String> redisMapping) {

        Flight flightObj = null;

        // Checking all params before injecting to object
        String catIds = StringUtil.safeString(redisMapping.get(CATEGORIES), "");
        Object catRQ = params.get(ParamUtils.CATEGORY);

        String locIds = StringUtil.safeString(redisMapping.get(LOCATION), "");
        Object locRQ = params.get(ParamUtils.LOCATION);

        String sourceProviderIds = StringUtil.safeString(redisMapping.get(SOURCE_PROVIDER), "");
        Object sourceParam = params.get(ParamUtils.COPYRIGHT_ID);

        Object movieIdRQ = params.get(ParamUtils.MOVIE_ID);

        String hours = StringUtil.safeString(redisMapping.get(HOURS), "");

        String dateOfWeeks = StringUtil.safeString(redisMapping.get(DAY_WEEKS), "");

        String dateRanges = StringUtil.safeString(redisMapping.get(DATE_RANGE), "");

        System.out.println("catID:" + catRQ.toString() + ", loc:" + locRQ + ", " + "sourcePro:" + sourceParam);
        String tzone = redisMapping.get(TIMEZONE);
        if (isValidFlight(id, catIds, catRQ, locIds, locRQ, sourceProviderIds, sourceParam, movieIdRQ, hours, dateOfWeeks, dateRanges,
                tzone)) {
            flightObj = gson.fromJson(ParserUtils.standalizeRedisMapToJSON(redisMapping), Flight.class);
            flightObj.setFlightId(Integer.valueOf(id).intValue());
        }
        return flightObj;
    }

    private boolean isValidFlight(String id, String catIds, Object catRQ, String locIds, Object locRQ, String sourceProviderIds,
            Object sourceParam, Object movieIdRQ, String hours, String dateOfWeeks, String dateRanges, String tzone) {

        boolean availableBooking = filter.isAvailableBooking(id, tzone);
        boolean targetingOnCat = filter.isTargetingOn(catIds, catRQ);
        boolean includeHour = filter.isIncludeHour(hours, tzone);
        boolean includeDateOfWeek = filter.isIncludeDateOfWeek(dateOfWeeks, tzone);
        boolean includeDateRangeWithinComparing = filter.isIncludeDateRangeWithinComparing(dateRanges, tzone);
        boolean targetingOnLoc = filter.isTargetingOn(locIds, locRQ);
        boolean targetingOnSourceProvider = filter.isTargetingOn(sourceProviderIds, sourceParam);
        boolean targetingOnMvId = filter.isTargetingOn(catIds, movieIdRQ);

        System.out.println("FlightId: " + id + ",\n availableBooking: " + availableBooking + "\n, targetingOnCat: " + targetingOnCat
                + "\n, includeHour: " + includeHour + "\n, includeDateOfWeek: " + includeDateOfWeek
                + "\n, includeDateRangeWithinComparing: " + includeDateRangeWithinComparing + "\n, targetingOnLoc: " + targetingOnLoc
                + "\n, targetingOnSourceProvider: " + targetingOnSourceProvider + "\n, targetingOnMvId: " + targetingOnMvId);

        return availableBooking && targetingOnCat && includeHour && includeDateOfWeek && includeDateRangeWithinComparing && targetingOnLoc
                && targetingOnSourceProvider
                // Checking movieId by using categories
                && targetingOnMvId;
    }
}
