package adsplay.delivery.redis.cache.injector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.filter.WeightFilter;
import adsplay.delivery.redis.cache.model.Creative;
import adsplay.delivery.redis.cache.model.Flight;
import adsplay.delivery.redis.cache.model.Placement;
import adsplay.delivery.redis.cache.thirdpt.FptPlayDataUtils;
import adsplay.delivery.redis.cache.thirdpt.IPTVDataUtils;
import adsplay.server.delivery.query.IPTVDataUtil;

public class InjectorExecutor {

    private static final String DASH = "_";

    private static final String WEIGHT_PREFIX = "w_";

    public static String start(List<String> pmIds, Map<String, Object> paramsAsMap, RedisModelInjector injector) {

        List<String> newFIds = new ArrayList<String>();

        Map<String, Flight> flightIdObjPair = new HashMap<String, Flight>();

        StringBuilder wKey = new StringBuilder();
        wKey.append(WEIGHT_PREFIX);

        List<Placement> placements = new ArrayList<Placement>();
        pmIds.forEach(pmId -> {
            Placement pmObj = (Placement) injector.injectToPersistent(Placement.KEY_NAME, pmId, new HashMap<>(), RedisPool.PLACEMENT);
            if (pmObj != null) {
                placements.add(pmObj);

                String[] separatorKey = new String[1];
                separatorKey[0] = "";
                pmObj.getFlights().forEach(flightId -> {

                    Flight flight = (Flight) injector.injectToPersistent(Flight.KEY_NAME, String.valueOf(flightId), paramsAsMap, RedisPool.FLIGHT);

                    // Check all condition for flight
                    if (flight != null) {
                        wKey.append(separatorKey[0]);
                        wKey.append(flightId);
                        separatorKey[0] = DASH;

                        int i = 0;
                        while (i < flight.getWeight()) {
                            newFIds.add(String.valueOf(flightId));
                            i++;
                        }
                        flightIdObjPair.put(String.valueOf(flight.getFlightId()), flight);
                    }
                });
            }
            System.out.println("valid flight size:" + flightIdObjPair.size());
        });

        // Empty list flight
        if (flightIdObjPair.isEmpty()) {
            return "";
        }

        List<Creative> crts = new ArrayList<Creative>();

        Flight selectedFlight = WeightFilter.weightChecking(newFIds, flightIdObjPair, wKey);
        int flightId = selectedFlight.getFlightId();
        System.out.println("Selected flight id:" + flightId);
        if (selectedFlight != null) {
            selectedFlight.getCreativeIds().forEach(crtId -> {
                Creative creative = (Creative) injector.injectToPersistent(Creative.KEY_NAME, String.valueOf(crtId), paramsAsMap, RedisPool.CREATIVE);
                if (creative == null) {
                    return;
                }
                System.out.println("Valid creative with flightId: " +flightId + ", creativeId: "  + creative.getCreativeId());
                crts.add(creative);
            });

            int mainPmId = Integer.valueOf(pmIds.get(0)).intValue();

            if (mainPmId != IPTVDataUtil.placementId) {
                // crt have preroll, midroll, postroll with ordering
                return FptPlayDataUtils.fillEventTracking(paramsAsMap, crts, selectedFlight, mainPmId);
            } else {
                // For IPTV - preroll or midroll only
                return IPTVDataUtils.fillEventTracking(paramsAsMap, crts, selectedFlight, mainPmId);
            }
        }
        return "";
    }
}
