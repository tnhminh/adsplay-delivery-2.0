package adsplay.delivery.redis.cache.injector;

import java.util.Map;

import com.google.gson.Gson;

import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.model.Creative;
import adsplay.delivery.redis.cache.model.Flight;
import adsplay.delivery.redis.cache.model.Placement;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public abstract class RedisModelInjector {

    protected static final String DATE_RANGE = "date_range";
    protected static final String DAY_WEEKS = "day_weeks";
    protected static final String HOURS = "hours";
    protected static final String TIMEZONE = "timezone";
    protected static final String SOURCE_PROVIDER = "source_providers";
    protected static final String LOCATION = "location";
    protected static final String CATEGORIES = "categories";
    
    public RedisModelInjector() {
    }

    public Object injectToPersistent(String prefix, String id, Map<String, Object> params, RedisPool redisPool) {
        try {
            Gson gson = new Gson();
            Map<String, String> redisMapping = RedisCache.hget(prefix + "_" + id, redisPool);
            if (redisMapping.isEmpty()) {
                return null;
            }
            if (Placement.KEY_NAME.equals(prefix)) {
                return injectPlacement(gson, redisMapping);
            } else if (Flight.KEY_NAME.equals(prefix)) {
                return checkingAndInjectFlight(id, params, gson, redisMapping);
            } else if (Creative.KEY_NAME.equals(prefix)) {
                return injectCreative(id, gson, redisMapping);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.error(e);
        }
        return null;
    }

    private Object injectPlacement(Gson gson, Map<String, String> redisMapping) {
        Placement pmObj = null;
        try {
            String flightIds = StringUtil.safeString(redisMapping.get("flights"), "");
            if (!flightIds.isEmpty()) {
                pmObj = gson.fromJson(ParserUtils.standalizeRedisMapToJSON(redisMapping), Placement.class);
            }
        } catch (Exception e) {
            LogUtil.error(RedisModelInjector.class, "injectPlacement()", "Error when inject placement instance !!!");
            e.printStackTrace();
        }
       
        return pmObj;
    }

    private Object injectCreative(String id, Gson gson, Map<String, String> redisMapping) {
        Creative crtObj = gson.fromJson(ParserUtils.standalizeRedisMapToJSON(redisMapping), Creative.class);
        crtObj.setCreativeId(Integer.valueOf(id).intValue());
        return crtObj;
    }

    public abstract Object checkingAndInjectFlight(String id, Map<String, Object> params, Gson gson, Map<String, String> redisMapping);
}
