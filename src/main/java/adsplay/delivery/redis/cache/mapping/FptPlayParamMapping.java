package adsplay.delivery.redis.cache.mapping;

import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import adsplay.common.util.ParamUtils;
import adsplay.common.util.ParserUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.function.Mapping;
import adsplay.server.delivery.handler.VideoTvcAdHandler;
import io.vertx.core.MultiMap;
import rfx.core.util.StringUtil;

public class FptPlayParamMapping implements Mapping {

    @Override
    public void fillParamsMapping(MultiMap params, List<String> pmIds, Map<String, Object> paramsAsMap) {

        paramsAsMap.put(ParamUtils.IS_PAYTV, false);

        String urlPage = StringUtil.safeString(params.get(ParamUtils.PAGE_URL), NO_DATA);

        // get content id of video or live channel
        String contentId = StringUtil.safeString(params.get(ParamUtils.CID), NO_DATA);
        
        String location = StringUtil.safeString(params.get(ParamUtils.LOCATION), NO_DATA);
        paramsAsMap.put(ParamUtils.LOCATION, location);

        if (NO_DATA.equals(contentId)) {
            contentId = StringUtil.safeString(params.get(ParamUtils.CONTENT_ID), NO_DATA);
            if (NO_DATA.equals(contentId)) {
                contentId = VideoTvcAdHandler.getContentIdFromURL(urlPage);
            }
        }
        
        // Finally content id
        paramsAsMap.put(ParamUtils.CONTENT_ID, contentId);
        
        String structureIds  = RedisCache.get("ctc_" + contentId, RedisPool.CREATIVE);
        
        String[] strArr = ParserUtils.convertStringToArray(structureIds);
        List<String> strStructureList = null;
        if (strArr != null){
            strStructureList = Arrays.asList(strArr);
        }

        paramsAsMap.put(ParamUtils.CATEGORY, strStructureList);
    }

}
