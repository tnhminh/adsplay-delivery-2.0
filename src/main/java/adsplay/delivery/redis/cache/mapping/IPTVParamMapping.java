package adsplay.delivery.redis.cache.mapping;

import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import adsplay.common.util.ParamUtils;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.function.Mapping;
import io.vertx.core.MultiMap;
import rfx.core.util.StringUtil;

public class IPTVParamMapping implements Mapping {

    @Override
    public void fillParamsMapping(MultiMap params, List<String> pmIds, Map<String, Object> paramsAsMap) {

        // ========GET ALL PARAMS PAYTV===================

        paramsAsMap.put(ParamUtils.IS_PAYTV, true);

        String catId = StringUtil.safeString(params.get(ParamUtils.CATEGORY), NO_DATA);
        List<String> categories = new ArrayList<String>();
        
        String localCatID = RedisCache.get("category_" + catId, RedisPool.CREATIVE);
        categories.add(localCatID);

        paramsAsMap.put(ParamUtils.CATEGORY, categories);
        
        String conId = StringUtil.safeString(params.get(ParamUtils.CON_ID), NO_DATA);
        paramsAsMap.put(ParamUtils.LOCATION, conId);

        String copyrightId = StringUtil.safeString(params.get(ParamUtils.COPYRIGHT_ID), NO_DATA);
        paramsAsMap.put(ParamUtils.COPYRIGHT_ID, copyrightId);

        String movieId = StringUtil.safeString(params.get(ParamUtils.MOVIE_ID), NO_DATA);
        paramsAsMap.put(ParamUtils.MOVIE_ID, movieId);

    }

}
