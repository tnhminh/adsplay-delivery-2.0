package adsplay.delivery.redis.cache.thirdpt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.common.Ad3rdRequestUtil;
import adsplay.common.VASTXmlParser;
import adsplay.common.VASTXmlParser.MediaFile;
import adsplay.common.VASTXmlParser.VastXmlData;
import adsplay.common.template.AdModel;
import adsplay.common.template.TemplateUtil;
import adsplay.common.util.ParamUtils;
import adsplay.delivery.redis.cache.model.Creative;
import adsplay.delivery.redis.cache.model.Flight;
import adsplay.delivery.redis.cache.model.Placement;
import adsplay.server.delivery.query.InstreamVideoAdFactory;
import adsplay.server.delivery.video.model.AdBreak;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.server.delivery.video.model.VmapDataModel;
import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class FptPlayDataUtils {

    private static final String LOCAL_FILE = "local_file";

    public static final String VMAP_3rd_AD = "delivery-ads/vmap-multi-slot-3rd-parties.xml";

    public static final String VAST2_3rd_TEMPLATE_PATH = "delivery-ads/vast2-third-parties.xml";

    public static final String VAST2_TEMPLATE_PATH = "delivery-ads/vast2.xml";

    public static final String VAST3_NATIVE_MOBILE = "delivery-ads/vast3.xml";

    public static final String VMAP_NATIVE_MOBILE = "delivery-ads/vmap.xml";

    public static final String VMAP_VPAID_AD = "delivery-ads/vmap-vpaid.xml";

    public static final String NO_AD_XML = "delivery-ads/no-ads-vast2.xml";

    public static String fillEventTracking(Map<String, Object> paramsAsMap, List<Creative> crts, Flight selectedFlight,
            int mainPlacementId) {
        return fillEventTracking(paramsAsMap, crts, selectedFlight, mainPlacementId, false);
    }

    public static String fillEventTracking(Map<String, Object> paramsAsMap, List<Creative> crts, Flight selectedFlight, int mainPlacementId,
            boolean isLiveTV) {

        String xml = "";
        AdModel model = new AdModel();
        // For FPTPlay
        // 202 use vast in-line.
        String contentId = StringUtil.safeString((String) paramsAsMap.get("ctid"), "");

        String parentCat = StringUtil.safeString((String) paramsAsMap.get(ParamUtils.PARENT_CATEGORY), "");

        if (mainPlacementId == Placement.FPT_PLAY_SMART_TV_VOD) {
            VideoAdDataModel videoAdModel = mappingtoVideoAdModel(crts, selectedFlight, "no-uuid", mainPlacementId, contentId, parentCat,
                    true);
            model.setVideoAdModel(videoAdModel);
        } else if (mainPlacementId == Placement.FPT_PLAY_WEB_VOD || mainPlacementId == Placement.FPT_PLAY_VOD_IOS
                || mainPlacementId == Placement.FPT_PLAY_VOD_ANDROID_BOX || mainPlacementId == Placement.FPT_PLAY_VOD_ANDROID_APP
                || mainPlacementId == Placement.FPT_PLAY_VOD_ANDROID_SMART_TV) {
            // Others use vmap.
            VmapDataModel vmapAdModel = mappingtoVmapAdModel(crts, selectedFlight, "no-uuid", mainPlacementId, contentId, parentCat, true);
            model.setVmapAdModel(vmapAdModel);
            // xml = TemplateUtil.render(VMAP_3rd_AD,
            // model.getVmapAdModel());
        } else if (isLiveTV){
            VideoAdDataModel videoAdModel = mappingtoVideoAdModel(crts, selectedFlight, "no-uuid", mainPlacementId, contentId, parentCat,
                    true);
            model.setVideoAdModel(videoAdModel);
        }
        

        VideoAdDataModel videoModel = model.getVideoAdModel();
        VmapDataModel vmapModel = model.getVmapAdModel();

        // Switch model
        if (videoModel != null) {

            if (videoModel.getVastAdTagURI() != null) {
                xml = TemplateUtil.render(VAST2_3rd_TEMPLATE_PATH, videoModel);
            } else if (videoModel.hasAds()) {
                if (mainPlacementId >= 300 && mainPlacementId < 400) {
                    xml = TemplateUtil.render(VAST3_NATIVE_MOBILE, videoModel);
                } else if (mainPlacementId == 202 || isLiveTV){
                    xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, videoModel);
                }
            }

        } else {
            if (vmapModel.hasAds()) {
                if (mainPlacementId > 200 && mainPlacementId < 300) {
                    xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, vmapModel);
                } else {
                    xml = TemplateUtil.render(VMAP_3rd_AD, vmapModel);
                }
            }
        }
        model.freeResource();
        return xml;

    }

    public static final String VAST_EVENT_IMPRESSION = "impression";

    public static final String VAST_EVENT_COMPLETE = "complete";

    static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder().maximumSize(500)
            .expireAfterWrite(5000, TimeUnit.MILLISECONDS).build(new CacheLoader<String, String>() {
                public String load(String url) {
                    String xml = adUnitLocalCache.getOrDefault(url, StringPool.BLANK);
                    System.out.println("xml3rdDataCache: " + url);
                    if (StringUtil.isEmpty(xml)) {
                        thirdpartyUrls.add(url);
                    }
                    return xml;
                }
            });

    private static final ConcurrentMap<String, String> adUnitLocalCache = new ConcurrentHashMap<>();
    private static final List<String> thirdpartyUrls = new CopyOnWriteArrayList<>();
    static {
        initThirdpartyUrls();
    }

    /**
     * Get xml tracking code from third party.
     */
    public static void initThirdpartyUrls() {
        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (String url : thirdpartyUrls) {
                    execute3rdPt(url);
                }
            }

        }, 0, 5000);
    }
    
    private static String execute3rdPt(String url) {
        String vastXml = Ad3rdRequestUtil.executeGet(url);
        if (vastXml.isEmpty()){
            try {
                vastXml = VASTXmlParser.xml3rdDataCache.get(url);
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        
        if (vastXml.contains("TrackingEvents")) {
            adUnitLocalCache.put(url, vastXml);
        } else {
            adUnitLocalCache.remove(url);
            thirdpartyUrls.remove(url);
        }
        return vastXml;
    }
    public static VmapDataModel mappingtoVmapAdModel(List<Creative> creativesInFlight, Flight flight, String uuid, int placementId,
            String contentId, String parentCat, boolean isSSL) {
        VmapDataModel vmapModel = new VmapDataModel();
        for (Creative crt : creativesInFlight) {

            // General 3rd pt
            VastXmlData vast3rdData = fill3rdPtTracking(crt);

            // For external 3rd pt ad tracking (new case)
            int flightId = flight.getFlightId();

            // get campaign id
            int campaignId = StringUtil.safeParseInt(flight.getCampaign_id(), 0);

            // get buyType
            String buyType = flight.getBuyType();

            InstreamVideoAd instreamVideoAd = createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL, flightId, campaignId,
                    buyType, parentCat);
            instreamVideoAd.setImpression3rdUrls(vast3rdData.getImpression3rdUrls());
            instreamVideoAd.setTrackingEvent3rdTags(vast3rdData.getEventTrackingTags());
            instreamVideoAd.setVideoClicksTags(vast3rdData.getClickTags());
            instreamVideoAd.setThirdPartyAd(true, vast3rdData.getStreamAdTrackingUrls());

            // We have 2 cases
            if (LOCAL_FILE.equals(crt.getType())) {
                try {
                    AdBreak adBreak = new AdBreak(crt.getStartTime().getValue());
                    adBreak.addInstreamVideoAd(instreamVideoAd);
                    vmapModel.addAdBreak(adBreak);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.error(e);
                }
            } else {
                // For 3rd pt ad serving
                AdBreak adBreak = new AdBreak(crt.getStartTime().getValue());
                String adTagURI = crt.getSource();
                if (StringUtil.isNotEmpty(adTagURI)) {

                    if (adTagURI.contains("[timestamp]")) {
                        long cb = System.currentTimeMillis();
                        adTagURI = adTagURI.replace("[timestamp]", String.valueOf(cb));
                    }
                    adBreak.addInstreamVideoAd(instreamVideoAd);
                    adBreak.setAdTagURI(adTagURI);
                    vmapModel.addAdBreak(adBreak);

                    // String xmlContent = "";
                    // try {
                    // xmlContent = xml3rdDataCache.get(adTagURI);
                    // } catch (ExecutionException e) {
                    // e.printStackTrace();
                    // }

                    // if (!xmlContent.isEmpty()) {
                    // if (adTagURI.contains("[timestamp]")) {
                    // long cb = System.currentTimeMillis();
                    // adTagURI = adTagURI.replace("[timestamp]",
                    // String.valueOf(cb));
                    // }
                    // adBreak.addInstreamVideoAd(instreamVideoAd);
                    // adBreak.setAdTagURI(adTagURI);
                    // vmapModel.addAdBreak(adBreak);
                    // } else {
                    // System.out.println("CreativeId: " + crt.getCreativeId() +
                    // ", XmlContent is empty !!!");
                    // }
                }
            }
        }
        return vmapModel;
    }

    private static int count = 0;

    public static VideoAdDataModel mappingtoVideoAdModel(List<Creative> creativesInFlight, Flight flight, String uuid, int placementId,
            String contentId, String parentCat, boolean isSSL) {
        VideoAdDataModel videoAdModel = new VideoAdDataModel();
        for (Creative crt : creativesInFlight) {

            // Generate 3rd pt tracking
            VastXmlData vast3rdData = fill3rdPtTracking(crt);

            int flightId = flight.getFlightId();

            // get campaign id
            int campaignId = StringUtil.safeParseInt(flight.getCampaign_id(), 0);

            // get buyType
            String buyType = flight.getBuyType();

            InstreamVideoAd instreamVideoAd = createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL, flightId, campaignId,
                    buyType, parentCat);

            // Adding 3rd pt tracking
            instreamVideoAd.setImpression3rdUrls(vast3rdData.getImpression3rdUrls());
            instreamVideoAd.setTrackingEvent3rdTags(vast3rdData.getEventTrackingTags());
            instreamVideoAd.setVideoClicksTags(vast3rdData.getClickTags());
            instreamVideoAd.setThirdPartyAd(true, vast3rdData.getStreamAdTrackingUrls());

            // Source content mp4 file
            if (LOCAL_FILE.equals(crt.getType())) {
                try {
                    videoAdModel.addInstreamVideoAd(instreamVideoAd);
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.error(e);
                }
            } else {
                // in-needed to parse vast xml
                String vastXml = "";

                String vastXml3rd = crt.getSource();
                if (vastXml3rd.contains("[timestamp]")) {
                    long cb = System.currentTimeMillis();
                    vastXml3rd = vastXml3rd.replace("[timestamp]", String.valueOf(cb));
                    String vastValue = Ad3rdRequestUtil.executeGet(vastXml3rd);
                    if (vastValue.contains("TrackingEvents")) {
                        vastXml = vastValue;
                    }
                } else {
                    try { 
                        if (count < 1){
                            vastXml = execute3rdPt(vastXml3rd);
                            count++;
                        }
                        if (vastXml.isEmpty()){
                            vastXml = xml3rdDataCache.get(vastXml3rd);
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                VastXmlData vastAdServingParser = VASTXmlParser.parse(vastXml);

                List<MediaFile> mediaFiles = vastAdServingParser.getMediaFiles();
                InstreamVideoAd instreamAdVideo = createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL, flightId,
                        campaignId, buyType, parentCat, mediaFiles);

                instreamAdVideo.setImpression3rdUrls(vastAdServingParser.getImpression3rdUrls());
                instreamAdVideo.setTrackingEvent3rdTags(vastAdServingParser.getEventTrackingTags());
                instreamAdVideo.setVideoClicksTags(vastAdServingParser.getClickTags());
                instreamAdVideo.setThirdPartyAd(true, vastAdServingParser.getStreamAdTrackingUrls());

                videoAdModel.addInstreamVideoAd(instreamAdVideo);

            }
        }

        return videoAdModel;
    }

    private static VastXmlData fill3rdPtTracking(Flight flight) {
        VastXmlData vast3rdData = new VastXmlData();

        flight.getList3rdTracking().forEach(thirdPtTracking -> {

            String eventName = thirdPtTracking.getEvent();
            String urlVal = thirdPtTracking.getUrl();

            if (eventName.equals("completeView")) {
                eventName = "complete";
            }

            if ("impression".equals(eventName)) {
                vast3rdData.addImpression3rdUrl(urlVal);
                vast3rdData.addStreamAdTrackingUrl(VAST_EVENT_IMPRESSION, urlVal, 1);
            } else if ("click".equals(eventName)) {
                String tagClickTracking = "<ClickTracking><![CDATA[" + urlVal.replace("http", "https") + "]]></ClickTracking>";
                vast3rdData.addClickTag(tagClickTracking);
            } else {
                String tag = "<Tracking event=\"" + eventName + "\"><![CDATA[" + urlVal + "]]></Tracking>\n";
                vast3rdData.addEventTrackingTag(tag);
            }
        });
        return vast3rdData;
    }

    private static VastXmlData fill3rdPtTracking(Creative creative) {
        VastXmlData vast3rdData = new VastXmlData();

        creative.getList3rdTracking().forEach(thirdPtTracking -> {

            String eventName = thirdPtTracking.getEvent();
            String urlVal = thirdPtTracking.getUrl();

            if (eventName.equals("completeView")) {
                eventName = "complete";
            }

            if ("impression".equals(eventName)) {
                vast3rdData.addImpression3rdUrl(urlVal);
                vast3rdData.addStreamAdTrackingUrl(VAST_EVENT_IMPRESSION, urlVal, 1);
            } else if ("click".equals(eventName)) {
                String tagClickTracking = "<ClickTracking><![CDATA[" + urlVal.replace("http", "https") + "]]></ClickTracking>";
                vast3rdData.addClickTag(tagClickTracking);
            } else {
                String tag = "<Tracking event=\"" + eventName + "\"><![CDATA[" + urlVal + "]]></Tracking>\n";
                vast3rdData.addEventTrackingTag(tag);
            }
        });
        return vast3rdData;
    }

    private static InstreamVideoAd createInstreamAdsFromCreative(Creative crt, String contentId, String uuid, int placementId,
            boolean isSSL, int flightId, int campaignId, String buyType, String parentCat) {
        return createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL, flightId, campaignId, buyType, parentCat, new ArrayList<MediaFile>());
    }
    
    private static InstreamVideoAd createInstreamAdsFromCreative(Creative crt, String contentId, String uuid, int placementId,
            boolean isSSL, int flightId, int campaignId, String buyType, String parentCat, List<MediaFile> mediaFiles) {
        InstreamVideoAd m = InstreamVideoAdFactory.createInstreamVideoAd();
        try {
            m.setAdId(crt.getCreativeId());
            m.setAdTitle(crt.getName());
            m.setClickThrough(crt.getLandingPage());
            m.setDuration(crt.getDuration());
            m.setSkipoffset(crt.getSkipTime());
            m.setStartTime(crt.getStartTime().getValue());
            if (mediaFiles.isEmpty()){
                m.setMediaFile(crt.getSource(), isSSL);
            } else {
                m.setMediaFiles(mediaFiles);
            }
            m.setContentId(contentId);
            m.setFlighId(flightId);
            m.setPlacementId(placementId);

            // Adding 2 fields for filling params vast xml
            m.setCampaignId(campaignId);
            m.setBuyType(buyType);
            m.setParentCategory(parentCat);
        } catch (Exception e) {
            LogUtil.error(FptPlayDataUtils.class, "createInstreamAdsFromCreative", "Failure when creative stream ad from creative");
            LogUtil.error(e);
            return m;
        }
        return m;
    }

}
