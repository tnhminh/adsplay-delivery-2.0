package adsplay.delivery.redis.cache.thirdpt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import adsplay.common.Ad3rdRequestUtil;
import adsplay.common.AdBeaconUtil;
import adsplay.common.VASTXmlParser;
import adsplay.common.util.ParamUtils;
import adsplay.delivery.redis.cache.model.Creative;
import adsplay.delivery.redis.cache.model.Flight;
import adsplay.server.delivery.query.IPTVDataUtil.IptvAdData;
import adsplay.server.delivery.query.IPTVDataUtil.PayTvAdData;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class IPTVDataUtils {

    private static final String HTTPS = "https:";

    private static final String HTTP = "http:";

    private static final ConcurrentMap<String, String> adUnitLocalCache = new ConcurrentHashMap<>();

    private static final List<String> thirdpartyUrls = new CopyOnWriteArrayList<>();
    static {
        initThirdpartyUrls();
        // clearNotMakeSenseQueryCache();
    }

    /**
     * Get xml tracking code from third party.
     */
    public static void initThirdpartyUrls() {
        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runInitTask();
            }

        }, 0, 5000);
    }

    private static final int HTTP_TIMED_OUT = 3000;

    private static void runInitTask() {
        // long startInitTask = System.currentTimeMillis();
        System.out.println("Init 3rd party URLSs timer task...");
        for (String url : thirdpartyUrls) {
            executeGet3rdPt(url);
        }

        System.out.println(new Date() + " - Current 3rd party list:" + thirdpartyUrls);
        System.out.println(new Date() + " - Current adUnitLocalCache:" + adUnitLocalCache);
        System.out.println(new Date() + " - Finished 3rd party URLSs time task !!! \n =============================\n");
    }
    
    private static String executeGet3rdPt(String url) {
        String vastXml = "";

        long startTime = System.currentTimeMillis();
        while (!vastXml.contains("<AdParameters>") && !vastXml.contains("<TrackingEvents>")) {
            vastXml = Ad3rdRequestUtil.executeGet(url);
            if (vastXml.isEmpty()) {
                try {
                    vastXml = VASTXmlParser.xml3rdDataCache.get(url);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            long maxTime = System.currentTimeMillis() - startTime;
            LogUtil.i("RunInitTask every 5s", "500".equals(vastXml) || "404".equals(vastXml) || "444".equals(vastXml) ? vastXml : "OK",
                    true);
            if (maxTime >= HTTP_TIMED_OUT) {
                LogUtil.i("Max get HTTP time is greater than " + HTTP_TIMED_OUT + "(ms)" + ", so break the requesting loop !!!");
                break;
            }
        }
        if (vastXml.contains("<AdParameters>") || vastXml.contains("<TrackingEvents>")) {
            adUnitLocalCache.put(url, vastXml);
        } else {
            adUnitLocalCache.remove(url);
            thirdpartyUrls.remove(url);
        }
        return vastXml;
    }

    private static int count = 0;

    public static String fillEventTracking(Map<String, Object> paramsAsMap, List<Creative> crts, Flight selectedFlight,
            int mainPlacementId) {
        String[] jsonRes = new String[1];
        boolean[] isHaveCrt = new boolean[1];
        isHaveCrt[0] = false;

        Collections.shuffle(crts);
        crts.forEach(crt -> {
            if (isHaveCrt[0]) {
                return;
            }
            int adID = 0;
            int campaignId = selectedFlight.getCampaign_id();
            int flightId = 0;
            String mp4Link = null;
            int adsFrequency = 4;
            String[] units = new String[3];
            long startTimeMil = 0L;
            String startTime = "";
            String skipTime = "5";

            mp4Link = crt.getSource();
            adID = crt.getCreativeId();
            flightId = selectedFlight.getFlightId();
            
            String buyType = selectedFlight.getBuyType();
            
            int categoryId = StringUtil.safeParseInt(((List<String>) paramsAsMap.get(ParamUtils.CATEGORY)).get(0), 0);

            int sourceProvider =  StringUtil.safeParseInt(((String) paramsAsMap.get(ParamUtils.COPYRIGHT_ID)), 0);
            
            // FrequencyCapping
            // adsFrequency = crt.getFrequencyCapping();
            // is3rdAd = crt.isThirdPartyAd();
            // vastXml3rdUrl = crt.getVastXml3rd();
            System.out.println("Final creative for json result:" + adID);
            // get creative duration
            units = crt.getDuration().split(":");

            // get creative skip time
            String skip = crt.getSkipTime().split(":")[2];

            startTime = crt.getStartTime().getValue();

            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            Date date;
            try {
                date = dateFormat.parse(startTime);
                Date dateRef = dateFormat.parse("00:00:00");
                startTimeMil = (date.getTime() - dateRef.getTime()) / 1000L;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }

            try {
                skipTime = Integer.valueOf(skip).toString();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String result = mp4Link != null ? "1" : "0";// 1 has Ad, 0 no ad

            List<IptvAdData> item = new ArrayList<>(1);

            if (mp4Link != null) {
                if ("external_file".equals(crt.getType())) {
                    String vastXml = "";
                    try {
                        if (count < 1){
                            vastXml = executeGet3rdPt(crt.getSource());
                            count++;
                        }
                        if (vastXml.isEmpty()){
                            vastXml = xml3rdDataCache.get(crt.getSource());
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    if (StringUtil.isNotEmpty(vastXml)) {
                        // get events url
                        IptvAdData ipTvAd = new IptvAdData();

//                        try {
                            // switch between VAST and VPAID
                            if (vastXml.contains("<AdParameters>")) {
                                ipTvAd = readEvent3rdVpaidTracking(vastXml, adID, mp4Link, adsFrequency, units);
                            }

                            if (vastXml.contains("<TrackingEvents>")) {
                                ipTvAd = readEvent3rdVastTracking(vastXml, adID, mp4Link, adsFrequency, units);
                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                        // Set startTime
                        ipTvAd.setStartTime(String.valueOf(startTimeMil));

                        // add defaul adsplay tracking code
                        String adDataParam = AdBeaconUtil.getAdParams(adID, "no-cusId", 320, campaignId, flightId, buyType, categoryId, sourceProvider);

                        String impTracking = "https://log.adsplay.net/track/videoads?event=impression" + adDataParam;

                        String completeViewTracking = "https://log.adsplay.net/track/videoads?event=view100" + adDataParam;
                        
                        String requestTracking = "https://log.adsplay.net/track/videoads?event=request" + adDataParam;

                        ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", impTracking, 1));
                        ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", completeViewTracking, Integer.valueOf(units[2])));
                        ipTvAd.addStartTrackingUrls(new AdTrackingUrl("request", requestTracking, 3));


                        // set SkipTime
                        ipTvAd.setSkipTime(skipTime);

                        // add creative to final list
                        item.add(ipTvAd);
                    }
                } else {
                    IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);

                    String adDataParam = AdBeaconUtil.getAdParams(adID, "no-cusId", 320, campaignId, flightId, buyType, categoryId, sourceProvider);

                    String impTracking = "https://log.adsplay.net/track/videoads?event=impression" + adDataParam;

                    String completeViewTracking = "https://log.adsplay.net/track/videoads?event=view100" + adDataParam;
                    
                    String requestTracking = "https://log.adsplay.net/track/videoads?event=request" + adDataParam;

                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", impTracking, 1));
                    ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", completeViewTracking, 15));
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("request", requestTracking, 3));


                    // set SkipTime
                    ipTvAd.setSkipTime(skipTime);

                    // set StartTime
                    ipTvAd.setStartTime(String.valueOf(startTimeMil));

                    item.add(ipTvAd);
                }
            }
            PayTvAdData adData = new PayTvAdData(result, item);
            jsonRes[0] = new Gson().toJson(adData);
            if (!jsonRes[0].isEmpty()) {
                isHaveCrt[0] = true;
            }
        });
        return jsonRes[0];
    }

    static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder().maximumSize(500)
            .expireAfterWrite(5000, TimeUnit.MILLISECONDS).build(new CacheLoader<String, String>() {
                public String load(String url) {
                    String xml = adUnitLocalCache.getOrDefault(url, StringPool.BLANK);
                    System.out.println("xml3rdDataCache: " + url);
                    if (StringUtil.isEmpty(xml)) {
                        thirdpartyUrls.add(url);
                    }
                    return xml;
                }
            });

    public static IptvAdData readEvent3rdVpaidTracking(String xml, int adID, String mp4Link, int adsFrequency, String[] units) {

        // get creative in second
        int completeDelay = 15;
        if (units.length > 0) {
            completeDelay = Integer.valueOf(units[2]);
        }

        IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);

        int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

        Document doc = Jsoup.parse(xml);
        Elements creatives = doc.select("AdParameters");
        String jsonTracking = creatives.get(0).text();

        JsonElement trackElement = new GsonBuilder().setLenient().create().toJsonTree(jsonTracking);

        if (trackElement.isJsonObject()) {
            JsonObject beacon = trackElement.getAsJsonObject().getAsJsonObject("media").getAsJsonObject("tracking");

            JsonArray trackings = beacon.getAsJsonArray("beacon");

            for (JsonElement track : trackings) {
                String event = track.getAsJsonObject().get("type").getAsString();
                String url = track.getAsJsonObject().get("beacon_url").getAsString();

                url = url.replace(HTTP, HTTPS).replace("[timestamp]", time + "");

                if (event.equals("creativeview")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", url, 1));
                } else if (event.equals("initialization")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("initialization", url, 1));
                } else if (event.equals("impression")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", url, 1));
                } else if (event.equals("firstQuartile")) {
                    // set time
                    int firstQuartileDelay = completeDelay * 25 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("firstquartile", url, firstQuartileDelay));
                } else if (event.equals("midpoint")) {
                    // set time
                    int midpointDelay = completeDelay * 50 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("midpoint", url, midpointDelay));
                } else if (event.equals("thirdQuartile")) {
                    // set time
                    int thirdquartileDelay = completeDelay * 75 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("thirdquartile", url, thirdquartileDelay));
                } else if (event.equals("complete")) {
                    // set time
                    ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", url, completeDelay));
                }
            }
        }
        
        Elements medieFiles = doc.select("MediaFile");
        for (Element node : medieFiles) {
            int width = StringUtil.safeParseInt(node.attr("width"), 0);
            if (width >=300){
                String mediaSource = node.text();
                ipTvAd.setAdFile(mediaSource);
            }
        }

        return ipTvAd;
    }

    public static IptvAdData readEvent3rdVastTracking(String xml, int adID, String mp4Link, int adsFrequency, String[] units) {
        // get creative in second
        int completeDelay = 15;
        if (units.length > 0) {
            completeDelay = Integer.valueOf(units[2]);
        }

        IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);
        int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

        Document doc = Jsoup.parse(xml);

        Elements impNodes = doc.select("Impression");
        for (Element node : impNodes) {
            String text = node.text();
            String impressUrl = text.replace("[timestamp]", time + "");
            ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", impressUrl, 1));
        }

        // another tracking events
        Elements eventNodes = doc.select("Tracking[event]");

        for (Element node : eventNodes) {
            String eventName = node.attr("event");

            String eventValue = node.text();
            if (StringUtil.isNotEmpty(eventValue) && StringUtil.isNotEmpty(eventName)) {
                String url = eventValue.replace("[timestamp]", time + "");
                if (eventName.equals("creativeview")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", url, 1));
                } else if (eventName.equals("initialization")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("initialization", url, 1));
                } else if (eventName.equals("impression")) {
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", url, 1));
                } else if (eventName.equals("firstQuartile")) {
                    // set time
                    int firstQuartileDelay = completeDelay * 25 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("firstquartile", url, firstQuartileDelay));
                } else if (eventName.equals("midpoint")) {
                    // set time
                    int midpointDelay = completeDelay * 50 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("midpoint", url, midpointDelay));
                } else if (eventName.equals("thirdQuartile")) {
                    // set time
                    int thirdquartileDelay = completeDelay * 75 / 100;
                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("thirdquartile", url, thirdquartileDelay));
                } else if (eventName.equals("complete")) {
                    // set time
                    ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", url, completeDelay));
                }
            }
        }
        
        Elements medieFiles = doc.select("MediaFile");
        for (Element node : medieFiles) {
            int width = StringUtil.safeParseInt(node.attr("width"), 0);
            if (width >=300){
                String mediaSource = node.text();
                ipTvAd.setAdFile(mediaSource);
            }
        }

        return ipTvAd;
    }

}
