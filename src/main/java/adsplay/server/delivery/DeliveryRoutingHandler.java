package adsplay.server.delivery;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;

import com.google.gson.Gson;

import adsplay.common.BaseHttpLogHandler;
import adsplay.common.template.ErrorModel;
import adsplay.common.template.TemplateUtil;
import adsplay.delivery.redis.cache.handler.AdplayDeliveryHandler;
import adsplay.delivery.redis.cache.handler.AdsplayIPTVHandler;
import adsplay.delivery.redis.cache.handler.AdsplayLiveTvHandler;
import adsplay.delivery.redis.cache.model.Placement;
import adsplay.delivery.redis.cache.thirdpt.FptPlayDataUtils;
import adsplay.server.delivery.query.IPTVDataUtil;
import adsplay.server.delivery.query.IPTVDataUtil.IptvAdData;
import adsplay.server.delivery.query.IPTVDataUtil.PayTvAdData;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.LogUtil;
import server.http.util.HttpTrackingUtil;

public class DeliveryRoutingHandler extends BaseHttpLogHandler {

    private static final String LIVETV = "/livetv";
    public static final String ADSPLAY_NET = "adsplay.net";
    public static final String APLUUID = "apluuid";

    // delivery prefix URI
    public static final String DELIVERY_PREFIX = "/delivery";

    public static final String DELIVERY_IPTV_PAYTV_DELIVERY = "/iptv/paytv/v1/get";

    public static final String PAYTV_XML = "/iptv/paytv/xml";

    public static final String POWERED_BY = "PoweredBy";
    public static final String ADS_PLAY_VERSION = "AdsPlayServer-2.1";
    public static final String HTTP = "http://";
    private static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();
    public static final String BASE_DOMAIN = WORKER_CONFIGS.getCustomConfig("baseServerDomain");
    public static final String LOG1_DOMAIN = WORKER_CONFIGS.getCustomConfig("log1ServerDomain");
    public static final String LOG2_DOMAIN = WORKER_CONFIGS.getCustomConfig("log2ServerDomain");
    public static final String LOG3_DOMAIN = WORKER_CONFIGS.getCustomConfig("log3ServerDomain");
    public static final String LOG4_DOMAIN = WORKER_CONFIGS.getCustomConfig("log4ServerDomain");

    public DeliveryRoutingHandler() {
    }

    @Override
    public void handle(HttpServerRequest request) {
        // executeDeliveryProcess(request);
    }

    public static void runWorker(String uri, MultiMap params, List<String> pmIds, Future<String> future) {
        // Do the blocking operation in here
        String rsText = "";
        if (uri.contains("/favicon.ico")) {
            return;
        }

        // IPTV
        if (uri.startsWith(DELIVERY_IPTV_PAYTV_DELIVERY)) {
            if (pmIds.isEmpty()) {
                pmIds.add(String.valueOf(IPTVDataUtil.placementId));
            }
            rsText = AdsplayIPTVHandler.handle(params, pmIds);
        } else if (uri.startsWith(DELIVERY_PREFIX)) {
            // FPT-PLAY
            rsText = AdplayDeliveryHandler.handle(params, pmIds);
        } else if (uri.startsWith(LIVETV)) {
            if (pmIds.isEmpty()) {
                pmIds.add(String.valueOf(600));
            }
            // FPT-PLAY LiveTV
            rsText = AdsplayLiveTvHandler.handle(params);
        }

        future.complete(rsText);
    }

    public static void routingToHandler(HttpServerRequest request, Vertx vertxInstance) {
        try {
            String uri = request.uri();
            System.out.println("RoutingHandler.URI: " + uri);
            MultiMap params = request.params();

            List<String> pmIds = params.getAll(Placement.KEY_NAME);

            // Adding executeBlocking here
            vertxInstance.<String>executeBlocking(future -> {
                DeliveryRoutingHandler.runWorker(uri, params, pmIds, future);
            }, res -> {
                if (res.succeeded()) {
                    HttpServerResponse response = request.response();
                    doResponse(uri, res.result(), pmIds, response, params);
                } else {
                    LogUtil.error(DeliveryRoutingHandler.class, "routingToHandler", "Failure when doing response to client !!!");
                    res.cause().printStackTrace();
                }
            });

        } catch (Throwable e) {
            String err = e.getMessage();
            request.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
        }
    }

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerResponse response, MultiMap params) {
        MultiMap outHeaders = response.headers();
        outHeaders.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);
        outHeaders.set(POWERED_BY, ADS_PLAY_VERSION);
        // Keep here (don't remove)
        setCorsHeaders(outHeaders, pmIds);

        if (pmIds.isEmpty()) {
            response.end("Placement param not found" + uri);
            return;
        }

        if (!rsText.isEmpty()) {
            response.end(rsText);
            return;
        } else {
            int placementId = Integer.valueOf(pmIds.get(0)).intValue();
            if (placementId != 320) {
                ErrorModel errorModel = new ErrorModel(uri.replace("/delivery?", ""));
                response.end(TemplateUtil.render(FptPlayDataUtils.NO_AD_XML, errorModel));
                return;
            } else {
                // PayTV empty result
                List<IptvAdData> item = new ArrayList<IptvAdData>(1);
                PayTvAdData adData = new PayTvAdData("0", item);
                String json = new Gson().toJson(adData);
                response.end(json);
                return;
            }
        }
    }

    // private void executeDeliveryProcess(HttpServerRequest request) {
    // String uri = request.uri();
    // String rsText = "";
    // if (uri.contains("/favicon.ico")) {
    // return;
    // }
    //
    // MultiMap params = request.params();
    //
    // List<String> pmIds = params.getAll(Placement.KEY_NAME);
    //
    // HttpServerResponse response = request.response();
    //
    // System.out.println("RoutingHandler.URI: " + uri);
    //
    // // IPTV
    // if (uri.startsWith(DELIVERY_IPTV_PAYTV_DELIVERY)) {
    // if (pmIds.isEmpty()) {
    // pmIds.add(String.valueOf(IPTVDataUtil.placementId));
    // }
    // rsText = AdsplayIPTVHandler.handle(params, pmIds);
    // }
    //
    // // FPT-PLAY
    // if (uri.startsWith(DELIVERY_PREFIX)) {
    // rsText = AdplayDeliveryHandler.handle(params, pmIds);
    // }
    //
    // doResponse(uri, rsText, pmIds, response);
    // }

}
