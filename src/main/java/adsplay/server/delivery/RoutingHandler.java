package adsplay.server.delivery;

import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.adsplay.graylog.enumeration.GrayLevel;
import com.adsplay.graylog.handler.GrayLogHandler;

import adsplay.common.BaseHttpLogHandler;
import adsplay.common.Ip2LocationUtil;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.common.RequestInfoUtil;
import adsplay.common.template.TemplateUtil;
import adsplay.common.util.UserRedisUtil;
import adsplay.server.delivery.handler.AdDeliveryHandler;
import adsplay.server.delivery.handler.AdLogHandler;
import adsplay.server.delivery.handler.PayTVXmlHandler;
import adsplay.server.delivery.query.IPTVDataUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.FileUtils;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;
import server.http.util.HttpTrackingUtil;
import server.http.util.KafkaLogHandlerUtil;

public class RoutingHandler extends BaseHttpLogHandler {

  private static final String NATIVE_AD_GET = "/get";
  public static final String ADSPLAY_NET = "adsplay.net";
  public static final String APLUUID = "apluuid";

  // delivery prefix URI
  public static final String DELIVERY_VMAP_ADS = "/vmap/ads";
  public static final String DELIVERY_PREFIX = "/delivery";
  public static final String ADX_PREFIX = "/adx/video";
  public static final String DELIVERY_IPTV_PAYTV_DELIVERY = "/iptv/paytv/v1/get";
  public static final String DELIVERY_IPTV_LOG_DELIVERY = "/iptv/paytv/v1/track";
  public static final String POWERED_BY = "PoweredBy";
  public static final String ADS_PLAY_VERSION = "AdsPlayServer-2.1";
  public static final String HTTP = "http://";
  private static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();
  public static final String BASE_DOMAIN = WORKER_CONFIGS.getCustomConfig("baseServerDomain");
  public static final String LOG1_DOMAIN = WORKER_CONFIGS.getCustomConfig("log1ServerDomain");
  public static final String LOG2_DOMAIN = WORKER_CONFIGS.getCustomConfig("log2ServerDomain");
  public static final String LOG3_DOMAIN = WORKER_CONFIGS.getCustomConfig("log3ServerDomain");
  public static final String LOG4_DOMAIN = WORKER_CONFIGS.getCustomConfig("log4ServerDomain");

  public static final String PREFIX_VIDEO_LOG = "/track/videoads";
  public static final String PREFIX_ADS_LOG = "/track/ads";
  
  public static final String PAYTV_XML = "/iptv/paytv/xml";

  private static ExecutorService executorService = Executors.newFixedThreadPool(8);
  
  public RoutingHandler() {}

  @Override
  public void handle(HttpServerRequest req) {
      executorService.submit(new Runnable() {
        
        @Override
        public void run() {
            long duration = 0L;
            try  {
                long startRQTime = System.currentTimeMillis();
                executeDeliveryResult(req);
                duration = System.currentTimeMillis() - startRQTime;
            } catch (Exception e){
                LogUtil.error("Error when executeDeliveryResult line 87: " + e);
            } 
            addingGrayLog(req, duration);
        }

        private void addingGrayLog(HttpServerRequest req, long duration) {
            String uri = req.uri();
            Map<String, Object> additionalInfos = new HashMap<String, Object>();
            String timeField = "";
            try {
                if ("/favicon.ico".startsWith(uri)){
                    return;
                }
                
                LogUtil.i("Graylog appending process","URI API: " + uri, true);

                // Adding general
                additionalInfos.put("RQ.method.name", String.valueOf(Thread.currentThread().getStackTrace()[1].getMethodName()));
                additionalInfos.put("RQ.total.time", duration);
                additionalInfos.put("RQ.api", uri);
                String rqType = uri.substring(0, uri.indexOf("?"));
                additionalInfos.put("RQ.type", rqType.equals("/delivery/zone/") ? rqType.substring(0, rqType.lastIndexOf("/")) : rqType);
                additionalInfos.put("RQ.domain", req.host());
                
                MultiMap params = req.params();

                // ====== Additional========
                int placementId = StringUtil.safeParseInt(params.get("placement"), 0);
                String ctId = StringUtil.safeString(params.get("ctid"), "");
                String catId = StringUtil.safeString(params.get("catid"), "");
                String uuid = StringUtil.safeString(params.get("uuid"), "");
                String ut = StringUtil.safeString (params.get("ut"), "");
                String cid = StringUtil.safeString(params.get("cid"), "");
                String versionApp = StringUtil.safeString (params.get("verison_app"), "");

                String url = StringUtil.safeString(params.get("url"), "");
                String location = StringUtil.safeString(params.get("loc"), "");
                String adType = StringUtil.safeString(params.get("at"), "");
                String referer = StringUtil.safeString(params.get("referrer"), "");
                String contextTitle = StringUtil.safeString(params.get("cxt"), "");
                String contextKeywords = StringUtil.safeString(params.get("cxkw"), "");
                //=================
                
                MultiMap reqHeaders = req.headers();

                String useragent = StringUtil.safeString(reqHeaders.get("User-Agent"));
                additionalInfos.put("RQ.dl.userAgent", useragent);

               if (uri.startsWith("/delivery/zone")){
                    long clientTime = StringUtil.safeParseLong(params.get("t"), 0L);
                    timeField = "RQ.dl.clientTime";
                    additionalInfos.put(timeField, clientTime);
                } else if (uri.startsWith(DELIVERY_PREFIX) && !uri.startsWith("/delivery/zone")){
                    long clientTime = getClientTime(params, placementId);
                    timeField = "RQ.dl.clientTime";
                    additionalInfos.put(timeField, clientTime);
                   
                }
               
                additionalInfos.put("RQ.dl.placementId", placementId);
                additionalInfos.put("RQ.dl.ctId", ctId);
                additionalInfos.put("RQ.dl.catId", catId);
                additionalInfos.put("RQ.dl.uuid", uuid);
                additionalInfos.put("RQ.dl.ut", ut);
                additionalInfos.put("RQ.dl.cid", cid);
                
                additionalInfos.put("RQ.dl.url", url);
                additionalInfos.put("RQ.dl.location", location);
                additionalInfos.put("RQ.dl.adType", adType);
                additionalInfos.put("RQ.dl.referer", referer);
                additionalInfos.put("RQ.dl.contextTitle", contextTitle);
                additionalInfos.put("RQ.dl.contextKeywords", contextKeywords);
                additionalInfos.put("RQ.dl.versionApp", versionApp);

                additionalInfos.put("RQ.serverAddr", InetAddress.getLocalHost().getHostName());
            } catch (Exception e) {
                LogUtil.error(e);
                LogUtil.error(uri);
                e.printStackTrace();
            }
            GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "[Request]-[Delivery/Log]-[ALL]", "Request was sent for all type",
                    Thread.currentThread().getStackTrace()[1].getLineNumber(),
                    String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos, timeField);
        }

        private long getClientTime(MultiMap params, int placementId) {
            long deliveryTime = 0L;
            if (placementId == InstreamAdDataUtil.FPT_PLAY_WEB_VOD){
                deliveryTime = StringUtil.safeParseLong(params.get("t"), 0L);
            } else {
                deliveryTime = StringUtil.safeParseLong(params.get("t"), 0L) * 1000;
            }
            return deliveryTime;
        }
    });
  }

  public static void main(String[] args) throws UnknownHostException {
      String uri = "/track/videoads?metric=impression&adid=1716&beacon=zizhzg2pzgzhzj2pzizkzizl2pzj2pzj&t=1509543758";
      System.out.println(uri.substring(0, uri.indexOf("?")));
}
  
private void executeDeliveryResult(HttpServerRequest req) {
    String uri = req.uri();
    System.out.println("RoutingHandler.URI: " + uri);

    MultiMap params = req.params();

    List<String> pmIds = params.getAll("placement");

    HttpServerResponse resp = req.response();
    MultiMap outHeaders = resp.headers();
    outHeaders.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);
    outHeaders.set(POWERED_BY, ADS_PLAY_VERSION);
    setCorsHeaders(outHeaders, pmIds);

    try {

      // ---------------------------------------------------------------------------------------------------
      // ads delivery
      if (uri.startsWith(DELIVERY_PREFIX) || uri.startsWith("/vast.xml?")) {
        AdDeliveryHandler.adHandlerVersion1(req, resp, outHeaders);
        return;
      }

      // ads delivery in JSON for Native Mobile Banner, Infeed, Masterhead, Lightbox, 360
      // interactive banner
      if (uri.startsWith(NATIVE_AD_GET)) {
        AdDeliveryHandler.adLoad(req, resp, outHeaders);
        return;
      }

      // handler for video adx
      else if (uri.startsWith(ADX_PREFIX)) {
        // /vmap/ads?placement=302&ctid=5215971ec969287094465754&catid=&uuid=1d9f18543b439f53&ut=3&t=1490339624
        AdDeliveryHandler.adxVideoHandler(req, resp, outHeaders);
        return;
      }

      // video ads log
      else if (uri.startsWith(PREFIX_VIDEO_LOG)) {
        LogUtil.i("Tracking metric", "URI: " + uri, true);
        int placementId = StringUtil.safeParseInt(params.get("placement"), 0);
        AdLogHandler.handle(req, resp, outHeaders, placementId);
        return;
      }

      // for new Android SDK
      if (uri.startsWith(DELIVERY_VMAP_ADS)) {
        AdDeliveryHandler.adHandlerMobile(req, resp, outHeaders);
        return;
      }

      // ads log
      else if (uri.startsWith(PREFIX_ADS_LOG)) {
        int placementId = StringUtil.safeParseInt(params.get("placement"), 0);
        AdLogHandler.handle(req, resp, outHeaders, placementId);
        return;
      }

      // PayTV
      else if (uri.startsWith(DELIVERY_IPTV_PAYTV_DELIVERY)) {
        IPTVDataUtil.handleForIPTV(req, resp, outHeaders);
        return;
      } else if (uri.startsWith(DELIVERY_IPTV_LOG_DELIVERY)) {
        AdLogHandler.processLogPayTv(req);
        resp.end("1");
        return;
      }

      // ova config
      else if (uri.startsWith("/delivery/ova")) {
        outHeaders.set(CONTENT_TYPE, "application/xml");
        InstreamVideoAd m = new InstreamVideoAd();
        String s = TemplateUtil.render("delivery-ads/ova.xml", m);
        resp.end(s);
        return;
      }

      // click redirect
      else if (uri.startsWith("/click-redirect")) {
        int loggedTime = DateTimeUtil.currentUnixTimestamp();
        int adid = StringUtil.safeParseInt(params.get("adid"), 0);
        String uuid = StringUtil.safeString(params.get("uuid"));
        String url = StringUtil.safeString(params.get("url"));
        int placement = StringUtil.safeParseInt(params.get("placement"), 0);

        resp.setStatusCode(301);
        MultiMap headers = resp.headers();
        headers.add("Location", url);

        KafkaLogHandlerUtil.logAndResponseImage1px(req, AdLogHandler.logAdsClickKafkaKey);
        String[] events = new String[] {AdLogHandler.V_C, AdLogHandler.V_C + "-" + adid};
        RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events, true);
        UserRedisUtil.addClickUser(loggedTime, placement, uuid);
        System.out.println("url: " + url);
        return;
      }

      // ---------------------------------------------------------------------------------------------------
      // analytics handlers
      else if (uri.startsWith("/metric/playview")) {
        String host = StringUtil.safeString(params.get("host"));
        if (StringUtil.isNotEmpty(host)) {
          RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), "mplv-" + host,
              true);
        }
        // TODO log to kafka
        HttpTrackingUtil.trackingResponse(req);
        return;
      } else if (uri.startsWith("/metric/pageview")) {
        // https://log1.adsplay.net/metric/pageview?uuid=cdebc033538a4ea596ab21b6b1567ecf&referrer=&url=https%3A%2F%2Fwww.fshare.vn%2Ffile%2FOKLYRPW83HVQ&host=www.fshare.vn&t=1450609053690&sid=7&tag=download%2Findex
        String host = StringUtil.safeString(params.get("host")).toLowerCase();
        String tag = StringUtil.safeString(params.get("tag")).toLowerCase();
        if (StringUtil.isNotEmpty(host) && StringUtil.isNotEmpty(tag)) {
          // RealtimeTrackingUtil.updateEvent("pv:",DateTimeUtil.currentUnixTimestamp(),
          // "mpav-"+host, true);
          String uuid = StringUtil.safeString(params.get("uuid"));
          RealtimeTrackingUtil.updatePageViewEvent(host, uuid, tag);
        }
        // TODO log to kafka
        HttpTrackingUtil.trackingResponse(req);
        return;
      } else if (uri.startsWith("/metric/event")) {
        // https://log1.adsplay.net/metric/event?uuid=cdebc033538a4ea596ab21b6b1567ecf&referrer=&url=https%3A%2F%2Fwww.fshare.vn%2Ffile%2FOKLYRPW83HVQ&host=www.fshare.vn&t=1450609053690&event=tgrm2016-imp
        // https://log1.adsplay.net/metric/event?uuid=cdebc033538a4ea596ab21b6b1567ecf&referrer=&url=https%3A%2F%2Fwww.fshare.vn%2Ffile%2FOKLYRPW83HVQ&host=www.fshare.vn&t=1450609053690&event=tgrm2016-pv
        String hostReferer = StringUtil.safeString(params.get("host")).toLowerCase();
        String event = StringUtil.safeString(params.get("event")).toLowerCase();

        if (StringUtil.isNotEmpty(hostReferer) && StringUtil.isNotEmpty(event)) {
          String uuid = StringUtil.safeString(params.get("uuid"));
          String url = StringUtil.safeString(params.get("url"));

          if (url.contains("nhacso.net")) {
            RealtimeTrackingUtil.updatePageViewEvent("nhacso.net", uuid, "epv");
          } else {
            RealtimeTrackingUtil.updatePageViewEvent(hostReferer, uuid, "epv");
          }
        }
        // TODO log to kafka
        HttpTrackingUtil.trackingResponse(req);
        return;
      }
      // conversion tracking for 3rd party website
      else if (uri.startsWith("/metric/conversion")) {
        String host = StringUtil.safeString(params.get("host"));
        if (StringUtil.isNotEmpty(host)) {
          RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), "mcvs-" + host,
              true);
        }
        // TODO log to kafka
        HttpTrackingUtil.trackingResponse(req);
        return;
      }

      // DEV MODE JS
      // ---------------------------------------------------------------------------------------------------
      else if (uri.startsWith("/js/adsplay.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/adsplay.js"));
        return;
      } else if (uri.startsWith("/js/adsplay.smarttv.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/adsplay.smarttv.js"));
        return;
      } else if (uri.startsWith("/js/adsplay-click-track.min.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/adsplay-click-track.js"));
        return;
      } else if (uri.startsWith("/js/adsplay-display-ad.min.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/adsplay-display-ad.js"));
        return;
      } else if (uri.startsWith("/js/userreport.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/userreport.js"));
        return;
      } else if (uri.startsWith("/analytics.js")) {
        outHeaders.set(CONTENT_TYPE, "text/javascript");
        resp.end(FileUtils.readFileAsString("./resources/static/analytics.js"));
        return;
      }


      // ---------------------------------------------------------------------------------------------------
      // demo code
      else if (uri.equals("/ads/test-ad.html")) {
        outHeaders.set(CONTENT_TYPE, "text/html");
        resp.end(FileUtils.readFileAsString("./resources/ads/test-ad.html"));
      } else if (uri.startsWith("/ads/interactive-banner/")) {
        outHeaders.set(CONTENT_TYPE, "text/html");
        resp.end(FileUtils.readFileAsString("./resources/ads/default.html"));
      }


      // ---------------------------------------------------------------------------------------------------
      // monitor
      else if (uri.equalsIgnoreCase("/ping")) {
        outHeaders.set(CONTENT_TYPE, "text/html");
        resp.end("PONG");
        return;
      } else if (uri.equalsIgnoreCase("/req-info")) {
        outHeaders.set(CONTENT_TYPE, "text/html");
        resp.end("AdsPlay-Server-version-2 <br>" + RequestInfoUtil.getRequestInfo(req));
        return;
      } else if (uri.equalsIgnoreCase("/my-region")) {
        String ip = RequestInfoUtil.getRemoteIP(req);
        String city = Ip2LocationUtil.findCityAndUpdateStats(ip, null, null);
        outHeaders.set(CONTENT_TYPE, "text/html");
        resp.end("You are at the city : " + city + " , server-time " + new Date());
        return;
      } else if (uri.startsWith(PAYTV_XML)){
    	  PayTVXmlHandler.handle(req, resp);
    	  return;
      }

      // ---------------------------------------------------------------------------------------------------
      // no handler found
      else {
        resp.end("Not handler found for uri:" + uri);
        return;
      }
    } catch (Exception e) {
      LogUtil.error("Error when execute delivery/tracking request: " + e);
      // resp.end();
      System.err.println("uri:" + uri + " error:" + e.getMessage());
      e.printStackTrace();
    }
}
}

