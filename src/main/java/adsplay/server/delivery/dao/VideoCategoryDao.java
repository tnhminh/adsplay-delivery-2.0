package adsplay.server.delivery.dao;

import java.util.List;

import rfx.data.util.cache.CacheConfig;

@CacheConfig(type = CacheConfig.LOCAL_CACHE_ENGINE, keyPrefix = "video_cat:", expireAfter = 6)
public interface VideoCategoryDao {
	
	public List<VideoCategory> getAllVideoCategories();

}
