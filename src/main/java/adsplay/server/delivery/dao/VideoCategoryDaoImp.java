package adsplay.server.delivery.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import rfx.core.util.StringUtil;
import rfx.data.util.sql.CommonSpringDAO;
import rfx.data.util.sql.SqlTemplateUtil;

public class VideoCategoryDaoImp extends CommonSpringDAO implements VideoCategoryDao {

	String sql = SqlTemplateUtil.getSql("SQL.video_category.get_all");

	static class VideoCateMapper implements RowMapper<List<VideoCategory>> {

		@Override
		public List<VideoCategory> mapRow(ResultSet rs, int rowNum) throws SQLException {
			String catName = rs.getString("video_category");
			if(StringUtil.isNotEmpty(catName)){
				String[] toks = catName.split(",");
				List<VideoCategory> list = new ArrayList<>(toks.length);
				for (String tok : toks) {
					if(!tok.equalsIgnoreCase("banner")){
						list.add(new VideoCategory(tok));
					}
				}
				return list;
			}
			return new ArrayList<>(0);
		}

	}

	@Override
	public List<VideoCategory> getAllVideoCategories() {
		List<VideoCategory> rs = new ArrayList<>();
		List<List<VideoCategory>> list = jdbcTpl.query(sql, new VideoCateMapper());
		for (List<VideoCategory> list2 : list) {
			for (VideoCategory videoCategory : list2) {		
				rs.add(videoCategory);
			}
		}
		return rs;
	}

}
