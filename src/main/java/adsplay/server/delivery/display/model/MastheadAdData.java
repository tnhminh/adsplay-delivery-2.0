package adsplay.server.delivery.display.model;

import java.util.ArrayList;
import java.util.List;

import adsplay.common.model.AdDataBaseModel;
import adsplay.server.delivery.video.model.AdTrackingUrl;

/**
 * https://docs.google.com/document/d/1paB2VPmbD3LVDeZHglcBQn_fc4Jkc7ehHLokgnXt_E0/edit?usp=sharing
 * 
 * @author trieu
 *
 */
public class MastheadAdData extends AdDataBaseModel {
	public static final int vertical = 1;
	public static final int horizontal = 0;

	int delayShowVideo = 0;
	protected int width = 0;
	protected int height = 0;
	List<AdTrackingUrl> tracking3rdUrls = new ArrayList<>();
	String background;
	String descriptionText;
	String headlineText;
	String brandIcon;
	String styleAttr;
	String textColor;
	int align = vertical;
	boolean closeAd = true;
	
	
	public MastheadAdData(int placementId, String adMedia, String clickurl, String actionText, int adId, int adType, int width, int height, 
			String background, String head,String des, String brand, String styleAttr, String textColor, int align) {
		super();
		this.placementId = placementId;
		this.adMedia = adMedia;
		this.clickthroughUrl = clickurl;
		this.clickActionText = actionText;
		this.adId = adId;
		this.adType = adType;
		this.width = width;
		this.height = height;
		this.background = background;		
		this.headlineText = head;
		this.descriptionText = des;
		this.brandIcon = brand;
		this.styleAttr = styleAttr;
		this.textColor = textColor;
		this.align = align;
	}
	
	public int getDelayShowVideo() {
		return delayShowVideo;
	}

	public void setDelayShowVideo(int delayShowVideo) {
		this.delayShowVideo = delayShowVideo;
	}

	public String getBackground() {
		return background;
	}


	public void setBackground(String background) {
		this.background = background;
	}


	public int getAlign() {
		return align;
	}


	public void setAlign(int align) {
		this.align = align;
	}


	public String getDescriptionText() {
		return descriptionText;
	}

	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}

	public String getHeadlineText() {
		return headlineText;
	}

	public void setHeadlineText(String headlineText) {
		this.headlineText = headlineText;
	}

	public String getBrandIcon() {
		return brandIcon;
	}

	public void setBrandIcon(String brandIcon) {
		this.brandIcon = brandIcon;
	}

	

	public String getStyleAttr() {
		return styleAttr;
	}

	public void setStyleAttr(String styleAttr) {
		this.styleAttr = styleAttr;
	}

	public MastheadAdData() {
		super();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	

	public String getTextColor() {
		return textColor;
	}


	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}


	public boolean isCloseAd() {
		return closeAd;
	}


	public void setCloseAd(boolean closeAd) {
		this.closeAd = closeAd;
	}


	public static int getVertical() {
		return vertical;
	}


	public static int getHorizontal() {
		return horizontal;
	}


	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public List<AdTrackingUrl> getTracking3rdUrls() {
		return tracking3rdUrls;
	}

	public void setTracking3rdUrls(List<AdTrackingUrl> tracking3rdUrls) {
		this.tracking3rdUrls = tracking3rdUrls;
	}	

	public void addTracking3rdUrls(AdTrackingUrl tracking3rdUrl) {
		this.tracking3rdUrls.add(tracking3rdUrl);
	}	
	

}