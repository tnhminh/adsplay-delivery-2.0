package adsplay.server.delivery.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import adsplay.common.Ip2LocationUtil;
import adsplay.common.PlatformUtil;
import adsplay.common.RequestInfoUtil;
import adsplay.common.VASTXmlParser;
import adsplay.data.Creative;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.HashUtil;
import rfx.core.stream.util.ua.Client;
import rfx.core.stream.util.ua.Parser;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class AdDeliveryHandler {

  public static final String ORIGIN = "Origin";
  public static final String USER_AGENT = "User-Agent";
  public static final String REFERER = "Referer";
  public static final String DISPLAY = "display";
  public static final String OVERLAY = "overlay";
  public static final String REACTIVE = "reactive";
  public static final String NATIVE = "native";


  private static final String TVC = "tvc";
  public static final String NO_DATA = "0";
  public static final String HTTP_FPTPLAY_NET = "http://fptplay.net";
  public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
  public static final String APPLICATION_JSON = "application/json";
  public static final String APPLICATION_XML = "application/xml";

  public static void adHandlerVersion1(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
    // input params
    MultiMap reqHeaders = req.headers();
    String refererHeaderUrl = StringUtil.safeString(reqHeaders.get(REFERER));
    String useragent = StringUtil.safeString(reqHeaders.get(USER_AGENT));
    String origin = StringUtil.safeString(reqHeaders.get(ORIGIN));

    int placementId = 0;
    List<Integer> placementIds;
    MultiMap params = req.params();
    List<String> pmIds = params.getAll("placement");
    if (pmIds != null) {
      placementIds = pmIds.stream().map((String i) -> {
        return StringUtil.safeParseInt(i);
      }).filter(pmId -> {
        return pmId > 0;
      }).collect(Collectors.toList());
      if (placementIds.size() > 0) {
        placementId = placementIds.get(0);
      }
    } else {
      placementIds = new ArrayList<>(0);
    }

    Client userAgentClient = Parser.load().parse(useragent);
    int platformId = PlatformUtil.getPlatformId(placementId);
    String adType = StringUtil.safeString(params.get("at"), TVC);

    // serving request for Video TVC Ad VAST 2 XML
    if (adType.equalsIgnoreCase(TVC)) {
      VideoTvcAdHandler.process(req, resp, outHeaders, params, userAgentClient, platformId,
          placementId, useragent, refererHeaderUrl, origin);
    }
    // serving request for Overlay Ad : Breaking News Style and Banner Image Style
    else if (adType.equalsIgnoreCase(OVERLAY)) {
      OverlayAdHandler.process(req, resp, outHeaders, params, userAgentClient, platformId,
          placementIds, useragent, refererHeaderUrl, origin);
    }
    // serving request for Reactive Ad : TVC or Overlay
    else if (adType.equalsIgnoreCase(REACTIVE)) {
      OverlayAdHandler.process(req, resp, outHeaders, params, userAgentClient, platformId,
          placementIds, useragent, refererHeaderUrl, origin);
    }
    // serving request for Display Ad : Banner, HTML5 Ads and Google DFP
    else if (adType.equalsIgnoreCase(DISPLAY)) {
      DisplayAdHandler.process(req, resp, outHeaders, params, userAgentClient, platformId,
          placementIds, useragent, refererHeaderUrl, origin);
    } else if (adType.equalsIgnoreCase(NATIVE)) {
      NativeAdHandler.process(req, resp, outHeaders, params, userAgentClient, platformId,
          placementIds, useragent, refererHeaderUrl, origin);
    } else {
      resp.end(StringPool.BLANK);
    }
  }

  public static void adHandlerMobile(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
    // input params
    MultiMap reqHeaders = req.headers();
    MultiMap params = req.params();

    String useragent = StringUtil.safeString(reqHeaders.get(USER_AGENT));
    int placementId = StringUtil.safeParseInt(params.get("placement"));

    Client userAgentClient = Parser.load().parse(useragent);
    int platformId = Creative.PLATFORM_NATIVE_APP;
    String adType = StringUtil.safeString(params.get("at"), TVC);


    // serving request for Video TVC Ad VAST 3 XML
    if (adType.equals(TVC)) {
      VideoTvcAdHandler.servingMobileVast3(req, resp, outHeaders, params, userAgentClient,
          platformId, placementId, useragent);
      VideoTvcAdHandler.updatePlayViewRealtimeStats(platformId, placementId);
    } else {
      resp.end(VASTXmlParser.NO_AD_XML_VMAP);
    }
  }

  public static void adxVideoHandler(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
    // input params
    MultiMap params = req.params();
    MultiMap reqHeaders = req.headers();
    String useragent = StringUtil.safeString(reqHeaders.get(USER_AGENT));
    String url = StringUtil.safeString(reqHeaders.get(REFERER));
    // String origin = StringUtil.safeString(reqHeaders.get("Origin"));

    int placementId = StringUtil.safeParseInt(params.get("placement"));
    int platformId = PlatformUtil.getPlatformId(placementId);

    String ip = RequestInfoUtil.getRemoteIP(req);
    String uuid = HashUtil.sha1(ip + useragent);
    String cxt = StringUtil.safeString(params.get("cxt"));
    String cxkw = StringUtil.safeString(params.get("cxkw"));
    String location = StringUtil.safeString(params.get("loc"));
    String contentId = StringUtil.safeString(params.get("cid"));

    String cityFromIp = Ip2LocationUtil.findCityAndUpdateStats(ip);

    // serving request for Video TVC Ad VAST 2 XML
    String xml = InstreamAdDataUtil.getVastXml(contentId, ip, uuid, cxt, cxkw, placementId,
        platformId, location, cityFromIp, url, useragent);
    resp.end(xml);
  }

  public static void adLoad(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders)
      throws Exception {
    // input params
    MultiMap reqHeaders = req.headers();
    // String useragent = StringUtil.safeString(reqHeaders.get(USER_AGENT));
    MultiMap params = req.params();
    int placementId = StringUtil.safeParseInt(params.get("placement"));
    int adType = StringUtil.safeParseInt(params.get("adtype"));
    String uuid = StringUtil.safeString(params.get("uuid"));

    // int platformId = PlatformUtil.getPlatformId(placementId);
    // int timestamp = StringUtil.safeParseInt(params.get("time"));

    NativeAdHandler.servingAd(req, resp, uuid, placementId, adType);
  }



}
