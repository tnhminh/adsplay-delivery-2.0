package adsplay.server.delivery.handler;

import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_ANDROID_INFEED_HOME;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_IOS_INFEED_HOME;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_SMART_TV_MASTHEAD;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_WEB_MASTHEAD;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.server.delivery.display.model.MastheadAdData;
import rfx.core.util.HttpClientUtil;
import rfx.core.util.RandomUtil;

public class MastheadAdHandler {

  static final LoadingCache<Integer, String> htmlCache = CacheBuilder.newBuilder().maximumSize(5)
      .expireAfterWrite(3, TimeUnit.HOURS).build(new CacheLoader<Integer, String>() {
        public String load(Integer placementId) {
          // FIXME load from template,|| placementId == FPT_PLAY_IOS_INFEED_HOME

          if (placementId == FPT_PLAY_ANDROID_INFEED_HOME
              || placementId == FPT_PLAY_IOS_INFEED_HOME) {
            return "";
          }
          return HttpClientUtil.executeGet(
              "http://st.adsplay.net/richmedia/masthead-mobile-" + placementId + ".html");
        }
      });

  static MastheadAdData queryForMobile(String uuid, int placementId, String location) {
    int d = RandomUtil.getRandom(100);
    int align = MastheadAdData.vertical;// MastheadAdData.horizontal;

    if (placementId == FPT_PLAY_ANDROID_INFEED_HOME || placementId == FPT_PLAY_IOS_INFEED_HOME) {
      String adMedia =
          "//ads-cdn.fptplay.net/static/ads/instream/7e05f4b2d392bf5c71ef7c0090203a54.mp4";
      String clickUrl =
          "https://grab.onelink.me/2695613898?pid=fptplay&c=VN_0177_ALL_PAX_GB_ALL_050617_AWAR_FPTplay_Instream__VN17DGGBPDIVEJUN&af_dp=grab%3A%2F%2Fopen%3FscreenType%3DBOOKING&af_web_dp=https%3A%2F%2Fwww.grab.com%2Fvn%2Fblog%2Fnhap-ma-dive-giam-50-cac-chuyen-grabbike-thu-2-trong-ngay%2F%3Futm_source%3Dfptplay%26utm_medium%3DDisplay%26utm_campaign%3DVN17DGGBPDIVEJUN&is_retargeting=true";
      String actionText = "";
      int adId = 1515;
      int adType = 11;
      int width = 0;
      int height = 0;
      String bgImg =
          "//st50.adsplay.net/ads/overlay/1496907701496/a0b8ccc148e97709c156a22859215d3a.png";
      String des = "";
      String head = "";
      String brand = "";
      String styleAttr = "";
      String colortext = "";
      // 970x250
      MastheadAdData ad = new MastheadAdData(placementId, adMedia, clickUrl, actionText, adId,
          adType, width, height, bgImg, head, des, brand, styleAttr, colortext, align);
      ad.buildBeaconData(uuid, 0, 0);
      return ad;
    } else {
      if (d < 100) {
        String adMedia = "//ads-cdn.fptplay.net/static/ads/nokia_3310.mp4";
        String clickUrl = "https://www.nokia.com/vi_vn/phones/nokia-3310-dual-sim";
        String actionText = "Tìm hiểu thêm";
        int adId = 1484;
        int adType = 11;
        int width = 0;
        int height = 0;
        String bgImg =
            "//st88.adsplay.net/ads/overlay/1495939345170/55e9ac132c0422c73d58e3d97f0f1542.png";
        String des = "Biểu tượng đã trở lại";
        String head = "Nokia 3310 Hai SIM";
        String brand = "//st.adsplay.net/richmedia/nokia-logo.png";
        String styleAttr = "left: 525px; top: 25px; width: 356px; height: 200px;";
        String colortext = "color:#FFF;";
        // 970x250
        MastheadAdData ad = new MastheadAdData(placementId, adMedia, clickUrl, actionText, adId,
            adType, width, height, bgImg, head, des, brand, styleAttr, colortext, align);
        ad.buildBeaconData(uuid, 0, 0);
        return ad;
      } else {
        String adMedia =
            "//ads-cdn.fptplay.net/static/ads/instream/9c184d7975960112e5e1e133c939b651.mp4";
        String clickUrl =
            "//clicktrackingv2.userreport.com/?SPV85490f6c;https://www.sapporovietnam.com.vn/trang-chu/?utm_campaign=SapporoSummer2017&utm_medium=preroll&utm_source=FPTPlay";
        String actionText = "Tham Gia Ngay";
        int adId = 1422;
        int adType = 11;
        int width = 0;
        int height = 0;
        String bgImg = "//st.adsplay.net/js/masthead/sapporo-masthead-mobile-background.png";
        String des = "";
        String head = "Sapporo Premium Beer";
        String brand = "//www.sapporovietnam.com.vn/wp-content/themes/sapporo/images/logo.gif";
        String styleAttr = "left: 525px; top: 25px; width: 356px; height: 200px;";
        String colortext = "color:#FFF;";
        // 970x250
        MastheadAdData ad = new MastheadAdData(placementId, adMedia, clickUrl, actionText, adId,
            adType, width, height, bgImg, head, des, brand, styleAttr, colortext, align);
        ad.buildBeaconData(uuid, 0, 0);
        return ad;
      }
    }
  }

  static MastheadAdData queryForDesktop(String uuid, int placementId, String location,
      int delayShowVideo) {
    // String adMedia =
    // "//ads-cdn.fptplay.net/static/ads/instream/2ecae7bb57b55efb3dcf21b368829406.mp4";
    String adMedia = "";
    String clickthroughUrl = "https://www.toshiba.com.vn/san-pham/thiet-bi-gia-dung";
    String clickActionText = "";
    int adId = 1445;
    int adType = 11;
    int width = 1200;
    int height = 250;
    String bgImg = "//st.adsplay.net/img/toshiba-ml2.png";
    String des = "";
    String head = "";
    String brand = "";
    // String styleAttr = "left: 330px; top: 0px; width: 356px; height: 200px;";
    String styleAttr = "";
    String colortext = "color:#000;";
    // 970x250

    MastheadAdData ad = new MastheadAdData(placementId, adMedia, clickthroughUrl, clickActionText,
        adId, adType, width, height, bgImg, head, des, brand, styleAttr, colortext,
        MastheadAdData.horizontal);
    ad.buildBeaconData(uuid, 0, 0);

    // int t = DateTimeUtil.currentUnixTimestamp();
    // ad.addTracking3rdUrls(new AdTrackingUrl("start", "//bit.ly/adsplay-onepixel-start?t="+t, 1));
    // ad.addTracking3rdUrls(new AdTrackingUrl("start",
    // "https://visitanalytics.userreport.com/hit.gif?t=ISV37ff30d1&ord="+t, 1));
    // ad.addTracking3rdUrls(new AdTrackingUrl("trueview",
    // "https://visitanalytics.userreport.com/hit.gif?t=ISV109b93c4&ord="+t, 15));
    // ad.addTracking3rdUrls(new AdTrackingUrl("trueview",
    // "//bit.ly/adsplay-onepixel-complete?t="+t, 15));
    ad.setCloseAd(true);
    ad.setDelayShowVideo(0);
    return ad;
  }

  public static MastheadAdData getAd(String uuid, int placementId, String location,
      int delayShowVideo) {
    // placementId == FSHARE_WEB_MASTHEAD
    if (placementId == FPT_PLAY_WEB_MASTHEAD || placementId == FPT_PLAY_SMART_TV_MASTHEAD) {
      return queryForDesktop(uuid, placementId, location, delayShowVideo);
    } else if (313 <= placementId && placementId <= 319) {
      return queryForMobile(uuid, placementId, location);
    }
    return null;
    // Query q = new Query(Creative.PLATFORM_PC,placementId);
    // q.setUuid(uuid);
    // q.setLocation(location);
    // q.setAdType(Creative.ADTYPE_IMAGE_DISPLAY_AD);
    // return AdUnitQueryUtil.queryDisplayBannerAdData(q);
  }

  public static String getHtml(int placement) {
    try {
      return htmlCache.get(placement);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }



}
