package adsplay.server.delivery.handler;

import static adsplay.server.delivery.handler.AdDeliveryHandler.APPLICATION_XML;
import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;
import static adsplay.targeting.LocationDataUtil.VN_NORTH;
import static adsplay.targeting.LocationDataUtil.VN_SOUTH;
import static adsplay.targeting.LocationDataUtil.northCities;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import adsplay.common.Ip2LocationUtil;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.common.RequestInfoUtil;
import adsplay.common.VASTXmlParser;
import adsplay.common.util.UserRedisUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.HashUtil;
import rfx.core.stream.util.ua.Client;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class VideoTvcAdHandler {

  public static void process(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders,
      MultiMap params, Client userAgentClient, int platformId, int placementId, String useragent,
      String refererHeaderUrl, String origin) throws Exception {
    String uuid = StringUtil.safeString(params.get("uuid"), NO_DATA);
    String contextTitle = StringUtil.safeString(params.get("cxt"), NO_DATA).toLowerCase();
    String contextKeywords = StringUtil.safeString(params.get("cxkw"), NO_DATA);
    String url = StringUtil.safeString(params.get("url"), NO_DATA);
    // int userType = StringUtil.safeParseInt(StringUtil.safeString(params.get("ut"),NO_DATA));

    // get content id of video or live channel
    String contentId = StringUtil.safeString(params.get("cid"), NO_DATA);
    if (NO_DATA.equals(contentId)) {
      contentId = StringUtil.safeString(params.get("ctid"), NO_DATA);
      if (NO_DATA.equals(contentId)) {
        contentId = getContentIdFromURL(url);
      }
    }

    // if (NO_DATA.equals(url)) {
    // url = refererHeaderUrl;

    String ip = RequestInfoUtil.getRemoteIP(req);
    String location = StringUtil.safeString(params.get("loc"), NO_DATA);

    if (NO_DATA.equals(uuid)) {
      uuid = HashUtil.sha1(ip + useragent);
    }

    // TODO
    // boolean skipShowAds = true;
    // //if(userType == InstreamAdDataUtil.USER_TYPE_VID || uuid.equals("000000")){
    // skipShowAds = true;
    // } else {
    // skipShowAds = SkipAdUtil.isSkipAd(url);
    // }
    // if(skipShowAds){
    // System.out.println("skipShowAds: "+skipShowAds);
    // resp.end(VASTXmlParser.NO_AD_XML);
    // return;
    // }

    // TODO no ads.
    if (uuid.equals("000000")) {
      resp.end(VASTXmlParser.NO_AD_XML);
      return;
    }

    // if (placementId == FPT_PLAY_LIVE_STREAM_IOS || placementId ==
    // FPT_PLAY_LIVE_STREAM_ANDROID_APP
    // || placementId == FPT_PLAY_LIVE_STREAM_ANDROID_BOX) {
    // // no ads for LIVE TV
    // resp.end(VASTXmlParser.NO_AD_XML);
    // return;
    // } else {
    // detect audience location
    String cityFromIp = Ip2LocationUtil.findCityAndUpdateStats(ip,
        RealtimeTrackingUtil.LOCATION_METRIC_PLAYVIEW, location);
    if (NO_DATA.equals(location) || location.indexOf("-") < 0) {
      if (northCities.containsKey(cityFromIp)) {
        location = VN_NORTH;
      } else {
        location = VN_SOUTH;
      }
    }
    String xml = VASTXmlParser.NO_AD_XML;
    if (placementId == 101) {
      if (refererHeaderUrl.startsWith("https://fptplay.net/xem-epl/")) {
        placementId = 102;
      }
    }
    xml = InstreamAdDataUtil.getVastXml(contentId, ip, uuid, contextTitle, contextKeywords,
        placementId, platformId, location, cityFromIp, url, useragent);
    outHeaders.set(CONTENT_TYPE, APPLICATION_XML);
    resp.end(xml);

    // write to redis
    updatePlayViewRealtimeStats(req, userAgentClient, platformId, placementId, uuid);

    // write to kafka
    AdLogHandler.logPlayView(req);
    // }
  }

  public static void servingMobileVast3(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders, MultiMap params, Client userAgentClient, int platformId, int placementId,
      String useragent) throws Exception {
    String uuid = StringUtil.safeString(params.get("uuid"), NO_DATA);
    String catid = StringUtil.safeString(params.get("catid"));
    String ctid = StringUtil.safeString(params.get("ctid"));
    int userType = StringUtil.safeParseInt(StringUtil.safeString(params.get("ut"), NO_DATA));
    String ip = RequestInfoUtil.getRemoteIP(req);
    String cityFromIp = Ip2LocationUtil.findCityAndUpdateStats(ip);

    outHeaders.set(CONTENT_TYPE, APPLICATION_XML);
    String xml = InstreamAdDataUtil.getVastXmlVast3(ctid, ip, uuid, userType, catid, placementId,
        platformId, useragent, cityFromIp, userType);
    if (NO_DATA.equals(uuid)) {
      uuid = HashUtil.sha1(ip + useragent);
    }
    resp.end(xml);
  }


  // --------------------------------------------------------------------------------

  public static String getChannelName(String url) {
    int idx = url.indexOf("/livetv");
    boolean isLiveTV = idx > 0;
    if (isLiveTV) {
      String channelName = url.endsWith("/livetv") ? "vtv3" : url.substring(idx + 8);

      int idx1 = channelName.indexOf("#");
      if (idx1 > 0) {
        channelName = channelName.substring(0, idx1);
      }

      int idx2 = channelName.indexOf("?");
      if (idx2 > 0) {
        channelName = channelName.substring(0, idx2);
      }

      int idx3 = channelName.indexOf("/");
      if (idx3 > 0) {
        channelName = channelName.substring(0, idx3);
      }

      return channelName;
    }
    return "";
  }

  public static String getContentIdFromURL(String url) {
    String id = null;

    if (url.contains("livetv")) {
      int beginIndex = url.lastIndexOf("/") + 1;
      try {
        id = url.substring(beginIndex);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      try {
        int idx1 = url.lastIndexOf("/") + 1;
        int idx2 = url.indexOf("-");
        if (idx1 > 1 && idx2 > idx1) {
          id = url.substring(idx1, idx2);
        } else if (idx1 > 1) {
          id = url.substring(idx1);
        }
        
        // If id still be the episode of film 
        if (id.matches("\\d+")) {
          id = url.substring(url.indexOf("vod/") + 4, url.lastIndexOf("/"));
          LogUtil.i("Check id is episode of film", "Final contentID:" + id, true);
        }

      } catch (Throwable e) {
        LogUtil.error(e);
        e.printStackTrace();
      }
    }

    return id;
  }


  public static void updatePlayViewRealtimeStats(int platformId, int placementId) {
    try {
      String[] events = new String[] {AdLogHandler.V_PLAYVIEW, "pf-" + platformId,
          "pvpm-" + placementId, "pvpfpm-" + platformId + "-" + placementId};
      int unixTime = DateTimeUtil.currentUnixTimestamp();
      RealtimeTrackingUtil.updateEvent(unixTime, events, true);
    } catch (Exception e) {
    }
  }

  public static void updatePlayViewRealtimeStats(HttpServerRequest req, Client client,
      int platformId, int placement, String uuid) {
    // AdLogHandler.logPlayView(req);
    // real-time stats for device and OS

    String osStr = "iptv";
    if (client != null) {
      if (platformId != 1 && client.os.major != null) {
        osStr = client.os.family + StringPool.MINUS + client.os.major;
      } else {
        osStr = client.os.family;
      }
    }

    String url = req.params().get("url");
    String channelName = "";
    if (StringUtil.isNotEmpty(url)) {
      System.out.println("url:" + url);
      channelName = getChannelName(url);
    }

    String[] events;
    if (StringUtil.isEmpty(channelName)) {
      events = new String[] {AdLogHandler.V_PLAYVIEW, "pf-" + platformId, "pvpm-" + placement,
          "pvpfpm-" + platformId + "-" + placement, "os-" + osStr};
    } else {
      events = new String[] {AdLogHandler.V_PLAYVIEW, "pf-" + platformId, "pvpm-" + placement,
          "pvpfpm-" + platformId + "-" + placement, "os-" + osStr, "chn-" + channelName};
    }
    int unixTime = DateTimeUtil.currentUnixTimestamp();
    RealtimeTrackingUtil.updateEvent(unixTime, events, true);
    UserRedisUtil.addPlayViewUser(unixTime, placement, uuid);
  }

}
