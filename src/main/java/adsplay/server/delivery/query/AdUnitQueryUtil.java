package adsplay.server.delivery.query;

import static adsplay.common.SpotXCommonTag.SPOTX_APP_TAG;
import static adsplay.common.SpotXCommonTag.SPOTX_MIDROLL_1_APP;
import static adsplay.common.SpotXCommonTag.SPOTX_MIDROLL_1_WEB;
import static adsplay.common.SpotXCommonTag.SPOTX_MIDROLL_2_APP;
import static adsplay.common.SpotXCommonTag.SPOTX_MIDROLL_2_WEB;
import static adsplay.common.SpotXCommonTag.SPOTX_PREROLL_APP;
import static adsplay.common.SpotXCommonTag.SPOTX_PREROLL_WEB;
import static adsplay.common.SpotXCommonTag.SPOTX_WEB_TAG;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_VOD_ANDROID_APP;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_VOD_ANDROID_BOX;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_VOD_ANDROID_SMART_TV;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_VOD_IOS;
import static adsplay.server.delivery.query.InstreamAdDataUtil.FPT_PLAY_WEB_VOD;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;

import adsplay.common.Ad3rdRequestUtil;
import adsplay.common.VASTXmlParser;
import adsplay.common.VASTXmlParser.VastXmlData;
import adsplay.common.model.AdDataBaseModel;
import adsplay.common.template.AdModel;
import adsplay.data.Creative;
import adsplay.server.delivery.display.model.BannerAdData;
import adsplay.server.delivery.display.model.VideoNativeAdData;
import adsplay.server.delivery.handler.OverlayAdHandler;
import adsplay.server.delivery.overlay.model.OverlayAdData;
import adsplay.server.delivery.video.model.AdBreak;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.server.delivery.video.model.VmapDataModel;
import adsplay.targeting.ContentDataUtil;
import adsplay.targeting.ContentMetaData;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.LogUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;
import scala.collection.mutable.StringBuilder;

/**
 * Ad Data Query Util, data from MongoDB, for delivery
 * 
 * @author trieu
 *
 */
public class AdUnitQueryUtil {

  private static final int DANET_SPOTX_APP = 198475;
  private static final int DANET_SPOTX_WEB = 192458;
  private static final int GAMMA_WEB_PLACEMENT_ID = 1469030741;
  private static final String TYPE_THIEU_NHI = "thieu-nhi";
  private static final String EMPTY_STRING = "";

  private static final String DANET_MIENPHI = "danet-mienphi";
  private static final String IMAGE_STATIC_BASE_DOMAIN1 = "//st50.adsplay.net/";
  private static final String IMAGE_STATIC_BASE_DOMAIN2 = "//st88.adsplay.net/";
  private static final int LOAD_BALANCE_SCORE = 5;

  public static final int PRIORITY_SCORE =
      StringUtil.safeParseInt(WorkerConfigs.load().getCustomConfig("adPriorityScore"), 50);

  static final LoadingCache<Query, List<Creative>> queriedAdCache =
      CacheBuilder.newBuilder().maximumSize(1000000).expireAfterWrite(40, TimeUnit.SECONDS)
          .build(new CacheLoader<Query, List<Creative>>() {
            public List<Creative> load(Query q) {
              System.out
                  .println("##### AdUnitQueryUtil ==> MISS CACHE placementId:" + q.getPlacement());
              return queryFromMongoDb(q);
            }
          });

  static final Queue<String> startTimeMidroll = new LinkedList<>();
  static {
    startTimeMidroll.add("00:10:00");
    for (int i = 25; i < 100; i += 15) {
      startTimeMidroll.add("00:" + i + ":00");
    }
  }

  /**
   * Get ad unit from mongoDB.
   * 
   * @param query parameter to get ad unit.
   * @return list of ad unit - creatives matched with conditions
   */
  public static List<Creative> queryFromMongoDb(Query query, boolean isPayTVRes) {
    int placement = query.getPlacement();
    List<Integer> targetedPlacements = Arrays.asList(placement);
    List<Creative> results = null;

    if (query.getListAdId().size() > 0) {
      results = MongoAdQuery.findCreativesByListId(query.getListAdId());
    } else {
      if (query.getAdId() > 0) {
        Creative crt = MongoAdQuery.findCreativesById(query.getAdId());
        if (crt != null) {
          results = Arrays.asList(crt);
        }
      } else {
    	// For Targeting query
    	  
        List<Integer> targetedPlatforms = null;
        if (query.getPlatformId() > 0) {
          targetedPlatforms = Arrays.asList(query.getPlatformId());
        }

        String location = query.getLocation();
        List<String> locs = null;
        if (StringUtil.isNotEmpty(location)) {
          locs = Arrays.asList(location);
        }
        
        List<Integer> targetedGenders = null;
        if (placement == FPT_PLAY_WEB_VOD) {
          targetedGenders = new ArrayList<>(2);
          if (query.isFemaleGroupOnVOD()) {
            targetedGenders.add(Creative.GENDER_TARGET_FEMALE);
          }
          if (query.isMaleGroupOnVOD()) {
            targetedGenders.add(Creative.GENDER_TARGET_MALE);
          }
        }
        System.out.println("targetedGenders" + targetedGenders);
        // List<Long> targetedKeywords = null;//StreamUtil.toCRC64List( Arrays.asList());

        List<Integer> targetedCatIds = null;
        if (query.getCategoryId() > 0) {
          targetedCatIds = Arrays.asList(query.getCategoryId());
        }

        List<String> targetedStructureId = null;
        if (query.getListStructureId().size() > 0) {
          targetedStructureId = query.getListStructureId();
        }
        
        List<Integer> targetMovies = null;
        if (query.getMovieId() > 0) {
        	targetMovies = Arrays.asList(query.getMovieId());
        }
        
        List<Integer> targetCopyrights = null;
        if (query.getCopyrightId() > 0) {
        	targetCopyrights = Arrays.asList(query.getCopyrightId());
        }
        
        results = MongoAdQuery.findCreativesByTargeting(query.isDemo(), query.getAdType(),
            targetedPlacements, targetedPlatforms, locs, targetedGenders, targetedCatIds,
            targetedStructureId, targetMovies, targetCopyrights, isPayTVRes);
      }
    }

    if (results == null) {
      results = new ArrayList<>(0);
    }
    System.out.println("==> queryFromMongoDb results " + results.size());
    return results;
  }
  
  
  public static List<Creative> queryFromMongoDb(Query query) {
      return queryFromMongoDb(query, false);
  }


  /**
   * Get overlay ad unit.
   * 
   * @param query
   * @param autoHideTimeout
   * @param timeToShow
   * @return
   */
  public static OverlayAdData queryOverlayAdData(Query query, int autoHideTimeout, int timeToShow) {
    int adType = query.getAdType();
    try {
      List<Creative> queriedCreatives = doQuery(query);
      System.out.println("queriedCreatives.size: " + queriedCreatives.size());

      int score = RandomUtil.getRandom(100) + 1;// 1 to 100
      List<Creative> filteredCreatives = queriedCreatives.stream()
          .filter(crt -> crt.getScore() > score).collect(Collectors.toList());

      if (filteredCreatives.size() > 0) {
        String uuid = query.getUuid();
        int placementId = query.getPlacement();

        Collections.shuffle(filteredCreatives, new Random(System.nanoTime()));
        Creative crt = filteredCreatives.get(0);
        int adId = crt.getId();
        // int w = 480, h = 120;
        String imgUrl;
        if (score < LOAD_BALANCE_SCORE) {
          imgUrl = IMAGE_STATIC_BASE_DOMAIN1 + crt.getMedia();
        } else {
          imgUrl = IMAGE_STATIC_BASE_DOMAIN2 + crt.getMedia();
        }

        OverlayAdData ad = new OverlayAdData(placementId, imgUrl, crt.getClickThrough(), "", adId,
            adType, autoHideTimeout, timeToShow, OverlayAdHandler.PLAYER_WRAPPER, crt.getWidth(),
            crt.getHeight());
        ad.buildBeaconData(uuid, crt.getCampaignId(), 0);

        System.out.println("===========================================================" + adId);

        return ad;
      }
    } catch (Exception e) {
      e.printStackTrace();
      LogUtil.error(e);
    }
    return null;
  }

  private static List<Creative> doQuery(Query query) throws ExecutionException {
    System.out.println("query.hashCode " + query.hashCode());
    return queriedAdCache.get(query);
  }

  /**
   * Get banner ad unit.
   * 
   * @param query
   * @return
   */
  public static BannerAdData queryDisplayBannerAdData(Query query) {
    int adType = query.getAdType();
    try {
      List<Creative> queriedCreatives = doQuery(query);
      System.out.println("queriedCreatives.size: " + queriedCreatives.size());

      if (queriedCreatives.size() > 0) {
        String uuid = query.getUuid();
        int placementId = query.getPlacement();

        Collections.shuffle(queriedCreatives, new Random(System.nanoTime()));
        Creative crt = queriedCreatives.get(0);

        String imgUrl = crt.getMedia();
        if (imgUrl.contains("ads/overlay")) {
          int score = RandomUtil.getRandom(100) + 1;// 1 to 100
          if (score < LOAD_BALANCE_SCORE) {
            imgUrl = IMAGE_STATIC_BASE_DOMAIN1 + crt.getMedia();
          } else {
            imgUrl = IMAGE_STATIC_BASE_DOMAIN2 + crt.getMedia();
          }
        } else {
          // FIXME
          imgUrl = "https://adsplay.net/ads/banner/" + crt.getMedia();
        }

        BannerAdData ad = new BannerAdData(placementId, imgUrl, crt.getClickThrough(),
            crt.getDescription(), crt.getId(), adType, crt.getWidth(), crt.getHeight());
        ad.buildBeaconData(uuid, crt.getCampaignId(), crt.getFlightId());

        return ad;
      }
    } catch (Exception e) {
      e.printStackTrace();
      LogUtil.error(e);
    }
    return null;
  }

  /**
   * Serving native ad unit.
   * 
   * @param query parameter to get ad unit.
   * @return
   */
  public static AdDataBaseModel queryInPageVideoAdData(Query query) {
    int adType = query.getAdType();
    try {
      String uuid = query.getUuid();
      int placementId = query.getPlacement();
      AdModel model = AdUnitQueryUtil.queryVideoAd(query);
      VideoAdDataModel videoModel = model.getVideoAdModel();
      List<InstreamVideoAd> instreamVideoAds = videoModel.getInstreamVideoAds();
      if (instreamVideoAds.size() > 0) {
        InstreamVideoAd ia = instreamVideoAds.get(0);
        String actionText = "";
        VideoNativeAdData ad = new VideoNativeAdData(placementId, ia.getMediaFile(),
            ia.getClickThrough(), actionText, ia.getAdId(), adType, ia.getOnlyAdTracking3rdUrls());
        ad.buildBeaconData(uuid, 0, 0);
        return ad;
      }
    } catch (Exception e) {
      e.printStackTrace();
      LogUtil.error(e);
    }
    return null;
  }

  /**
   * Get ad unit by ad ID.
   * 
   * @param q
   * @return mapping to VideoAdDataModel
   */
  public static AdModel queryVideoAdById(Query q) {
    AdModel model = new AdModel();
    String uuid = q.getUuid();
    int placement = q.getPlacement();
    boolean isSSL = q.getPurl().startsWith("https");
    List<Creative> finalCreatives = queryFromMongoDb(q);
    VideoAdDataModel videoModel =
        mappingToVideoAdModel(finalCreatives, uuid, placement, q.getContentId(), isSSL);
    model.setVideoAdModel(videoModel);
    return model;
  }

  /**
   * Get ad unit by ad ID.
   * 
   * @param q
   * @return
   */
  public static AdModel queryVmapAdById(Query q) {
    AdModel model = new AdModel();
    String uuid = q.getUuid();
    int placement = q.getPlacement();
    // boolean isSSL = q.getPurl().startsWith("https");
    boolean isSSL = true;
    List<Creative> finalCreatives = queryFromMongoDb(q);
    VmapDataModel vmapModel =
        mappingtoVmapAdModel(finalCreatives, uuid, placement, q.getContentId(), isSSL);
    model.setVmapAdModel(vmapModel);
    return model;
  }

  public static AdModel queryVideoAdForGlee(Query q) {

    int preRollId = 1605;
    int firstMidRoll = 1606;
    int secondMidRoll = 1607;

    q.addListAdId(preRollId);
    q.addListAdId(firstMidRoll);
    q.addListAdId(secondMidRoll);

    String uuid = q.getUuid();
    int mainPlacement = q.getPlacement();
    String contentId = q.getContentId();
    // get timestamp and encode url
    long cb = System.currentTimeMillis();
    String url = EMPTY_STRING;
    try {
      url = URLEncoder.encode(q.getPurl(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    boolean isSSL = true;

    AdModel model = new AdModel();
    VmapDataModel vmapAdModel = new VmapDataModel();
    try {
      List<Creative> listAdsById = doQuery(q);

      if (mainPlacement == 102) {
        for (Creative crt : listAdsById) {
          String adTagURI = "";
          if (crt.getStartTime().equals("00:00:00")) {
            adTagURI = SPOTX_WEB_TAG.replace("[placementSpotX]", SPOTX_PREROLL_WEB);
          } else if (crt.getStartTime().equals("00:10:00")) {
            adTagURI = SPOTX_WEB_TAG.replace("[placementSpotX]", SPOTX_MIDROLL_1_WEB);
          } else if (crt.getStartTime().equals("00:36:40")) {
            adTagURI = SPOTX_WEB_TAG.replace("[placementSpotX]", SPOTX_MIDROLL_2_WEB);
          }

          adTagURI = adTagURI.replace("[content_page_url]", url).replace("[timestamp]",
              String.valueOf(cb));

          InstreamVideoAd m =
              createInstreamAdsFromCreative(crt, contentId, uuid, mainPlacement, isSSL);

          AdBreak adBreak = new AdBreak(crt.getStartTime());
          adBreak.addInstreamVideoAd(m);
          adBreak.setAdTagURI(adTagURI);

          vmapAdModel.addAdBreak(adBreak);
        }
      } else if (mainPlacement > 300 && mainPlacement < 400) {
        for (Creative crt : listAdsById) {
          String adTagURI = "";
          if (crt.getStartTime().equals("00:00:00")) {
            adTagURI = SPOTX_APP_TAG.replace("[placementSpotX]", SPOTX_PREROLL_APP);
          } else if (crt.getStartTime().equals("00:10:00")) {
            adTagURI = SPOTX_APP_TAG.replace("[placementSpotX]", SPOTX_MIDROLL_1_APP);
          } else if (crt.getStartTime().equals("00:36:40")) {
            adTagURI = SPOTX_APP_TAG.replace("[placementSpotX]", SPOTX_MIDROLL_2_APP);
          }

          if (mainPlacement == 302) {
            adTagURI = adTagURI.replace("[mobile_os]", "iOS");
          } else {
            adTagURI = adTagURI.replace("[mobile_os]", "ANDROID");
          }

          InstreamVideoAd m =
              createInstreamAdsFromCreative(crt, contentId, uuid, mainPlacement, isSSL);

          AdBreak adBreak = new AdBreak(crt.getStartTime());
          adBreak.addInstreamVideoAd(m);
          adBreak.setAdTagURI(adTagURI);

          vmapAdModel.addAdBreak(adBreak);
        }
      }

      model.setVmapAdModel(vmapAdModel);
    } catch (ExecutionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return model;
  }

  /**
   * Get instream video ad unit.
   * 
   * @param q parameter to get ad unit.
   * @return videoAdModel for VAST or vmapAdModel for VMAP ad.
   */
  public static AdModel queryVideoAd(Query q) {

    AdModel model = new AdModel();

    VideoAdDataModel videoAdModel;
    VmapDataModel vmapAdModel;

    String uuid = q.getUuid();
    int mainPlacement = q.getPlacement();
    String contentId = q.getContentId();
    boolean isSSL = q.getPurl().startsWith("https");
    try {

      // get content meta-data from redis cache
      ContentMetaData contentMetaData = ContentDataUtil.getContentMetaData(contentId);
      String contentSource = contentMetaData.getSource();
      // List<String> categories = contentMetaData.getCategories();
      List<String> listStructureId = contentMetaData.getListStructureId();
      // String vodType = contentMetaData.getType();

      int dropoutScore = RandomUtil.getRandom(100) + 1;// 1 to 100

      // get timestamp and encode url
      long cb = System.currentTimeMillis();
      String url = EMPTY_STRING;
      try {
        url = URLEncoder.encode(q.getPurl(), "UTF-8");
      } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      boolean isSpotX = false;
      int placementSpotX = DANET_SPOTX_WEB;// FPT Play Web
      // delivery danet ads- from SpotX network
      if (contentSource.contains(DANET_MIENPHI)) {
        // random switch between spotX network and iTvAd network
        if (dropoutScore >= 50) {
          isSpotX = false;
        } else {
          if ((mainPlacement == 102)) {
            placementSpotX = DANET_SPOTX_WEB;
            // query ads for spotX
            // FIXME set spotX placement.
            // q.setPlacement(plamentSpotX);
            // isSpotX = true;
          } else if (mainPlacement > 300 && mainPlacement < 400) {
            placementSpotX = DANET_SPOTX_APP; // FPT Play native app.
          } else if ((mainPlacement > 200 && mainPlacement < 299) || mainPlacement > 1000) {
            placementSpotX = DANET_SPOTX_WEB; // FIXME FPT Play Smart TV
          }
        }
      }

      if (mainPlacement == DANET_SPOTX_WEB) {
        isSpotX = true;
      }

      // if (!isSpotX) {
      // if (dropoutScore < 40) {
      // if (mainPlacement == 102) {
      // // query ads for gamma
      // int placementGamma = GAMMA_WEB_PLACEMENT_ID;
      // q.setPlacement(placementGamma);
      // // query creative from mongodb database
      // List<Creative> queriedCreatives = doQuery(q);
      // if (queriedCreatives.size() > 0) {
      // videoAdModel = create3rdVastTag(queriedCreatives, uuid, mainPlacement, contentId,
      // isSSL, cb, url);
      //
      // model.setVideoAdModel(videoAdModel);
      // return model;
      // }
      // }
      // }
      // }

      // query creative from mongodb database
      q.setListStructureId(listStructureId);
      List<Creative> queriedCreatives = doQuery(q);
      System.out.println("queriedCreatives size:" + queriedCreatives.size());
      //========non-samsung-tv
      // (Tizen|Samsung|SmartTV2013|SmartHub|SMART-TV|Maple|Navi)
      if (q.getUserAgent().contains("Tizen") 
              || q.getUserAgent().contains("Samsung")
              || q.getUserAgent().contains("SmartTV2013") 
              ||q.getUserAgent().contains("SmartHub") 
              ||q.getUserAgent().contains("SMART-TV") 
              ||q.getUserAgent().contains("Maple")
              ||q.getUserAgent().contains("Navi")){
          LogUtil.i("queriedCreatives", "size before:" + queriedCreatives.size(), true);
          Iterator<Creative> crtIte = queriedCreatives.iterator();
          while (crtIte.hasNext()){
              try {
                  Creative crt = crtIte.next();
                  if (crt.getId() == 1891){
                      crtIte.remove();
                  }
              }catch (Exception e) {
                  LogUtil.error(e);
            }
          
          }
          LogUtil.i("queriedCreatives", "size after removing 2 campaign:" + queriedCreatives.size(), true);
      }
      
      //======================
      
      System.out.println("dropout-score: " + dropoutScore + " contentSource:" + contentSource);
      List<Creative> matchedCreatives = null;
      StringBuilder adIdsList = new StringBuilder();

      // FPT Play categories targeting
      matchedCreatives = queriedCreatives.stream().filter(crt -> {
            adIdsList.append(crt.getId()).append("|");
            return crt.getScore() > dropoutScore;
          }).collect(Collectors.toList());
          
      System.out.println("queriedCreatives list:" + adIdsList.toString());

      int n = matchedCreatives.size();
      List<Creative> finalCreatives = new ArrayList<Creative>(n);
      boolean prerollFound = false;
      if (n > 0) {
        Collections.shuffle(matchedCreatives, new Random(System.nanoTime()));

        finalCreatives = getFinalCreatives(matchedCreatives, prerollFound);
      }
      System.out.println("finalCreatives size:" + finalCreatives.size());
      
      StringBuilder finalIdsList = new StringBuilder();
      finalCreatives.forEach(finalCrt -> {
          finalIdsList.append(finalCrt.getId()).append("|");
      });
      LogUtil.i("queryVideoAd", "queriedCreatives size:" + queriedCreatives.size() + ", qrAdId:" + adIdsList
      + ", finalCreatives size:" + finalCreatives.size() + ", final id:"  + finalIdsList + ", dropout-score: " + dropoutScore, true);

      // adModel = mappingToAdDataModel(finalCreatives, uuid, placement, contentId, isSSL);

      if (isSpotX && n > 0) {
        videoAdModel = mappingToVideoAdModel(finalCreatives, uuid, mainPlacement, contentId, isSSL);
        // Third-party code

        String vastAdTagSpotX = "https://search.spotxchange.com/vast/2.0/" + placementSpotX
            + "?VPAID=JS&content_page_url=" + url + "&cb=" + cb
            + "&player_width=360&player_height=640";
        videoAdModel.setVastAdTagURI(vastAdTagSpotX);

        model.setVideoAdModel(videoAdModel);
      } else {
        // PC placement

        if (mainPlacement == FPT_PLAY_WEB_VOD || mainPlacement == FPT_PLAY_VOD_IOS
            || mainPlacement == FPT_PLAY_VOD_ANDROID_BOX
            || mainPlacement == FPT_PLAY_VOD_ANDROID_APP
            || mainPlacement == FPT_PLAY_VOD_ANDROID_SMART_TV) {
          vmapAdModel = mappingtoVmapAdModel(finalCreatives, uuid, mainPlacement, contentId, isSSL);
          model.setVmapAdModel(vmapAdModel);
        } else {
          videoAdModel =
              mappingToVideoAdModel(finalCreatives, uuid, mainPlacement, contentId, isSSL);
          model.setVideoAdModel(videoAdModel);

        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      LogUtil.error(e);
    }
    return model;
  }

  /**
   * Create third party VAST tag
   * 
   * @param queriedCreatives
   * @param uuid
   * @param mainPlacement
   * @param contentId
   * @param isSSL
   * @param cb
   * @param url
   * @return
   */
  public static VideoAdDataModel create3rdVastTag(List<Creative> queriedCreatives, String uuid,
      int mainPlacement, String contentId, boolean isSSL, long cb, String url) {
    VideoAdDataModel videoAdModel;
    videoAdModel = mappingToVideoAdModel(queriedCreatives, uuid, mainPlacement, contentId, isSSL);
    // Third-party code
    String vastAdTagGamma =
        "https://tag.gammaplatform.com/adx/request/?wid=1469030741&zid=1469030993&content_page_url="
            + url + "&cb=" + cb + "&player_width=360&player_height=640&device_id=__device-id__";
    videoAdModel.setVastAdTagURI(vastAdTagGamma);
    return videoAdModel;
  }

  /**
   * Filter ads by categories
   * 
   * @param q
   * @param categories
   * @param vodType
   * @param dropoutScore
   * @param crt
   * @return matched or not
   */
  public static boolean categoriesTargeting(Query q, List<String> listStructureId, String vodType,
      int dropoutScore, Creative crt) {
    int id = crt.getId();
    System.out.println("Matched-CreativeID:" + id + " categories: " + listStructureId);

    // if (vodType.equals("glee-viet-nam")) {
    // return false;
    // }

    // FIXME check enchantuer ads
    if (id == 1587 || id == 1602) {
      // check type of vod and target female
      if (vodType.equals(TYPE_THIEU_NHI)) {
        return false;
      }
      if (vodType.equals("the-thao")) {
        return false;
      }
    }

    // FIXME load from DB, test targeting male group
    if (id == 1631) {
      if (q.isMaleGroupOnVOD()) {
        return true;
      }
      // boolean cat1 = categories.contains(HAN_QUOC);
      boolean cat1 = listStructureId.contains("5575463017dc1321eb858658");
      if (cat1) {
        return false;
      }
      // check type of vod and target male
      if (vodType.equals(TYPE_THIEU_NHI)) {
        return false;
      }
    }

    // check if this creative was not target
    List<String> tgkws = crt.getTargetedKeywords();
    if (tgkws.isEmpty()) {
      return true;
    }

    // check if cannot get contentID -> vodKeyword is null
    if (listStructureId.size() == 0) {
      return true;
    }
    // Set<String> vodKeyWord = categories.keySet();
    Set<String> intersection =
        Sets.intersection(Sets.newHashSet(listStructureId), Sets.newHashSet(tgkws));

    if (intersection.size() == 0) {
      return false;
    }

    // System.out.println(id+" "+crt.getName() + " score: " + crt.getScore());
    return crt.getScore() > dropoutScore;
  }

  /**
   * Get list final creatives and return to client.
   * 
   * @param matchedCreatives List creatives query from database
   * @param prerollFound check is preroll found.
   * @return list of final creatives.
   */
  private static List<Creative> getFinalCreatives(List<Creative> matchedCreatives,
      boolean prerollFound) {

    List<Creative> finalCreatives = new ArrayList<>();
    // find pre-roll TVC first
    for (Creative crt : matchedCreatives) {
      if (crt.isTvcPrerollAd() && !prerollFound) {
        finalCreatives.add(crt);
        prerollFound = true;
        System.out
            .println("=> prerollFound, matchedCreatives " + crt.getId() + " " + crt.getName());
        LogUtil.i("getFinalCreatives", "=> prerollFound, matchedCreatives " + crt.getId() + " " + crt.getName(), true);
      }
    }

    // find mid-roll TVC
    for (Creative crt : matchedCreatives) {
      if (crt.isTvcMidrollAd()) {
        if (!prerollFound) {
          // startTime = Creative.TIME_00_00_00;
          prerollFound = true;
        }
        if (crt.getStartTime() != null) {
          // TODO set start time of ads
          // crt.setStartTime(startTime);
          finalCreatives.add(crt);
          System.out
              .println("=> midrollFound, matchedCreatives " + crt.getId() + " " + crt.getName());
          LogUtil.i("getFinalCreatives", "=> midrollFound, matchedCreatives " + crt.getId() + " " + crt.getName(), true);

        }
      }
    }

    return finalCreatives;
  }

  /**
   * Get ad unit with ad id and list of parameter - for testing purposes.
   * 
   * @param adId
   * @param platformId
   * @param placementId
   * @param uuid
   * @param contentId
   * @return
   */
  public static VideoAdDataModel queryByAdId(int adId, int platformId, int placementId, String uuid,
      String contentId) {
    Query q = new Query(adId, platformId, placementId, uuid);
    boolean isSSL = q.getPurl().startsWith("https");
    VideoAdDataModel adModel = null;
    try {
      List<Creative> matchedCreatives = doQuery(q);
      int queriedAdCount = matchedCreatives.size();
      List<Creative> finalCreatives = new ArrayList<>(queriedAdCount);

      if (queriedAdCount > 0) {
        Creative crt = matchedCreatives.get(0);

        // find pre-roll first
        crt.setStartTime("00:00:00");
        crt.setSkipoffset("00:00:15");
        finalCreatives.add(crt);

        // find mid-roll ads
        // Queue<String> startTimeQueue = new LinkedList<>(startTimeMidroll);
        // for (int i=0; i< 5; i++) {
        // String startTime = startTimeQueue.poll();
        // if(startTime != null){
        // crt.setStartTime(startTime);
        // finalCreatives.add(crt);
        // }
        // }
      }
      adModel = mappingToVideoAdModel(finalCreatives, uuid, placementId, contentId, isSSL);
    } catch (Exception e) {
      e.printStackTrace();
      LogUtil.error(e);
    }
    return adModel;
  }

  private static final ConcurrentMap<String, String> adUnitLocalCache = new ConcurrentHashMap<>();
  private static final List<String> thirdpartyUrls = new CopyOnWriteArrayList<>();
  static {
    initThirdpartyUrls();
  }

  /**
   * Get xml tracking code from third party.
   */
  public static void initThirdpartyUrls() {
    Timer timer = new Timer(true);
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        for (String url : thirdpartyUrls) {
          String vastXml = Ad3rdRequestUtil.executeGet(url);
          if (vastXml.contains("TrackingEvents")) {
            adUnitLocalCache.put(url, vastXml);
          } else {
            adUnitLocalCache.remove(url);
            thirdpartyUrls.remove(url);
          }
        }
      }
    }, 0, 2222);
  }

  static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder().maximumSize(500)
      .expireAfterWrite(2222, TimeUnit.MILLISECONDS).build(new CacheLoader<String, String>() {
        public String load(String url) {
          String xml = adUnitLocalCache.getOrDefault(url, StringPool.BLANK);
          System.out.println("xml3rdDataCache: " + url);
          if (StringUtil.isEmpty(xml)) {
            thirdpartyUrls.add(url);
          }
          return xml;
        }
      });

  /**
   * Create instream video ad from list of creatives.
   * 
   * @param crt
   * @param contentId
   * @param uuid
   * @param placementId
   * @param isSSL
   * @return
   */
  private static InstreamVideoAd createInstreamAdsFromCreative(Creative crt, String contentId,
      String uuid, int placementId, boolean isSSL) {
    InstreamVideoAd m = InstreamVideoAdFactory.createInstreamVideoAd();

    m.setAdId(crt.getId());
    m.setAdTitle(crt.getName());
    m.setClickThrough(crt.getClickThrough());
    m.setDuration(crt.getDuration());
    m.setSkipoffset(crt.getSkipoffset());
    m.setStartTime(crt.getStartTime());
    m.setMediaFile(crt.getMedia(), isSSL);
    m.setContentId(contentId);

    m.buildBeaconData(uuid, placementId, crt.getCampaignId(), crt.getFlightId());

    return m;
  }

  /**
   * Mapping creatives to VMAP object - serving spotX and multiple slot ad.
   * 
   * @param creatives
   * @param uuid
   * @param placementId
   * @param contentId
   * @param isSSL
   * @return
   */
  public static VmapDataModel mappingtoVmapAdModel(List<Creative> creatives, String uuid,
      int placementId, String contentId, boolean isSSL) {
    VmapDataModel vmapModel = new VmapDataModel();
    for (Creative crt : creatives) {
      if (crt.isThirdPartyAd()) {
        // processing for 3rd ads like: Vivid,Ambient
        try {
          String vastXml = "";
          // replace timestamp of 3rd tracking
          String vastXml3rd = crt.getVastXml3rd();
          if (vastXml3rd.contains("[timestamp]")) {
            long cb = System.currentTimeMillis();
            vastXml3rd = vastXml3rd.replace("[timestamp]", String.valueOf(cb));
            String vastValue = Ad3rdRequestUtil.executeGet(vastXml3rd);
            if (vastValue.contains("TrackingEvents")) {
              vastXml = vastValue;
            }
          } else {
            vastXml = xml3rdDataCache.get(vastXml3rd);
          }
          if (StringUtil.isNotEmpty(vastXml)) {
            VastXmlData vast3rdData = VASTXmlParser.parse(vastXml);

            InstreamVideoAd m =
                createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL);

            m.setImpression3rdUrls(vast3rdData.getImpression3rdUrls());
            m.setTrackingEvent3rdTags(vast3rdData.getEventTrackingTags());
            m.setVideoClicksTags(vast3rdData.getClickTags());
            m.setThirdPartyAd(true, vast3rdData.getStreamAdTrackingUrls());

            AdBreak adBreak = new AdBreak(crt.getStartTime());
            adBreak.addInstreamVideoAd(m);

            vmapModel.addAdBreak(adBreak);
          } else {
            System.out.println("NO 3rd Vast Data for AdId " + crt.getId());
          }
        } catch (Exception e) {
          e.printStackTrace();
          LogUtil.error(e);
        }
      } else {
        // For 3rd pt ad serving
        InstreamVideoAd m = createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL);

        AdBreak adBreak = new AdBreak(crt.getStartTime());
        adBreak.addInstreamVideoAd(m);
        String adTagURI = crt.getVastAdServing3rd();
        if (StringUtil.isNotEmpty(adTagURI)) {
          if (adTagURI.contains("[timestamp]")) {
            long cb = System.currentTimeMillis();
            adTagURI = adTagURI.replace("[timestamp]", String.valueOf(cb));
          }
          adBreak.setAdTagURI(adTagURI);
        }

        vmapModel.addAdBreak(adBreak);
      }
    }

    return vmapModel;
  }

  /**
   * Default mapping creatives to VAST format.
   * 
   * @param creatives
   * @param uuid
   * @param placementId
   * @param contentId
   * @param isSSL
   * @return
   */
  private static VideoAdDataModel mappingToVideoAdModel(List<Creative> creatives, String uuid,
      int placementId, String contentId, boolean isSSL) {

    VideoAdDataModel videoAdModel = new VideoAdDataModel();
    for (Creative crt : creatives) {
      if (crt.isThirdPartyAd()) {
        // processing for 3rd ads like: Vivid,Ambient
        try {

          String vastXml = "";
          // replace timestamp of 3rd tracking
          String vastXml3rd = crt.getVastXml3rd();
          if (vastXml3rd.contains("[timestamp]")) {
            long cb = System.currentTimeMillis();
            vastXml3rd = vastXml3rd.replace("[timestamp]", String.valueOf(cb));
            String vastValue = Ad3rdRequestUtil.executeGet(vastXml3rd);
            if (vastValue.contains("TrackingEvents")) {
              vastXml = vastValue;
            }
          } else {
            vastXml = xml3rdDataCache.get(vastXml3rd);
          }
          if (StringUtil.isNotEmpty(vastXml)) {
            VastXmlData vast3rdData = VASTXmlParser.parse(vastXml);

            InstreamVideoAd m =
                createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL);

            m.setImpression3rdUrls(vast3rdData.getImpression3rdUrls());
            m.setTrackingEvent3rdTags(vast3rdData.getEventTrackingTags());
            m.setVideoClicksTags(vast3rdData.getClickTags());
            m.setThirdPartyAd(true, vast3rdData.getStreamAdTrackingUrls());

            videoAdModel.addInstreamVideoAd(m);
          } else {
            System.out.println("NO 3rd Vast Data for AdId " + crt.getId());
          }
        } catch (Exception e) {
          e.printStackTrace();
          LogUtil.error(e);
        }
      } else {
        InstreamVideoAd m = createInstreamAdsFromCreative(crt, contentId, uuid, placementId, isSSL);

        videoAdModel.addInstreamVideoAd(m);
      }
    }
    return videoAdModel;
  }

  public static void main(String[] args) {
    Query q = new Query();
    q.setPlacement(202);
    q.setContentId("595a16e75583204248bca391");
    AdModel test = queryVideoAd(q);
    VideoAdDataModel videoModel = test.getVideoAdModel();
    System.out.println(videoModel.getId());
  }

}
