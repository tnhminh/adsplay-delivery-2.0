package adsplay.server.delivery.query;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import java.net.InetAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.adsplay.graylog.enumeration.GrayLevel;
import com.adsplay.graylog.handler.GrayLogHandler;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

import adsplay.common.Ad3rdRequestUtil;
import adsplay.common.AdBeaconUtil;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.common.util.UserRedisUtil;
import adsplay.data.Creative;
import adsplay.delivery.redis.cache.DeliveryRedisCache;
import adsplay.server.delivery.handler.AdDeliveryHandler;
import adsplay.server.delivery.handler.AdLogHandler;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class IPTVDataUtil {

  private static final int ONE_DAY = 86400000;

private static final int HTTP_TIMED_OUT = 3000;

public static final String HTTP_D3_ADSPLAY = "http://d3.adsplay.net/iptv/paytv/v1/track?file=";

  public static final String SPOTX_NETWORK =
      "https://search.spotxchange.com/vast/2.0/203958?VPAID=JS&content_page_url=&cb=[timestamp]&player_width=1920&player_height=1080";

  private static final String HTTPS = "https:";
  private static final String HTTP = "http:";
  private static final String EMPTY_STRING = "";

  public static final class PayTvAdData {
    @SerializedName("Result")
    String resultSize = "0";
    
    List<IptvAdData> item;

    public PayTvAdData(String result, List<IptvAdData> item) {
      super();
      resultSize = result;
      this.item = item;
    }

    public String getResultSize() {
        return resultSize;
    }

    public void setResultSize(String resultSize) {
        this.resultSize = resultSize;
    }

    public List<IptvAdData> getItem() {
      return item;
    }

    public void setItem(List<IptvAdData> item) {
      this.item = item;
    }
  }

  public static final class IptvAdData {
    int adID;
    String adFile;
    String startTime = "1";
    String skipTime = "5";
    String adsFrequency = "4";
    String duration;

    List<AdTrackingUrl> startTrackingUrls = new ArrayList<>();
    List<AdTrackingUrl> completeTrackingUrls = new ArrayList<>();
    // List<AdTrackingUrl> firstquartileUrls = new ArrayList<>();
    // List<AdTrackingUrl> midpointsUrls = new ArrayList<>();
    // List<AdTrackingUrl> thirdquartileUrls = new ArrayList<>();

    public IptvAdData() {

    }

    public IptvAdData(int adID, String adFilename, int adsFrequency) {
      super();
      this.adID = adID;
      this.adFile = "http://fbox-ads.fpt.vn/static/ads/instream/" + adFilename;
      this.adsFrequency = String.valueOf(adsFrequency);
    }

    public int getAdID() {
      return adID;
    }

    public void setAdID(int adID) {
      this.adID = adID;
    }

    public String getAdFile() {
      return adFile;
    }

    public void setAdFile(String adFile) {
      this.adFile = adFile;
    }

    public String getStartTime() {
      return startTime;
    }

    public void setStartTime(String startTime) {
      this.startTime = startTime;
    }

    public String getSkipTime() {
      return skipTime;
    }

    public void setSkipTime(String skipTime) {
      this.skipTime = skipTime;
    }

    public String getAdsFrequency() {
      return adsFrequency;
    }

    public void setAdsFrequency(String adsFrequency) {
      this.adsFrequency = adsFrequency;
    }

    public List<AdTrackingUrl> getStartTrackingUrls() {
      return startTrackingUrls;
    }

    public void addStartTrackingUrls(AdTrackingUrl startTrackingUrl) {
      this.startTrackingUrls.add(startTrackingUrl);
    }

    public List<AdTrackingUrl> getCompleteTrackingUrls() {
      return completeTrackingUrls;
    }

    public void addCompleteTrackingUrl(AdTrackingUrl completeTrackingUrl) {
      this.completeTrackingUrls.add(completeTrackingUrl);
    }

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
    
  }

  private static final ConcurrentMap<String, String> adUnitLocalCache = new ConcurrentHashMap<>();
  private static final List<String> thirdpartyUrls = new CopyOnWriteArrayList<>();
  static {
    initThirdpartyUrls();
    //clearNotMakeSenseQueryCache();
  }

  /**
   * Get xml tracking code from third party.
   */
  public static void initThirdpartyUrls() {
    Timer timer = new Timer(true);
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        runInitTask();
      }

   
    }, 0, 5000);
  }
  
  public static void main(String[] args) {
    runInitTask();
  }
  
  private static void runInitTask() {
//      long startInitTask = System.currentTimeMillis();
      System.out.println("Init 3rd party URLSs timer task...");
      for (String url : thirdpartyUrls) {
        String vastXml = "";
        
        long startTime = System.currentTimeMillis();
        while (!vastXml.contains("<AdParameters>") && !vastXml.contains("<TrackingEvents>")){
            vastXml = Ad3rdRequestUtil.executeGet(url);
            long maxTime = System.currentTimeMillis() - startTime;
            LogUtil.i("RunInitTask every 5s", "500".equals(vastXml) || "404".equals(vastXml) || "444".equals(vastXml) ? vastXml : "OK", true);
            if (maxTime >= HTTP_TIMED_OUT){
                LogUtil.i("Max get HTTP time is greater than " + HTTP_TIMED_OUT + "(ms)" + ", so break the requesting loop !!!");
                break;
            }
        }
        if (vastXml.contains("<AdParameters>") || vastXml.contains("<TrackingEvents>")) {
          adUnitLocalCache.put(url, vastXml);
        } else {
          adUnitLocalCache.remove(url);
          thirdpartyUrls.remove(url);
        }
      }
//      long durationInitTask = System.currentTimeMillis() - startInitTask;
      
//      try {
//          Map<String, Object> additionalInfos = new HashMap<String, Object>();
//          additionalInfos.put("Init.methodName", String.valueOf(Thread.currentThread().getStackTrace()[1].getMethodName()));
//          additionalInfos.put("Init.durationInitTask", durationInitTask);
//          additionalInfos.put("Init.3rdPTSize", thirdpartyUrls.size());
//          additionalInfos.put("Init.serverAddr", InetAddress.getLocalHost().getHostName());
//
//          GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "PayTV - [InitTask] - every 5s ", " The response is OK",
//                  Thread.currentThread().getStackTrace()[1].getLineNumber(),
//                  String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos);
//        
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
      
      System.out.println(new Date() + " - Current 3rd party list:"+ thirdpartyUrls);
      System.out.println(new Date() + " - Current adUnitLocalCache:"+ adUnitLocalCache);
      System.out.println(new Date() + " - Finished 3rd party URLSs time task !!! \n =============================\n");
  }

  static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder().maximumSize(500)
      .expireAfterWrite(5000, TimeUnit.MILLISECONDS).build(new CacheLoader<String, String>() {
        public String load(String url) {
          String xml = adUnitLocalCache.getOrDefault(url, StringPool.BLANK);
          System.out.println("xml3rdDataCache: " + url);
          if (StringUtil.isEmpty(xml)) {
            thirdpartyUrls.add(url);
          }
          return xml;
        }
      });

  static final LoadingCache<Query, String> jsonCache = CacheBuilder.newBuilder().maximumSize(100000)
      .expireAfterWrite(20, TimeUnit.SECONDS).build(new CacheLoader<Query, String>() {
        public String load(Query query) {
          return queryInMongoDB(query);
        }

        private String queryInMongoDB(Query query) {
              
              Map<String, Object> additionalInfos = new HashMap<String, Object>();

        	  long startTotalTime = System.currentTimeMillis();
        	  
              // query from MongoDB
              long startGetRedis = System.currentTimeMillis();

              List<Creative> creatives = new ArrayList<Creative>();
              StringBuilder apiPayTV = new StringBuilder();
              String stringApi = "";
              try {
                   apiPayTV.append(StringUtil.safeParseInt(query.getCategoryId(), DeliveryRedisCache.INT_DEFAULT)).append("-");
                   apiPayTV.append(StringUtil.safeString(query.getLocation(), DeliveryRedisCache.STR_DEFAULT)).append("-");
                   apiPayTV.append(StringUtil.safeParseInt(query.getCopyrightId(), DeliveryRedisCache.INT_DEFAULT)).append("-");
                   apiPayTV.append(StringUtil.safeParseInt(query.getMovieId(), DeliveryRedisCache.INT_DEFAULT));

                   stringApi = apiPayTV.toString();
                   additionalInfos.put("JSCache.QR.apiPayTV", stringApi);

                   creatives = DeliveryRedisCache.get(stringApi);
                   // creatives = queriedAdCache.get(query);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

              //=======================================================
              
              long loadCreativesWithRedisCache = System.currentTimeMillis() - startGetRedis;
              
              String cusId = query.getUuid();

              int adID = 0;
              int campaignId = 0;
              int flightId = 0;
              String mp4Link = null;
              int adsFrequency = 4;

              boolean is3rdAd = false;
              String vastXml3rdUrl = EMPTY_STRING;
              String[] units = new String[3];
              String skipTime = "5";
              List<Creative> filteredCreatives = new ArrayList<Creative>();
              StringBuilder creativesString = new StringBuilder();
              creatives.forEach(crt -> { 
              creativesString.append(crt.getId()).append("|");
              int priority = crt.getScore() / 10; 
                  for (int i=0 ; i < priority ; i++){ 
                      filteredCreatives.add(crt); 
                  } 
              }); 
              
              String startTime = "";
              long startTimeSec = 0L;
              if (filteredCreatives.size() > 0) {
                Collections.shuffle(filteredCreatives);
                Creative crt = filteredCreatives.get(0);
                mp4Link = crt.getMedia();
                adID = crt.getId();
                campaignId = crt.getCampaignId();
                flightId = crt.getFlightId();
                adsFrequency = crt.getFrequencyCapping();
                is3rdAd = crt.isThirdPartyAd();
                vastXml3rdUrl = crt.getVastXml3rd();
                System.out.println("Final creative for json result:" + adID);
                // get creative duration
                units = crt.getDuration().split(":");

                // get creative skip time
                String skip = crt.getSkipoffset().split(":")[2];
                
               
                
                startTime = crt.getStartTime();
                
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date date;
                try {
                    date = dateFormat.parse(startTime);
                    Date reference = dateFormat.parse("00:00:00");
                    startTimeSec = (date.getTime() - reference.getTime()) / 1000L;
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                
                try {
                  skipTime = Integer.valueOf(skip).toString();
                } catch (Exception e) {
                  e.printStackTrace();
                }
              } 
              String crtList = creativesString.toString();
              LogUtil.i("PayTV non-3rd-pt","AdId for result json: " + adID + ", api: " + stringApi + ", List creatives query:" + crtList, true);
              additionalInfos.put("JSCache.QR.creativesList", crtList);

              String result = mp4Link != null ? "1" : "0";// 1 has Ad, 0 no ad
              
              try {
            	  additionalInfos.put("JSCache.QR.creatives.size", creatives.size());
                  additionalInfos.put("JSCache.QR.adId", adID);
                  additionalInfos.put("JSCache.QR.load.redis.cache.time", loadCreativesWithRedisCache);
                  additionalInfos.put("JSCache.QR.queryString", query.toString());
                  additionalInfos.put("JSCache.QR.server.address", InetAddress.getLocalHost().getHostName());
	              additionalInfos.put("JSCache.QR.method.name", String.valueOf(Thread.currentThread().getStackTrace()[1].getMethodName()));
              } catch (Exception e1) {
				e1.printStackTrace();
              }

              long nonQueryStartTime = System.currentTimeMillis();

              List<IptvAdData> item = new ArrayList<>(1);
              if (mp4Link != null) {
                if (is3rdAd) {
                  String vastXml = EMPTY_STRING;
                  try {
                    // int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);
                    // vastXml3rdUrl = vastXml3rdUrl.replace(HTTP, HTTPS).replace("[timestamp]", time +
                    // EMPTY_STRING);
                    long startTimeGetVAST = System.currentTimeMillis();
                    vastXml = xml3rdDataCache.get(vastXml3rdUrl);
                    long getVastDuration = System.currentTimeMillis() - startTimeGetVAST;
                    
                    LogUtil.i("PayTV 3rd PT","AdId for result json: " + adID + ", vastXMLIsEmpty: " + vastXml.isEmpty() + ",\n vastXML3rdPT: " + vastXml3rdUrl + "\n", true);
                    System.out.println("AdId for result json:" + adID + ", \nvastXMLIsEmpty: " + vastXml.isEmpty() + ",\n vastXML3rdPT:" + vastXml3rdUrl);
                    
                    additionalInfos.put("JSCache.QR.getVast.duration", getVastDuration);
                    additionalInfos.put("JSCache.QR.vastXMLIsEmpty", String.valueOf(vastXml.isEmpty()));
                        
                    System.out.println(new Date() + " - VastXML was loaded successfully, content:" + vastXml);
                  } catch (ExecutionException e) {
                    e.printStackTrace();
                  }
                  System.out.println("Checking on 3rd party list:"+ thirdpartyUrls);
                  System.out.println("Checking on adUnitLocalCache:"+ adUnitLocalCache);
                  
                  if (StringUtil.isNotEmpty(vastXml)) {
                    // get events url
                    IptvAdData ipTvAd = new IptvAdData();
                    
                    // switch between VAST and VPAID
                    if (vastXml.contains("<AdParameters>")) {
                      ipTvAd = readEvent3rdVpaidTracking(vastXml, adID, mp4Link, adsFrequency, units);
                    }
                    if (vastXml.contains("<TrackingEvents>")) {
                      ipTvAd = readEvent3rdVastTracking(vastXml, adID, mp4Link, adsFrequency, units);
                    }
                    
                    // Set startTime
                    ipTvAd.setStartTime(String.valueOf(startTimeSec));
                    
                    // add defaul adsplay tracking code
                    String adDataParam = AdBeaconUtil.getAdParams(adID, EMPTY_STRING + cusId,
                        placementId, campaignId, flightId);

                    String impTracking =
                        "https://log.adsplay.net/track/videoads?metric=impression" + adDataParam;

                    String completeViewTracking =
                        "https://log.adsplay.net/track/videoads?metric=view100" + adDataParam;

                    ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", impTracking, 1));
                    ipTvAd.addCompleteTrackingUrl(
                        new AdTrackingUrl("complete", completeViewTracking, Integer.valueOf(units[2])));

                    // set SkipTime
                    ipTvAd.setSkipTime(skipTime);

                    // add creative to final list
                    item.add(ipTvAd);
                  }
                } else {

                  IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);
                  // e1.addStartTrackingUrls(new AdTrackingUrl("start",
                  // "http://bit.ly/adsplay-onepixel-start", 1));
                  // e1.addCompleteTrackingUrl(
                  // new AdTrackingUrl("complete", "http://bit.ly/adsplay-onepixel-complete", 15));

                  String adDataParam = AdBeaconUtil.getAdParams(adID, EMPTY_STRING + cusId, placementId,
                      campaignId, flightId);

                  String impTracking =
                      "https://log.adsplay.net/track/videoads?metric=impression" + adDataParam;

                  String completeViewTracking =
                      "https://log.adsplay.net/track/videoads?metric=view100" + adDataParam;

                  ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", impTracking, 1));
                  ipTvAd
                      .addCompleteTrackingUrl(new AdTrackingUrl("complete", completeViewTracking, 15));

                  // set SkipTime
                  ipTvAd.setSkipTime(skipTime);
                  
                  // set StartTime
                  ipTvAd.setStartTime(String.valueOf(startTimeSec));

                  item.add(ipTvAd);

                }
              }

              PayTvAdData adData = new PayTvAdData(result, item);
              String json = new Gson().toJson(adData);

              long nonQueryTimeDuration = System.currentTimeMillis() - nonQueryStartTime;
              long totalQueryTime = System.currentTimeMillis() - startTotalTime;
              
              boolean isInvalidResult = result.equals("0") ||  item.isEmpty();
              String fullMessage = isInvalidResult ?"The response is not integrity !":"The response is OK";

              additionalInfos.put("JSCache.QR.nonQueryTime", nonQueryTimeDuration);
              additionalInfos.put("JSCache.QR.jsonResultIsEmpty", String.valueOf(isInvalidResult));
              additionalInfos.put("JSCache.QR.totalQueryTime", totalQueryTime);

              GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "PayTV -[Cache expired or not contain key]", fullMessage,
                      Thread.currentThread().getStackTrace()[1].getLineNumber(),
                      String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos);
              return json;
        }
      });

  /**
   * Read data from 3rd tracking.
   * 
   * @param events
   * @param adID
   * @param mp4Link
   * @param adsFrequency
   * @return
   */
  public static IptvAdData readEvent3rdVpaidTracking(String xml, int adID, String mp4Link,
      int adsFrequency, String[] units) {

    // get creative in second
    int completeDelay = 15;
    if (units.length > 0) {
      completeDelay = Integer.valueOf(units[2]);
    }

    IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);

    int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

    Document doc = Jsoup.parse(xml);
    Elements creatives = doc.select("AdParameters");
    String jsonTracking = creatives.get(0).text();

    JsonElement trackElement = new JsonParser().parse(jsonTracking);

    if (trackElement.isJsonObject()) {
      JsonObject beacon =
          trackElement.getAsJsonObject().getAsJsonObject("media").getAsJsonObject("tracking");

      JsonArray trackings = beacon.getAsJsonArray("beacon");

      for (JsonElement track : trackings) {
        String event = track.getAsJsonObject().get("type").getAsString();
        String url = track.getAsJsonObject().get("beacon_url").getAsString();

        url = url.replace(HTTP, HTTPS).replace("[timestamp]", time + EMPTY_STRING);

        if (event.equals("creativeview")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", url, 1));
        } else if (event.equals("initialization")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("initialization", url, 1));
        } else if (event.equals("impression")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", url, 1));
        } else if (event.equals("firstQuartile")) {
          // set time
          int firstQuartileDelay = completeDelay * 25 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("firstquartile", url, firstQuartileDelay));
        } else if (event.equals("midpoint")) {
          // set time
          int midpointDelay = completeDelay * 50 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("midpoint", url, midpointDelay));
        } else if (event.equals("thirdQuartile")) {
          // set time
          int thirdquartileDelay = completeDelay * 75 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("thirdquartile", url, thirdquartileDelay));
        } else if (event.equals("complete")) {
          // set time
          ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", url, completeDelay));
        }
      }
    }

    return ipTvAd;
  }

  public static IptvAdData readEvent3rdVastTracking(String xml, int adID, String mp4Link,
      int adsFrequency, String[] units) {
    // get creative in second
    int completeDelay = 15;
    if (units.length > 0) {
      completeDelay = Integer.valueOf(units[2]);
    }

    IptvAdData ipTvAd = new IptvAdData(adID, mp4Link, adsFrequency);
    int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

    Document doc = Jsoup.parse(xml);

    Elements impNodes = doc.select("Impression");
    for (Element node : impNodes) {
      String text = node.text();
      String impressUrl = text.replace("[timestamp]", time + "");
      ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", impressUrl, 1));
    }

    // another tracking events
    Elements eventNodes = doc.select("Tracking[event]");

    for (Element node : eventNodes) {
      String eventName = node.attr("event");

      String eventValue = node.text();
      if (StringUtil.isNotEmpty(eventValue) && StringUtil.isNotEmpty(eventName)) {
    	String url = eventValue.replace("[timestamp]", time + "");
        if (eventName.equals("creativeview")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", url, 1));
        } else if (eventName.equals("initialization")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("initialization", url, 1));
        } else if (eventName.equals("impression")) {
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("impression", url, 1));
        } else if (eventName.equals("firstQuartile")) {
          // set time
          int firstQuartileDelay = completeDelay * 25 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("firstquartile", url, firstQuartileDelay));
        } else if (eventName.equals("midpoint")) {
          // set time
          int midpointDelay = completeDelay * 50 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("midpoint", url, midpointDelay));
        } else if (eventName.equals("thirdQuartile")) {
          // set time
          int thirdquartileDelay = completeDelay * 75 / 100;
          ipTvAd.addStartTrackingUrls(new AdTrackingUrl("thirdquartile", url, thirdquartileDelay));
        } else if (eventName.equals("complete")) {
          // set time
          ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", url, completeDelay));
        }
      }
    }

    return ipTvAd;
  }

  public static final int platformId = Creative.PLATFORM_IPTV_OTT;
  public static final int placementId = 320;

  public static void handleForIPTV(HttpServerRequest req, HttpServerResponse resp,
      MultiMap outHeaders) throws Exception {
	  
    long startTime = System.currentTimeMillis();

    outHeaders.set(CONTENT_TYPE, AdDeliveryHandler.APPLICATION_JSON);

    MultiMap params = req.params();
    int categoryID = StringUtil.safeParseInt(params.get("catID"));
    String uuid = StringUtil.safeString(params.get("cusID"));
    String location = StringUtil.safeString(params.get("conID"));

    // New params for paytv - 25-10-2017
    int copyrightId = StringUtil.safeParseInt(params.get("copyrightId"));
    int movieId = StringUtil.safeParseInt(params.get("movieId"));;

    Query query = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location, categoryID);
    query.setCopyrightId(copyrightId);
    query.setMovieId(movieId);
    resp.end(jsonCache.get(query));
    long responseEndDuration = System.currentTimeMillis() - startTime;
    
    long startTimeWritingToRedis = System.currentTimeMillis();
    // write to redis
    updatePlayViewRealtimeStats(uuid);
    // AdLogHandler.logPlayView(req);
    long updatePVRTDuration = System.currentTimeMillis() - startTimeWritingToRedis;
    
    long responseTime = System.currentTimeMillis() - startTime;
    
    Map<String, Object> additionalInfos = new HashMap<String, Object>();
    additionalInfos.put("IPTV.methodName", String.valueOf(Thread.currentThread().getStackTrace()[1].getMethodName()));
    additionalInfos.put("IPTV.totalTime", responseTime);
    additionalInfos.put("IPTV.responseEndDuration", responseEndDuration);
    additionalInfos.put("IPTV.updatePVRTDuration", updatePVRTDuration);
    additionalInfos.put("IPTV.apiURI", req.absoluteURI());
    additionalInfos.put("IPTV.serverAddress", InetAddress.getLocalHost().getHostName());

    GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "PayTV - [Delivery] - [Total Time Execution] ", "get IPTV Result",
            Thread.currentThread().getStackTrace()[1].getLineNumber(),
            String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos);
  }

  static Map<String, Integer> mapAdFileToAdId = new HashMap<>();
  static {
    Timer timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        Query query = new Query(platformId, placementId);
        List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(query);
        System.out.println("Starting timer task to get media from from MongoDB ...");
        for (Creative c : creatives) {
          mapAdFileToAdId.put(c.getMedia(), c.getId());
        }
      }
    }, 100, 3000);
  }

  public static int getAdIdFromAdFile(String name) {
    return mapAdFileToAdId.getOrDefault(name, 0);
  }

  public static void updatePlayViewRealtimeStats(String uuid) {
    int platformId = Creative.PLATFORM_IPTV_OTT;
    String[] events = new String[] {AdLogHandler.V_PLAYVIEW, "pf-" + platformId,
        "pvpm-" + placementId, "pvpfpm-" + platformId + "-" + placementId};
    int unixTime = DateTimeUtil.currentUnixTimestamp();
    RealtimeTrackingUtil.updateEvent(unixTime, events, true);
    UserRedisUtil.addPlayViewUser(unixTime, placementId, uuid);
  }
}
