package adsplay.server.delivery.query;

import static adsplay.common.VASTXmlParser.NO_AD_XML;
import static adsplay.common.VASTXmlParser.NO_AD_XML_VMAP;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.common.AdTimeUtil;
import adsplay.common.template.AdModel;
import adsplay.common.template.TemplateUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.video.model.AdBreak;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.server.delivery.video.model.VmapDataModel;
import adsplay.targeting.ContentDataUtil;
import adsplay.targeting.ContentMetaData;
import adsplay.targeting.UserSegmentUtil;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class InstreamAdDataUtil {

  private static final String PREMIER_LEAGUE = "premier league";
  private static final String XEM_EPL = "/xem-epl/";
  private static final String ADSPLAY_DEMO = "adsplay-demo-";
  public static final String MAC_OS = "Mac OS";
  public static final String I_PHONE = "iPhone";

  // Desktop/Mobile Web
  public static final int FPT_PLAY_WEB_LIVE_TV = 101;
  public static final int FPT_PLAY_WEB_VOD = 102;
  public static final int FPT_PLAY_WEB_MASTHEAD = 113;
  public static final int FSHARE_WEB_MASTHEAD = 115;
  public static final int FSHARE_TVC_PLACEMENT = 120;

  // Smart-TV
  public static final int FPT_PLAY_SMARTTV_LIVE_TV = 201;
  public static final int FPT_PLAY_SMART_TV_VOD = 202;
  public static final int FPT_PLAY_SMART_TV_MASTHEAD = 213;

  // Mobile/Native app
  public static final int FPT_PLAY_LIVE_STREAM_IOS = 301;
  public static final int FPT_PLAY_VOD_IOS = 302;
  public static final int FPT_PLAY_LIVE_STREAM_ANDROID_BOX = 303;
  public static final int FPT_PLAY_VOD_ANDROID_BOX = 304;
  public static final int FPT_PLAY_LIVE_STREAM_ANDROID_APP = 305;
  public static final int FPT_PLAY_VOD_ANDROID_APP = 306;
  public static final int FPT_PLAY_LIVE_STREAM_ANDROID_SMART_TV = 307;
  public static final int FPT_PLAY_VOD_ANDROID_SMART_TV = 308;

  // Mobile Masthead
  public static final int FPT_PLAY_ANDROID_MASTHEAD_EPL = 313;
  public static final int FPT_PLAY_IOS_MASTHEAD = 314;
  public static final int FPT_PLAY_ANDROID_MASTHEAD_U20 = 315;
  public static final int FPT_PLAY_ANDROID_INFEED_HOME = 317;
  public static final int FPT_PLAY_IOS_INFEED_HOME = 318;

  public static final int FPT_PLAY_MOBILE_DEV_TEST = 333;


  public static final int USER_TYPE_VID = 2;
  public static final int USER_TYPE_REGISTERED = 1;

  static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();


  public static final String VAST2_3rd_TEMPLATE_PATH = "delivery-ads/vast2-third-parties.xml";
  public static final String VAST2_TEMPLATE_PATH = "delivery-ads/vast2.xml";
  public static final String VAST3_NATIVE_MOBILE = "delivery-ads/vast3.xml";
  public static final String VMAP_NATIVE_MOBILE = "delivery-ads/vmap.xml";
  public static final String VMAP_VPAID_AD = "delivery-ads/vmap-vpaid.xml";
  public static final String VMAP_3rd_AD = "delivery-ads/vmap-multi-slot-3rd-parties.xml";

  public static final String BASE_INSTREAM_CDN_HTTP =
      "http://ads-cdn.fptplay.net/static/ads/instream/";
  public static final String BASE_INSTREAM_CDN_HTTPS =
      "https://ads-cdn.fptplay.net/static/ads/instream/";
  public static final String BASE_ADSPLAY_CDN_HTTPS = "https://st.adsplay.net/video/";
  public static final String BASE_ADSPLAY_CDN_HTTP = "http://st.adsplay.net/video/";

  static final int CACHE_TIME_VAST_XML = 6;
  // WORKER_CONFIGS.getCustomConfig("baseVideoCDN") ;

  static final LoadingCache<Query, String> vastXmlCache = CacheBuilder.newBuilder()
      .maximumSize(2000000).expireAfterWrite(CACHE_TIME_VAST_XML, TimeUnit.SECONDS)
      .build(new CacheLoader<Query, String>() {
        public String load(Query q) {
          String xml = NO_AD_XML;

          String contentId = q.getContentId();
          int placement = q.getPlacement();

          // switch to SPOTX ads if the video from GLEE
          ContentMetaData contentMetaData = ContentDataUtil.getContentMetaData(contentId);
          // String vodType = contentMetaData.getType();
          // List<String> tagsPlot = contentMetaData.getTagsPlot();
          List<String> listCategories = contentMetaData.getListStructureId();

          // int dropoutScore = RandomUtil.getRandom(100) + 1;// 1 to 100

          // query
          AdModel model = AdUnitQueryUtil.queryVideoAd(q);

          VideoAdDataModel videoModel = model.getVideoAdModel();
          VmapDataModel vmapModel = model.getVmapAdModel();
          // swich model
          if (videoModel != null) {

            if (videoModel.getVastAdTagURI() != null) {
              xml = TemplateUtil.render(VAST2_3rd_TEMPLATE_PATH, videoModel);
            } else if (videoModel.hasAds()) {
              if (placement >= 300 && placement < 400) {
                xml = TemplateUtil.render(VAST3_NATIVE_MOBILE, videoModel);
              } else {
                xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, videoModel);
              }
            }

          } else {
            if (vmapModel.hasAds()) {
              if (placement > 200 && placement < 300) {
                xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, vmapModel);
              } else {
                xml = TemplateUtil.render(VMAP_3rd_AD, vmapModel);
              }
            }
          }
          model.freeResource();

          return xml;
        }
      });

  static final LoadingCache<Query, String> vastXmlLiveEventCache = CacheBuilder.newBuilder()
      .maximumSize(2000000).expireAfterWrite(CACHE_TIME_VAST_XML, TimeUnit.SECONDS)
      .build(new CacheLoader<Query, String>() {
        public String load(Query q) {
          int placement = q.getPlacement();
          String xml = NO_AD_XML;
          // query
          AdModel model = AdUnitQueryUtil.queryVideoAdById(q);
          VideoAdDataModel videoModel = model.getVideoAdModel();
          if (videoModel != null) {
            if (videoModel.hasAds()) {
              if (placement >= 300 && placement < 400) {
                xml = TemplateUtil.render(VAST3_NATIVE_MOBILE, videoModel);
              } else {
                xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, videoModel);
              }
            }
          }
          model.freeResource();
          return xml;
        }
      });

  /**
   * Cache xml value for TVC query.
   * 
   * @param q
   * @return xml parsed file.
   */
  private static String getVastTVC(Query q) {
    q.setAdType(Creative.ADTYPE_INSTREAM_VIDEO);
    try {
      return vastXmlCache.get(q);
    } catch (Exception e) {
      LogUtil.error(e);
    }
    return NO_AD_XML;
  }

  /**
   * Cache xml value for LiveEvent query.
   * 
   * @param q
   * @return xml parsed file
   */
  private static String getVastLiveEvent(Query q) {
    try {
      return vastXmlLiveEventCache.get(q);
    } catch (Exception e) {
      LogUtil.error(e);
    }
    return NO_AD_XML;
  }

  // static Map<String, Boolean> sonyMovies = new HashMap<>();

  public static String getVastXmlVast3(String ctid, String ip, String uuid, int userType,
      String catid, int placementId, int platformId, String useragent, String cityFromIp, int ut)
      throws Exception {
    // if (userType == USER_TYPE_VID) {
    // return NO_AD_XML_VMAP;
    // }
    String xml = NO_AD_XML_VMAP;
    {
      VmapDataModel model = new VmapDataModel();
      {
        // int t = DateTimeUtil.currentUnixTimestamp();
        StringBuilder u = new StringBuilder();
        u.append("http://d4.adsplay.net/delivery?placement=").append(placementId);
        u.append("&ctid=").append(ctid).append("&catid=").append(catid);
        u.append("&ut=").append(ut).append("&uuid=").append(uuid);
        // if(t%2 == 1) {
        model.addAdBreak(new AdBreak("start", u.toString()));
        // } else {
        // String url =
        // "http://tag.gammaplatform.com/adx/request/zid_1469031312/wid_1469030857/?content_page_url=&cb="+t+"&player_width=640&player_height=360&device_id=";
        // url = url + uuid;
        // model.addAdBreak(new AdBreak("start", url));
        // }
        // model.addAdBreak(new AdBreak("00:05:00",
        // "https://d3.adsplay.net/delivery?placement="+placementId));
      }
      xml = TemplateUtil.render(InstreamAdDataUtil.VMAP_NATIVE_MOBILE, model);
    }
    return xml;
  }

  public static String getVastXml(String contentId, String ip, String uuid, String cxt, String cxkw,
      int placementId, int platformId, String location, String cityFromIp, String url,
      String userAgent) throws Exception {
    boolean peakTime = AdTimeUtil.isPeakTime();
    String xml = NO_AD_XML;

    System.out.println("placementId: " + placementId + " contentId: " + contentId);


    int eplAdId = 1585;
    // =================== WEB LIVE TV =====================
    if (placementId == FPT_PLAY_WEB_LIVE_TV) {
      // Web Live TV
      boolean isEPLEvent = ContentDataUtil.checkEplLiveEvent(contentId);
      if (isEPLEvent && url.contains("premier-league")) {
        Query q = new Query(true, eplAdId, platformId, placementId, uuid);
        q.setContentId(contentId);
        q.setPurl(url);
        xml = getVastLiveEvent(q);
      } else {
        return NO_AD_XML;
      }
      // if (url.contains("livetv")) {
      // return NO_AD_XML;
      // } else {
      // Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_LIVE_TV, location);
      // q.setPeakTime(peakTime);
      // q.setContentId(contentId);
      // xml = getVastTVC(q);
      // }
    }

    // ================ WEB VOD =============================
    else if (placementId == FPT_PLAY_WEB_VOD || placementId == 1010) {
      if (url.contains("event")) {
        return NO_AD_XML;
      } else {

        int idxDemo = url.indexOf(ADSPLAY_DEMO);
        // System.out.println(ADSPLAY_DEMO + " ======== " + idxDemo);
        if (idxDemo > 0) {
          try {
            int adId = StringUtil
                .safeParseInt(url.substring(idxDemo).replace(ADSPLAY_DEMO, StringPool.BLANK));
            if (adId > 0) {
              AdModel model = AdUnitQueryUtil
                  .queryVideoAdById(new Query(true, adId, platformId, placementId, uuid));

              VideoAdDataModel videoModel = model.getVideoAdModel();
              xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, videoModel);
              return xml;
            }
          } catch (Throwable e) {
            LogUtil.error(e);
          }
        }

        String[] toks = cxkw.split(StringPool.COMMA);
        String[] keywords = new String[toks.length];
        for (int i = 0; i < toks.length; i++) {
          keywords[i] = toks[i].trim().toLowerCase();
        }

        System.out.println("==============> cxt: " + cxt);
        if (url.contains(XEM_EPL) || cxt.contains(PREMIER_LEAGUE)) {
          // show Toshiba only
          // Query q = new Query(true, 1421, platformId, placementId, uuid);
          // q.setContentId(contentId);
          // VideoAdDataModel model = AdUnitQueryUtil.queryVideoAdById(q);
          // xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, model);

          Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
          q.setMaleGroupOnVOD(UserSegmentUtil.isMaleGroupOnVOD(uuid, keywords));
          q.setFemaleGroupOnVOD(UserSegmentUtil.isFemaleGroupOnVOD(uuid, keywords));
          q.setPeakTime(peakTime);
          q.setContentId(contentId);
          q.setPurl(url);
          xml = getVastTVC(q);
        } else {
          Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
          q.setMaleGroupOnVOD(UserSegmentUtil.isMaleGroupOnVOD(uuid, keywords));
          q.setFemaleGroupOnVOD(UserSegmentUtil.isFemaleGroupOnVOD(uuid, keywords));
          q.setPeakTime(peakTime);
          q.setContentId(contentId);
          q.setPurl(url);
          xml = getVastTVC(q);
        }

      }
    }

    // spotX demo
    else if (placementId == 192458) {
      System.out.println("contentId " + contentId);
      if (!contentId.equals("5927fb5d5583204f4e6655e6")) {
        platformId = 1;
        Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
        q.setPeakTime(peakTime);
        q.setContentId(contentId);
        q.setPurl(url);
        q.setUserAgent(userAgent);
        xml = getVastTVC(q);
      }
    }

    // ================= FShare TVC Web Video Ad =================
    else if (placementId == FSHARE_TVC_PLACEMENT) {
      Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
      q.setPeakTime(peakTime);
      q.setContentId("fshare-web");
      q.setPurl(url);
      xml = getVastTVC(q);
    }

    // ================== SMARTTV Placements ======================
    else if (placementId == FPT_PLAY_SMARTTV_LIVE_TV) {
      // SmartTV Live TV
      boolean isEPLEvent = ContentDataUtil.checkEplLiveEvent(contentId);
      if (isEPLEvent && url.contains("premier-league")) {
        Query q = new Query(true, eplAdId, platformId, placementId, uuid);
        q.setContentId(contentId);
        q.setPurl(url);
        xml = getVastLiveEvent(q);
      } else {
        return NO_AD_XML;
      }
    }

    else if (placementId == FPT_PLAY_SMART_TV_VOD) {
      System.out.println("contentId " + contentId);
      if (!contentId.equals("5927fb5d5583204f4e6655e6")) {
        Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
        q.setPeakTime(peakTime);
        q.setContentId(contentId);
        q.setPurl(url);
        q.setUserAgent(userAgent);
        xml = getVastTVC(q);
      }
    }

    // ================ MOBILE ==================
    else if (placementId == FPT_PLAY_LIVE_STREAM_IOS
        || placementId == FPT_PLAY_LIVE_STREAM_ANDROID_APP
        || placementId == FPT_PLAY_LIVE_STREAM_ANDROID_BOX
        || placementId == FPT_PLAY_LIVE_STREAM_ANDROID_SMART_TV) {
      boolean isEPLEvent = ContentDataUtil.checkEplLiveEvent(contentId);
      if (isEPLEvent) {
        Query q = new Query(true, eplAdId, platformId, placementId, uuid);
        q.setContentId(contentId);
        xml = getVastLiveEvent(q);
      } else {
        return NO_AD_XML;
      }
    }
    // Mobile App VOD
    else if (placementId == FPT_PLAY_VOD_IOS) {
      Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
      q.setPeakTime(peakTime);
      q.setContentId(contentId);
      xml = getVastTVC(q);
    }
    // Android Smart TV or Android TV Box
    else if (placementId == FPT_PLAY_VOD_ANDROID_APP
        || placementId == FPT_PLAY_VOD_ANDROID_SMART_TV) {
      Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
      q.setPeakTime(peakTime);
      q.setContentId(contentId);
      q.setUserAgent(userAgent);
      xml = getVastTVC(q);
    }
    // Mobile App Test
    else if (placementId == FPT_PLAY_MOBILE_DEV_TEST) {

      List<Creative> listCrt = new ArrayList<>();

      int preRoll = 1631;
      Creative crt = MongoAdQuery.findCreativesById(preRoll);
      listCrt.add(crt);

      VmapDataModel model =
          AdUnitQueryUtil.mappingtoVmapAdModel(listCrt, uuid, placementId, contentId, true);
      xml = TemplateUtil.render(VMAP_VPAID_AD, model);
    }
    // ================ MOBILE ==================

    // other test
    else if (placementId == 666) {
      int sonyAdId = 1177;
      VideoAdDataModel model =
          AdUnitQueryUtil.queryByAdId(sonyAdId, platformId, placementId, uuid, contentId);
      xml = TemplateUtil.render(VAST2_TEMPLATE_PATH, model);
      // xml = getVastTVC(new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD,
      // location));
    }
    // System.out.println("xml "+xml);
    return xml;
  }
}
