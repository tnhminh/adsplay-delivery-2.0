package adsplay.server.delivery.query;

import adsplay.server.delivery.RoutingHandler;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import rfx.core.util.RandomUtil;

public class InstreamVideoAdFactory {

	private static final String TRACK_VIDEOADS = "/track/videoads";
	private static final String BASE_URL = RoutingHandler.BASE_DOMAIN	+ TRACK_VIDEOADS;
	
	static final String HTTPS = "https://";
	static final String log1FullUrl = HTTPS + RoutingHandler.LOG1_DOMAIN + TRACK_VIDEOADS;
	static final String log2FullUrl = HTTPS + RoutingHandler.LOG2_DOMAIN+ TRACK_VIDEOADS;
	static final String log3FullUrl = HTTPS + RoutingHandler.LOG3_DOMAIN + TRACK_VIDEOADS;
	static final String log4FullUrl = HTTPS + RoutingHandler.LOG4_DOMAIN + TRACK_VIDEOADS;
		
	public static InstreamVideoAd createInstreamVideoAd() {
		InstreamVideoAd m = new InstreamVideoAd();
		m.setBaseUrl(BASE_URL);
		//m.setLogUrl(BASE_URL);
				
		//sharding log
		long shardId = RandomUtil.getRandom(10)+1;
		if(shardId == 1){
			m.setLogFullBaseUrl(log1FullUrl);
		} 
		else if(shardId == 2){
			m.setLogFullBaseUrl(log2FullUrl);
		}
		else if(shardId == 3){
			m.setLogFullBaseUrl(log3FullUrl);
		}
		else {
			m.setLogFullBaseUrl(log4FullUrl);	
		}
		
		return m;
	}	
}
