package adsplay.server.delivery.query;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import adsplay.data.Creative;
import adsplay.data.dao.AdsPlayMongoDbDAO;
import adsplay.data.dao.MongoDbCommand;

public class MongoAdQuery {
  private static final String EMPTY_STRING = "";
static AdsPlayMongoDbDAO adsPlayMongoDbDAO = AdsPlayMongoDbDAO.getInstance();

  public static List<Creative> getAllCreatives(int monthOffset) {
    return new MongoDbCommand<List<Creative>>(adsPlayMongoDbDAO) {
      @Override
      protected List<Creative> build() throws Exception {
        Datastore datastore = getDatastore();
        final Query<Creative> query = datastore.find(Creative.class);

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, (monthOffset * -1));
        query.field("crtDate").greaterThanOrEq(c.getTime());
        return query.asList();
      }
    }.execute();
  }

  public static List<Creative> findCreativesByTargeting(int adType, List<Integer> targetedPmIds) {
    return findCreativesByTargeting(false, adType, targetedPmIds, null, null, null, null, null);
  }

  public static List<Creative> findCreativesByTargeting(int adType, List<Integer> targetedPmIds,
      List<Integer> targetedPlatforms) {
    return findCreativesByTargeting(false, adType, targetedPmIds, targetedPlatforms, null, null,
        null, null);
  }

  public static List<Creative> findCreativesByTargeting(int adType, List<Integer> targetedPmIds,
      List<Integer> targetedPfIds, List<String> locs) {
    return findCreativesByTargeting(false, adType, targetedPmIds, targetedPfIds, locs, null, null,
        null);
  }

  public static List<Creative> findCreativesByTargeting(int adType, List<Integer> targetedPmIds,
      List<Integer> targetedPfIds, List<String> locs, List<Integer> targetedGenders) {
    return findCreativesByTargeting(false, adType, targetedPmIds, targetedPfIds, locs,
        targetedGenders, null, null);
  }
  
    public static List<Creative> findCreativesByTargeting(boolean demo, int adType, List<Integer> targetedPmIds,
            List<Integer> targetedPfIds, List<String> locs, List<Integer> targetedGenders, List<Integer> targetedCategoryIds,
            List<String> targetedStructureId) {
        return findCreativesByTargeting(demo, adType, targetedPmIds, targetedPfIds, locs, targetedGenders, targetedCategoryIds,
                targetedStructureId, null, null);
    }

  public static List<Creative> findCreativesByTargeting(boolean demo, int adType,
      List<Integer> targetedPmIds, List<Integer> targetedPfIds, List<String> locs,
      List<Integer> targetedGenders, List<Integer> targetedCategoryIds,
      List<String> targetedStructureId, List<Integer> targetedMovieIds, List<Integer> targetedCopyrightIds, boolean isPayTVQuery) {
    return new MongoDbCommand<List<Creative>>(adsPlayMongoDbDAO) {
      @Override
      protected List<Creative> build() throws Exception {
        Datastore datastore = getDatastore();
        final Query<Creative> query = datastore.find(Creative.class);
        query.field("adType").equal(adType);

        if (!demo) {
          query.field("status").equal(Creative.ADSTATUS_RUNNING);// must be valid ad unit
        }

        buildQuery(targetedPmIds, isPayTVQuery, query, "tgpms");

        buildQuery(targetedPfIds, isPayTVQuery, query, "tgpfs");
        
        buildQuery(locs, isPayTVQuery, query, "tglocs");
        
        buildQuery(targetedGenders, isPayTVQuery, query, "tggds");
          
        buildQuery(targetedCategoryIds, isPayTVQuery, query, "tgcats");
        
        buildQuery(targetedStructureId, isPayTVQuery, query, "tgkws");
        
        buildQuery(targetedMovieIds, isPayTVQuery, query, "tgmv");
        
        buildQuery(targetedCopyrightIds, isPayTVQuery, query, "tgcpr");
        
        System.out.println("Query to database with statement: " + query.toString());

        Date now = new Date();
        query.field("runDate").lessThanOrEq(now);

        Calendar c = Calendar.getInstance();
        c.setTime(now);
        c.add(Calendar.DATE, -1);
        query.field("expDate").greaterThanOrEq(c.getTime());
        query.order("-score");
        return query.asList();
      }

    private void buildQuery(List<? extends Object> targetedFields, boolean isPayTVQuery, final Query<Creative> query, String fieldString) {
        if (targetedFields != null) {
          // filter by placements
          if (targetedFields.size() > 0) {
              if (isPayTVQuery){
                  query.criteria(fieldString).hasAnyOf(targetedFields);
              } else {
                  if ("tggds".equals(fieldString)){
                      query.or(query.criteria(fieldString).doesNotExist(),query.criteria(fieldString).hasAnyOf(Arrays.asList(Creative.GENDER_TARGET_ALL)), query.criteria(fieldString).hasAnyOf(targetedFields));
                  } else {
                      query.or(query.criteria(fieldString).doesNotExist(), query.criteria(fieldString).hasAnyOf(targetedFields));
                  }
              }
          }
        } else {
            if (isPayTVQuery){
            query.criteria(fieldString).doesNotExist();
            }
        }
    }
    }.execute();
  }
  
  public static List<Creative> findCreativesByTargeting(boolean demo, int adType,
          List<Integer> targetedPmIds, List<Integer> targetedPfIds, List<String> locs,
          List<Integer> targetedGenders, List<Integer> targetedCategoryIds,
          List<String> targetedStructureId, List<Integer> targetedMovieIds, List<Integer> targetedCopyrightIds) {
      return findCreativesByTargeting(demo, adType, targetedPmIds, targetedPfIds, locs, targetedGenders, targetedCategoryIds, targetedStructureId, targetedMovieIds, targetedCopyrightIds, false);
  }


  public static Creative findCreativesById(int id) {
    return new MongoDbCommand<Creative>(adsPlayMongoDbDAO) {
      @Override
      protected Creative build() throws Exception {
        // Date now = new Date();
        Datastore datastore = getDatastore();
        final Query<Creative> query = datastore.find(Creative.class);
        query.field("_id").equal(id);
        query.field("status").equal(Creative.ADSTATUS_RUNNING);// must be valid ad unit
        List<Creative> list = query.asList();
        return list.size() > 0 ? list.get(0) : null;
      }
    }.execute();
  }

  public static List<Creative> findCreativesByListId(List<Integer> listId) {
    return new MongoDbCommand<List<Creative>>(adsPlayMongoDbDAO) {
      @Override
      protected List<Creative> build() throws Exception {
        // Date now = new Date();
        Datastore datastore = getDatastore();
        final Query<Creative> query = datastore.find(Creative.class);
        query.criteria("_id").hasAnyOf(listId);
        query.field("status").equal(Creative.ADSTATUS_RUNNING);// must be valid ad unit
        List<Creative> list = query.asList();
        return list.size() > 0 ? list : null;
      }
    }.execute();
  }
}
