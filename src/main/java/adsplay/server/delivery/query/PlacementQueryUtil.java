package adsplay.server.delivery.query;

import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.api.dao.PlacementMongoDbDao;
import adsplay.data.Placement;

public class PlacementQueryUtil {

	static final LoadingCache<Integer, Placement> queriedCache = CacheBuilder.newBuilder()
			.maximumSize(10000).expireAfterWrite(6, TimeUnit.HOURS).build(new CacheLoader<Integer, Placement>() {
				public Placement load(Integer id) {
					System.out.println("### PlacementQueryUtil, MISS CACHE id:"+id);
					Placement pm = PlacementMongoDbDao.get(id);
					return pm;
				}
			});
	
	public static Placement get(int placementId){
		try {
			return queriedCache.get(placementId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PlacementMongoDbDao.get(placementId);
	}

}
