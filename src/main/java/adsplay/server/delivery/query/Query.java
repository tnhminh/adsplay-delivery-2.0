package adsplay.server.delivery.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import adsplay.data.Creative;

public class Query implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -593243868837720868L;

  public static int CONTENT_GROUP_VOD = 1;
  public static int CONTENT_GROUP_SPORT = 2;
  public static int CONTENT_GROUP_LIVE_TV = 3;

  private int placement;
  private String uuid;
  private String contentId;
  private int platformId = 0;
  private int targetGroup = 0;
  private int categoryId = 0;
  private String location;
  private int adType = Creative.ADTYPE_INSTREAM_VIDEO;
  private int adId = 0;

  boolean isMaleGroupOnVOD;
  boolean isFemaleGroupOnVOD;
  boolean isPeakTime;
  boolean isLiveTV;
  String tvChannelName;
  boolean isIos = false;
  boolean demo = false;
  String purl = "";
  String userAgent = "";
  
  // Adding 2 attribute for query in MongoDB
  private int movieId = 0;
  private int copyrightId = 0;

  private List<Integer> listAdId = new ArrayList<>();
  private List<String> listStructureId = new ArrayList<>();

  public Query() {

  }

  public Query(boolean demo, int adId, int platformId, int placement, String uuid) {
    super();
    this.demo = true;
    this.adId = adId;
    this.platformId = platformId;
    this.placement = placement;
    this.uuid = uuid;
  }

  public Query(int adId, int platformId, int placement, String uuid) {
    super();
    this.adId = adId;
    this.platformId = platformId;
    this.placement = placement;
    this.uuid = uuid;
  }

  public Query(int platformId, int placement, String uuid, int targetGroup) {
    super();
    this.platformId = platformId;
    this.placement = placement;
    this.uuid = uuid;
    this.targetGroup = targetGroup;
  }

  public Query(int platformId, int placement, String uuid, int targetGroup, String location) {
    super();
    this.platformId = platformId;
    this.placement = placement;
    this.uuid = uuid;
    this.targetGroup = targetGroup;
    this.location = location;
  }

  public Query(int platformId, int placement, String uuid, int targetGroup, String location,
      int categoryId) {
    super();
    this.platformId = platformId;
    this.placement = placement;
    this.uuid = uuid;
    this.targetGroup = targetGroup;
    this.location = location;
    this.categoryId = categoryId;
  }

  public Query(int platformId, int placement) {
    super();
    this.platformId = platformId;
    this.placement = placement;
  }

  public Query(String uuid, int placement) {
    super();
    this.uuid = uuid;
    this.placement = placement;
  }

  public int getPlatformId() {
    return platformId;
  }

  public int getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(int categoryId) {
    this.categoryId = categoryId;
  }

  public void setPlatformId(int platformId) {
    this.platformId = platformId;
  }

  public int getPlacement() {
    return placement;
  }

  public void setPlacement(int placement) {
    this.placement = placement;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public int getTargetGroup() {
    return targetGroup;
  }

  public void setTargetGroup(int targetGroup) {
    this.targetGroup = targetGroup;
  }

  public boolean isMaleGroupOnVOD() {
    return isMaleGroupOnVOD;
  }

  public boolean isFemaleGroupOnVOD() {
    return isFemaleGroupOnVOD;
  }

  public void setMaleGroupOnVOD(boolean isMaleGroupOnVOD) {
    this.isMaleGroupOnVOD = isMaleGroupOnVOD;
  }

  public void setFemaleGroupOnVOD(boolean isFemaleGroupOnVOD) {
    this.isFemaleGroupOnVOD = isFemaleGroupOnVOD;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getAdType() {
    return adType;
  }

  public void setAdType(int adType) {
    this.adType = adType;
  }

  public boolean isLiveTV() {
    return isLiveTV;
  }

  public String getTvChannelName() {
    return tvChannelName;
  }

  public void setLiveTV(boolean isLiveTV) {
    this.isLiveTV = isLiveTV;
  }

  public void setTvChannelName(String tvChannelName) {
    this.tvChannelName = tvChannelName;
  }

  public boolean isPeakTime() {
    return isPeakTime;
  }

  public void setPeakTime(boolean isPeakTime) {
    this.isPeakTime = isPeakTime;
  }

  public String getContentId() {
    return contentId;
  }

  public void setContentId(String contentId) {
    this.contentId = contentId;
  }

  public boolean isIos() {
    return isIos;
  }

  public void setIos(boolean isIos) {
    this.isIos = isIos;
  }

  public int getAdId() {
    return adId;
  }

  public void setAdId(int adId) {
    this.adId = adId;
  }

  public String getPurl() {
    if (purl == null) {
      purl = "";
    }
    return purl;
  }

  public void setPurl(String purl) {
    this.purl = purl;
  }

  public boolean isDemo() {
    return demo;
  }

  public void setDemo(boolean demo) {
    this.demo = demo;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public List<Integer> getListAdId() {
    return listAdId;
  }

  public void addListAdId(int adId) {
    this.listAdId.add(adId);
  }

  public List<String> getListStructureId() {
    return listStructureId;
  }

  public void setListStructureId(List<String> listStructureId) {
    this.listStructureId = listStructureId;
  }

  public int getMovieId() {
	return movieId;
  }

  public void setMovieId(int movieId) {
	this.movieId = movieId;
  }

  public int getCopyrightId() {
	return copyrightId;
  }

  public void setCopyrightId(int copyrightId) {
	this.copyrightId = copyrightId;
  }

@Override
  public String toString() {
    return new Gson().toJson(this);
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return this.toString().equals(obj.toString());
  }
}
