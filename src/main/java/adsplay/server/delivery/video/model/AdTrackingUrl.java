package adsplay.server.delivery.video.model;

public class AdTrackingUrl {
	String url;
	int delay;
	String event;
	
	public AdTrackingUrl(String event,String url, int delay) {
		super();
		this.event = event;
		this.url = url;
		this.delay = delay;
	}
	
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
}
