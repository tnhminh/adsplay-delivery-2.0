package adsplay.server.delivery.video.model;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import adsplay.common.AdBeaconUtil;
import adsplay.common.VASTXmlParser;
import adsplay.common.VASTXmlParser.MediaFile;
import adsplay.common.template.DataModel;
import adsplay.data.Creative;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import io.netty.handler.codec.http.HttpHeaders;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.StringUtil;

public class InstreamVideoAd implements DataModel {
    private static final String HTTP2 = "http";
    public static final String HTTP = "http://";
    private int adId = 0;
    private String logUrl;
    private String baseUrl;
    private String adTitle;

    private String contentId = "";
    private String adBeacon = "";
    private String skipoffset = "00:00:05";
    private String duration = "00:00:00";
    private String startTime = "00:00:00";

    private int placementId;
    private int flighId;

    private String clickThrough = "";
    private String encodedClickThrough = "";
    private String mediaFile = "";
    
    private List<MediaFile> mediaFiles = new ArrayList<MediaFile>();

    private String uuid = "";
    private int time = DateTimeUtil.currentUnixTimestamp();


    private boolean thirdPartyAd;
    private String trackingEvent3rdTags = "", videoClicksTags = "";
    private List<AdTrackingUrl> streamAdTrackingUrls;
    private List<String> impression3rdUrls = new ArrayList<>();

    
    private int campaignId ;

    private String buyType = "";
    
    private String parentCategory = "";

    public InstreamVideoAd() {
    }

    public InstreamVideoAd(Creative crt) {
        this.adTitle = crt.getName();
        this.adId = crt.getId();
        this.clickThrough = crt.getClickThrough();
        this.duration = crt.getDuration();
        this.skipoffset = crt.getSkipoffset();
        setMediaFile(crt.getMedia(), false);
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public String getBuyType() {
        return buyType;
    }

    public void setBuyType(String buyType) {
        this.buyType = buyType;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = HTTP + baseUrl;
    }

    public String getLogUrl() {
        return logUrl;
    }

    public void setLogUrl(String logUrl) {
        this.logUrl = HTTP + logUrl;
    }

    public void setLogFullBaseUrl(String logUrl) {
        this.logUrl = logUrl;
    }

    public String getClickThrough() {
        return clickThrough;
    }

    public void setClickThrough(String clickThrough) {
        this.clickThrough = clickThrough;
        try {
            this.encodedClickThrough = URLEncoder.encode(clickThrough, "UTF-8");
        } catch (Exception e) {
        }
    }

    public String getEncodedClickThrough() {
        return encodedClickThrough;
    }

    public String getMediaFile() {
        return mediaFile;
    }

    public String getUuid() {
        return uuid;
    }

    public void setMediaFile(String mediaFile) {
        if (!mediaFile.startsWith(HTTP2)) {
            this.mediaFile = InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTPS + mediaFile;
        } else {
            this.mediaFile = mediaFile;
        }
    }

    public void setMediaFile(String mediaFile, boolean isSSL) {
        if (!mediaFile.startsWith(HTTP2)) {
            System.out.println(this.placementId);
            if (isSSL) {
                if (placementId > 200 && placementId < 300) {
                    // this.mediaFile =
                    // InstreamAdDataUtil.BASE_ADSPLAY_CDN_HTTPS + mediaFile;
                    this.mediaFile = InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTPS + mediaFile;
                } else {
                    this.mediaFile = InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTPS + mediaFile;
                }

            } else {
                if (placementId > 200 && placementId < 300) {
                    // this.mediaFile = InstreamAdDataUtil.BASE_ADSPLAY_CDN_HTTP
                    // + mediaFile;
                    this.mediaFile = InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTP + mediaFile;
                } else {
                    this.mediaFile = InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTP + mediaFile;
                }
            }

        } else {
            this.mediaFile = mediaFile;
        }
    }
    
    public void setMediaFiles(List<MediaFile> mediaUrls) {
        mediaFiles.addAll(mediaUrls);
    }
    
    public List<MediaFile> getMediaFiles() {
        return mediaFiles;
    }

    public String getSkipoffset() {
        return skipoffset;
    }

    public void setSkipoffset(String skipoffset) {
        this.skipoffset = skipoffset;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getAdBeacon() {
        return adBeacon;
    }

    public void setAdBeacon(String adBeacon) {
        this.adBeacon = adBeacon;
    }

    public String getStartTime() {
        return startTime;
    }

    public int getPlacementId() {
        return placementId;
    }

    public void setPlacementId(int placementId) {
        this.placementId = placementId;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean hasAd() {
        return this.adId > 0;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    @Override
    public void freeResource() {
        // TODO Auto-generated method stub
    }

    @Override
    public String getClasspath() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isOutputable() {
        return true;
    }

    @Override
    public List<HttpHeaders> getHttpHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isThirdPartyAd() {
        return thirdPartyAd;
    }

    public void setThirdPartyAd(boolean thirdPartyAd, List<AdTrackingUrl> streamAdTrackingUrls) {
        this.thirdPartyAd = thirdPartyAd;
        this.streamAdTrackingUrls = streamAdTrackingUrls;
    }

    /**
     * list ad tracking 3rd url for in-page video , specific ad format (infeed,
     * masthead, lightbox, 360 video, in-article video)
     * 
     * @return List<AdTrackingUrl>
     */
    public List<AdTrackingUrl> getOnlyAdTracking3rdUrls() {
        if (streamAdTrackingUrls == null) {
            streamAdTrackingUrls = new ArrayList<>(0);
        }
        return streamAdTrackingUrls;
    }

    /**
     * auto tracking log
     * 
     * @return List<AdTrackingUrl>
     */
    public List<AdTrackingUrl> getStreamAdTrackingUrls() {
        if (streamAdTrackingUrls == null) {
            streamAdTrackingUrls = new ArrayList<>();
        }
        String urlImp = "https://log4.adsplay.net/track/videoads?metric=impression&adid=" + adId + "&beacon=" + adBeacon + "&t="
                + DateTimeUtil.currentUnixTimestamp();
        streamAdTrackingUrls.add(new AdTrackingUrl(VASTXmlParser.VAST_EVENT_IMPRESSION, urlImp, 1));
        String urlView100 = "https://log4.adsplay.net/track/videoads?metric=view100&adid=" + adId + "&beacon=" + adBeacon + "&t="
                + DateTimeUtil.currentUnixTimestamp();
        streamAdTrackingUrls.add(new AdTrackingUrl(VASTXmlParser.VAST_EVENT_COMPLETE, urlView100, 5));
        return streamAdTrackingUrls;
    }

    public List<String> getImpression3rdUrls() {
        return impression3rdUrls;
    }

    public String getTrackingEvent3rdTags() {
        return trackingEvent3rdTags;
    }

    public void setImpression3rdUrls(List<String> impression3rdUrls) {
        this.impression3rdUrls = impression3rdUrls;
        this.thirdPartyAd = true;
    }

    public void setTrackingEvent3rdTags(String trackingEvent3rdTags) {
        this.trackingEvent3rdTags = trackingEvent3rdTags;
        this.thirdPartyAd = true;
    }

    public String getVideoClicksTags() {
        if (StringUtil.isEmpty(videoClicksTags)) {
            videoClicksTags = "<ClickThrough><![CDATA[" + clickThrough + "]]></ClickThrough>";
        } else if (!StringUtil.isEmpty(clickThrough)) {
            videoClicksTags = "<ClickThrough><![CDATA[" + clickThrough + "]]></ClickThrough>" + videoClicksTags;
        }
        return videoClicksTags;
    }

    public void setVideoClicksTags(String videoClicksTags) {
        this.videoClicksTags = videoClicksTags;
        this.thirdPartyAd = true;
    }

    public InstreamVideoAd buildBeaconData(String uuid, int placementId, int campaignId, int flightId) {
        this.uuid = uuid;
        this.adBeacon = AdBeaconUtil.createBeaconValue(getAdId(), uuid, placementId, campaignId, flightId) + "&ctid=" + this.contentId;
        this.placementId = placementId;
        return this;
    }

    public int getFlighId() {
        return flighId;
    }

    public void setFlighId(int flighId) {
        this.flighId = flighId;
    }

}
