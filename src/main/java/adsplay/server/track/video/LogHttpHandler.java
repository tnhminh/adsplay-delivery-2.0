package adsplay.server.track.video;



import static io.netty.handler.codec.http.HttpHeaderNames.CONNECTION;

import adsplay.common.BaseHttpLogHandler;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.server.track.click.ClickLogHttpHandler;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.StringUtil;
import server.http.util.HttpTrackingUtil;
import server.http.util.KafkaLogHandlerUtil;
import server.http.util.RedirectUtil;

public class LogHttpHandler extends BaseHttpLogHandler  {
	 
	private static final String A_C = "a-c";
	private static final String A_I = "a-i";
	private static final String V_C = "v-c";
	private static final String V_I = "v-i";
	private static final String V_T = "v-t";
	
	static final String logImpression = "/video/cpm/i";
	static final String logImpressionKafkaKey = getKafkaKey(logImpression);
	
	static final String logClick = "/video/cpm/c";	
	static final String logClickKafkaKey = getKafkaKey(logClick);
	
	static final String logTrack = "/video/track";
	static final String logTrackKafkaKey = getKafkaKey(logTrack);	
	
	static final String logAppImp = "/app/i";
	static final String logAppImpKafkaKey = getKafkaKey(logAppImp);
	
	static final String logAppClick = "/app/c";
	static final String logAppClickKafkaKey = getKafkaKey(logAppClick);
		
	@Override
	public void handle(HttpServerRequest req) {
		String uri = req.uri();		
		//System.out.println("URI: " + uri);
		
		HttpServerResponse resp = req.response();
		MultiMap headers = resp.headers();
		headers.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);	
		
		String headerOrigin = headers.get("Origin");
		if(StringUtil.isNotEmpty(headerOrigin)){
			headers.add("Access-Control-Allow-Origin",headerOrigin);
			headers.add("Access-Control-Allow-Credentials", "true");
			headers.add("Access-Control-Allow-Methods", "POST, GET");
			headers.add("Access-Control-Allow-Headers", "origin, content-type, accept, Set-Cookie");
		}		
		try {
			if(handleCommonRequest(uri, req, resp)){
				return;
			}
			
			//handle log request for ad video
			if(uri.startsWith(logTrack)){				
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logTrackKafkaKey);	
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), V_T);
			} 
			else if(uri.startsWith(logImpression)){							
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logImpressionKafkaKey);
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), V_I);
			}	
			else if(uri.startsWith(logClick)){					
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logClickKafkaKey);		
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), V_C);
			}
			
			//handle log request for ad mobile app
			else if(uri.startsWith(logAppImp)){					
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logAppImpKafkaKey);		
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), A_I);
			}
			else if(uri.startsWith(logAppClick)){
				MultiMap params = req.params();
				String link = params.get("link");
				int index = -1;
				if(StringUtil.isNotEmpty(link)){
					index = link.indexOf(ClickLogHttpHandler.REDIRECT_PREFIX);
				}
				if( index > 0 ){
					System.out.println("link "+link);
					String clickUrl = link.substring(index+3);
					System.out.println("clickUrl "+clickUrl);
					KafkaLogHandlerUtil.log(req, logAppClickKafkaKey);
					RedirectUtil.redirect(clickUrl, req);
				} else {
					RedirectUtil.redirect(uri, req);
				}				
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), A_C);
			}
			
			//no handler found
			else {
				resp.end("Not handler found for uri:"+uri);
			}
		} catch (Exception e) {
			resp.end("uri:"+uri + " error:"+e.getMessage());
		}
	}

}
