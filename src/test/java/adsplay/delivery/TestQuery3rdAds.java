package adsplay.delivery;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adsplay.common.AdTimeUtil;
import adsplay.common.template.AdModel;
import adsplay.common.template.TemplateUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.query.Query;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import rfx.core.util.Utils;

public class TestQuery3rdAds {
  static Gson gson = new GsonBuilder().setPrettyPrinting().create();

  public static void main(String[] args) {
    boolean peakTime = AdTimeUtil.isPeakTime();
    int platformId = Creative.PLATFORM_PC;
    int placementId = 102;
    String uuid = "abcd";
    String location = "vn-north";

    Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
    q.setPeakTime(peakTime);
    q.setLiveTV(false);
    q.setAdType(Creative.ADTYPE_INSTREAM_VIDEO);
    q.setMaleGroupOnVOD(true);
    // q.setFemaleGroupOnVOD(true);

    AdModel model = AdUnitQueryUtil.queryVideoAd(q);
    VideoAdDataModel videoModel = model.getVideoAdModel();
    Utils.sleep(5000);
    model = AdUnitQueryUtil.queryVideoAd(q);
    System.out.println(videoModel.getInstreamVideoAds());
    String xml = TemplateUtil.render(InstreamAdDataUtil.VAST2_TEMPLATE_PATH, videoModel);
    System.out.println(xml);
  }

}
