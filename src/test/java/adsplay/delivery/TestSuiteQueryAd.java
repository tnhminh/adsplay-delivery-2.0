package adsplay.delivery;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import adsplay.api.dao.CreativeMongoDbDao;
import adsplay.data.Creative;
import rfx.core.util.DateTimeUtil;

@RunWith(Suite.class)
@SuiteClasses({ MongoDbQueryTest.class })
public class TestSuiteQueryAd {

	@BeforeClass
	public static void initSampleAds() {
		try {
			DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.DATE_HOUR_FORMAT_PATTERN);

			// init sample data CreativeHourlyStats
			Creative crt1 = new Creative(168, "iTVad-Hyundai-trial-test-2016", "168-sd.mp4", "itvad",
					dateFormat.parse("2016-03-30-18"), dateFormat.parse("2016-03-30-18"),
					dateFormat.parse("2016-04-03-18"));
			crt1.setClickThrough("http://www.hyundai-thanhcong.vn/san-pham/tucson?utm_source=iTVad&utm_medium=Preroll&utm_campaign=Tuscon");
			crt1.setSkipoffset("00:00:05");
			crt1.setDuration("00:00:29");
			crt1.setCampaignId(10168);
			crt1.setFlightId(1168);
			crt1.setPricingModel(Creative.PRICING_MODEL_CPM);
			crt1.setStatus(Creative.ADSTATUS_RUNNING);
			crt1.setTotalBooking(200000);
			crt1.setScore(30);
			// set targeting fields
			crt1.setTargetedGenders(Arrays.asList(Creative.GENDER_TARGET_ALL));
			//crt1.setTargetedLocations(Arrays.asList("ho chi minh", "ha noi"));
			crt1.setTargetedKeywords(Arrays.asList("phim bộ", "tv show"));
			crt1.setTargetedPlacements(Arrays.asList(101, 102, 201));
			crt1.setTargetedPlatforms(Arrays.asList(Creative.PLATFORM_PC, Creative.PLATFORM_SMART_TV, Creative.PLATFORM_NATIVE_APP));
			CreativeMongoDbDao.saveCreative(crt1);
			

			Creative crt2 = new Creative(180, "iTVad-TVC-Test-PhuTaiLand", "168-sd.mp4", "itvad",
					dateFormat.parse("2016-03-30-18"), dateFormat.parse("2016-03-30-18"),
					dateFormat.parse("2016-04-03-18"));
			crt2.setClickThrough("http://phutailand.vn/Chung-cu/Chung-cu-Mon-City-My-Dinh-i862.html");
			crt2.setSkipoffset("00:00:05");
			crt2.setDuration("00:00:15");
			crt2.setCampaignId(10180);
			crt2.setFlightId(1180);
			crt2.setPricingModel(Creative.PRICING_MODEL_CPM);
			crt2.setStatus(Creative.ADSTATUS_RUNNING);
			crt2.setTotalBooking(200000);
			crt2.setScore(10);
			// set targeting fields
			crt2.setTargetedGenders(Arrays.asList(Creative.GENDER_TARGET_ALL));
			crt2.setTargetedKeywords(Arrays.asList("phim bộ", "tv show"));
			crt2.setTargetedPlacements(Arrays.asList(101, 102, 201));
			crt2.setTargetedPlatforms(
					Arrays.asList(Creative.PLATFORM_PC, Creative.PLATFORM_SMART_TV, Creative.PLATFORM_NATIVE_APP));
			CreativeMongoDbDao.saveCreative(crt2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
