package adsplay.mongo.test;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import adsplay.api.dao.CreativeMongoDbDao;
import adsplay.common.VASTXmlParser;
import adsplay.common.template.TemplateUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.query.InstreamVideoAdFactory;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import rfx.core.util.Utils;

public class CreativeQueryTest {

	static ConcurrentMap<Integer, List<Creative>> cachePlacementToCreatives = new ConcurrentHashMap<>();
	static {
		new Timer(true).schedule(new TimerTask() {			
			@Override
			public void run() {
				List<Creative> creatives = CreativeMongoDbDao.getRunningCreativesSortedByScore(Creative.ADTYPE_INSTREAM_VIDEO);				
				List<Creative> matchedCreatives = Arrays.asList(creatives.get(0), creatives.get(1));
				
				int[] validPlacementIds = new int[]{101, 102, 201, 202, 301, 302};
				for (int id : validPlacementIds) {
					cachePlacementToCreatives.put(id, matchedCreatives);
				}
			}
		}, 100, 3000);
	}
	
	public static void main(String[] args) {
		Utils.sleep(2000);
		
		int placementId = 101;
		String uuid = "0011eb68b0d34ab085a9d0160b70d943";
		
		VideoAdDataModel finalAdModel = null;
		
		
		try {
			finalAdModel = cachePlacementToCreatives.get(placementId).stream().filter(crt -> {
				System.out.println(crt);				
				return true;
			}).map((Creative crt) -> {
				VideoAdDataModel adModel = new VideoAdDataModel();				
				InstreamVideoAd m = InstreamVideoAdFactory.createInstreamVideoAd();	
				m.setAdId( crt.getId() );
				m.setAdTitle(crt.getName());
				m.setMediaFile(InstreamAdDataUtil.BASE_INSTREAM_CDN_HTTP + crt.getMedia(),false);						
				m.setClickThrough(crt.getClickThrough());	
				m.setDuration(crt.getDuration());
				m.setSkipoffset(crt.getSkipoffset());
				m.setStartTime("00:00:00");		
				adModel.addInstreamVideoAd(m.buildBeaconData(uuid,placementId, crt.getCampaignId(), crt.getFlightId()));				
				return adModel;
			}).findFirst().get();
		} catch (Exception e) {}	
		
		String xml = VASTXmlParser.NO_AD_XML;
		if(finalAdModel != null){
			xml = TemplateUtil.render(InstreamAdDataUtil.VAST2_TEMPLATE_PATH, finalAdModel);	
		}
		System.out.println(xml);
		
		Utils.sleep(2000);
		
	}
	
}
