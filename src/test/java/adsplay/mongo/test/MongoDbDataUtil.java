package adsplay.mongo.test;

import java.util.Calendar;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.google.gson.Gson;
import com.mongodb.MongoClient;

import adsplay.data.Creative;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import rfx.core.util.Utils;

public class MongoDbDataUtil {
	static MongoClient mongoClient = new MongoClient( "118.69.184.49", 11491 );

	public static void main(String[] args) {
		
		Calendar cal = Calendar.getInstance(); 
		long time = System.currentTimeMillis();
		cal.setTimeInMillis(time);
		int now = (int) (time / 1000);
		//cal.add(Calendar.HOUR, 1);
		cal.add(Calendar.WEEK_OF_MONTH, 1);
		int expiredTime = (int) (cal.getTimeInMillis() / 1000);
		
		final Morphia morphia = new Morphia();
		morphia.mapPackage("adsplay.data");
		final Datastore datastore = morphia.createDatastore(mongoClient, "adsplay");
		datastore.ensureIndexes();
				
		Creative creative1 = new Creative(102, "BiMatDemChuNhat", "BMDCN-1806-PC.mp4", "https://www.facebook.com/BIMATDEMCHUNHAT.DONGTAY", false,now,now, expiredTime);	
		creative1.setDuration("00:20:00");
		datastore.save(creative1);
		
		Utils.sleep(1000);
		
		Creative creative2 = new Creative(103, "Nhacso-Intro", "nhacso-intro-crt103.mp4","http://nhacso.net",false,now,now,expiredTime);		
		creative2.setDuration("00:00:00");
		datastore.save(creative2);
		
		Utils.sleep(1000);
		
		final Query<Creative> query = datastore.createQuery(Creative.class);
		query.order("-runDate");
		
		final List<Creative> creatives = query.asList();
		for (Creative creative : creatives) {
			//System.out.println(creative.getName() + " " + creative.getRunningDate());
			InstreamVideoAd m = new InstreamVideoAd(creative);
			System.out.println(new Gson().toJson(m));
		}		
		
		Utils.sleep(1000);
		
		mongoClient.close();
		Utils.sleep(2000);
		
	}
	
}
