package adsplay.mongo.test;

import java.util.List;

import adsplay.data.Creative;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.Query;

public class PayTvTestQuery {
	
	static final int platformId = 6;
	static final int placementId = 320;

	public static void main(String[] args) {
		Query query = new Query(platformId, placementId);
		List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(query);
		
		for (Creative creative : creatives) {
			System.out.println(creative.getName());	
		}
		
	}
}
