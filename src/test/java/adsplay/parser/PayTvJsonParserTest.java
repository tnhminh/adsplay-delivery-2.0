package adsplay.parser;

import java.io.IOException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import rfx.core.util.FileUtils;

public class PayTvJsonParserTest {

	public static void main(String[] args) throws IOException {
		String rawjson = FileUtils.readFileAsString("logs/sample-paytv-log.txt");
		System.out.println(rawjson);
		
		JsonParser parser = new JsonParser();
		JsonElement  obj = parser.parse(rawjson);
		
					
		
		
		if(obj != null){
			JsonObject fieldObj = obj.getAsJsonObject().getAsJsonObject("fields");
			System.out.println(fieldObj);
			
			String adEvent = fieldObj.get("Event").getAsString();//CompleteAdv or SkipAdv
			String customerID = fieldObj.get("CustomerID").getAsString();
			System.out.println(customerID);
		}
	}
}
