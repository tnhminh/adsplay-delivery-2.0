package adsplay.test.migrater;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import adsplay.api.dao.CreativeMongoDbDao;
import adsplay.data.Creative;
import adsplay.server.delivery.query.MongoAdQuery;

public class MongoDbToPostgreSql {

	public static void main(String[] args) throws ParseException {
		
		int monthOffset = 2;
		List<Creative> creatives = MongoAdQuery.getAllCreatives(monthOffset);
		
		int totalImp = 0, totalClick = 0, totalCompleteView = 0, totalReach = 0;
		for (Creative ad : creatives) {
			String name = ad.getName().toLowerCase();
			int id = ad.getId();
			String[] ids = new String[] {String.valueOf(id)};
			
			//Creative creative = CreativeMongoDbDao.getCreativeObject(id);				
			String json = CreativeMongoDbDao.getCreativeSummaryStatsJson(ids);
			JsonArray array = new Gson().fromJson(json, JsonArray.class);
			Optional<JsonArray> op = Optional.of(array);
			if( array.size()>0){
				JsonObject adSummary = array.get(0).getAsJsonObject();
				System.out.println(adSummary);
				
				totalImp += adSummary.get("imp").getAsInt();
				totalClick += adSummary.get("c").getAsInt();
				totalCompleteView += adSummary.get("trv").getAsInt();
				totalReach += adSummary.get("reach").getAsInt();
									
				System.out.println(name + " " + ad.getCreatedDate() + " " );	
			}					
					
		}
		
		System.out.println("==> size: "+creatives.size());
		System.out.println("==> totalImp: "+totalImp);
		System.out.println("==> totalClick: "+totalClick);
		System.out.println("==> totalCompleteView: "+totalCompleteView);
		System.out.println("==> totalReach: "+totalReach);
		
	}
}
